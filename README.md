# mBot
## A Discord Bot
[![Current Status][statusIndicator]][inviteLink]
[![Library Used][libIndicator]][docsLink]
[![Upvote Count][upvoteCount]][upvoteLink]
[![Server Count][serverCount]][inviteLink]
[![Owner][ownerIndicator]][supportServerLink]

Invite the bot [here][inviteLink]   
Upvote it [here][upvoteLink]

Code is public for transparency, and self hosting is not reccomended, however it is possible.

When running `dotnet restore`, pass a `-s` flag with the value `https://www.myget.org/F/discord-net/api/v3/index.json`.

[inviteLink]: https://invite.mbot.party "Invite Me!"
[dbotsLink]: https://discordbots.org/bot/284196779044503553
[upvoteLink]: https://discordbots.org/bot/284196779044503553/vote "Upvote Me!"
[docsLink]: https://discord.foxbot.me
[supportServerLink]: https://server.mbot.party
[bigWidget]: https://discordbots.org/api/widget/284196779044503553.svg
[statusIndicator]: https://discordbots.org/api/widget/status/284196779044503553.svg?noavatar=true "Current Status"
[serverCount]: https://discordbots.org/api/widget/servers/284196779044503553.svg?noavatar=true "Server Count"
[upvoteCount]: https://discordbots.org/api/widget/upvotes/284196779044503553.svg?noavatar=true "Upvote Count"
[libIndicator]: https://discordbots.org/api/widget/lib/284196779044503553.svg?noavatar=true "Library Used"
[ownerIndicator]: https://discordbots.org/api/widget/owner/284196779044503553.svg?noavatar=true "Owner"