using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using SQLite;
using mBot.Models;
using Discord;
using System.Net.Http;
using DiscordBotsList.Api;
using Discord.Commands;
using Microsoft.AspNetCore.Hosting;
using TextMagicClient.Api;

namespace mBot
{
    public class Config
    {
        public static void Init()
        {
            string json = File.ReadAllText(Paths.System.ConfigJson);
            BotConfig = JsonConvert.DeserializeObject<BotConfig>(json);
            Database = new SQLiteConnection(Paths.System.Database);
            Database.CreateTable<DApiToken>();
            Database.CreateTable<DCreditCode>();
            Database.CreateTable<DDIUser>();
            Database.CreateTable<DDILog>();
            Database.CreateTable<DGuild>();
            Database.CreateTable<DGuildSuggestion>();
            Database.CreateTable<DiagnosticResultMap>();
            Database.CreateTable<DInfraction>();
            Database.CreateTable<DInfractionCode>();
            Database.CreateTable<DLoginCode>();
            Database.CreateTable<DLoginConf>();
            Database.CreateTable<DOwOLog>();
            Database.CreateTable<DOwOUser>();
            Database.CreateTable<DRlmeLog>();
            Database.CreateTable<DRlmeUser>();
            Database.CreateTable<DSMSLog>();
            Database.CreateTable<DTag>();
            Database.CreateTable<DUser>();
            //Database.CreateTable<>();
            Intergrations.RLME.RLMEConfig.Load();
            Intergrations.NoSelfPromo.NSPConfig.Init();
            HttpClient = new HttpClient();
            StartupDT = DateTime.Now;
            Rand = new Random();
        }
        public static async Task Authenticate()
        {
            await Client.LoginAsync(TokenType.Bot, BotConfig.Token);
            await Client.StartAsync();
            while (Client.ConnectionState != ConnectionState.Connected)
            {
                await Task.Delay(25);
            }
            while (Client.LoginState != LoginState.LoggedIn)
            {
                await Task.Delay(25);
            }
            await Task.Delay(100);
            Commands = new CommandService();
            AppOwner = await ((IDiscordClient)Client).GetUserAsync(BotConfig.Owners[0]);
            Application = await Client.GetApplicationInfoAsync() as IApplication;
            Owners = new Owners(AppOwner);
            try
            {
                DblApi = new AuthDiscordBotListApi(Client.CurrentUser.Id, BotConfig.DBotsOrgToken);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error connecting to DBots: " + e.Message);
            }
            try
            {
                TextMagicClient.Client.Configuration TMConfig = new TextMagicClient.Client.Configuration();
                TMConfig.Username = "marens101";
                TMConfig.Password = BotConfig.TextMagicToken;
                TMConfig.BasePath = "https://rest.textmagic.com";
                TextMagicClient = new TextMagicApi(TMConfig);
                    //new TextmagicRest.Client("marens101", Config.BotConfig.TextMagicToken);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error connecting to TextMagic: " + e.Message);
            }
        }
        public static DiscordSocketClient Client;
        public static CommandService Commands;
        public static IApplication Application { get; set; }
        public static IUser AppOwner { get; set; }
        public static BotConfig BotConfig { get; set; }
        public static SQLiteConnection Database { get; set; }
        public static string Version { get; }
            = "2.3";
        public static HttpClient HttpClient { get; set; }
        public static Owners Owners { get; set; }
        public static DateTime StartupDT { get; set; }
        public static AuthDiscordBotListApi DblApi { get; set; }
        public static TextMagicApi TextMagicClient { get; set; }
        public static IWebHost WebHost { get; set; }
        public static string DefaultStatus { get; }
            = $"/help | v{Config.Version} | Made by @marens101#4158";
        public static Random Rand { get; set; }

        public static async Task<DGuild> GetOrCreateGuildAsync(IGuild guild)
        {
            var l = Database.Query<DGuild>($"SELECT * FROM Guilds WHERE Id = ?", guild.Id.ToString());
            if (l.Count == 0)
            {
                DGuild g = new DGuild();
                g.Id = guild.Id.ToString();
                g.OwnerId = guild.OwnerId.ToString();
                g.Name = guild.Name;
                DUser du = await GetOrCreateUserAsync(await guild.GetOwnerAsync());
                Database.Insert(g);
                return g;
            }
            l[0].Name = guild.Name;
            Database.Update(l[0]);
            return l[0];
        }
        public static async Task<DGuild> GetOrCreateGuildAsync(ulong guildId, bool ThrowOnError = true)
        {
            var l = Database.Query<DGuild>($"SELECT * FROM Guilds WHERE Id = ?", guildId.ToString());
            var guild = await ((IDiscordClient)Client).GetGuildAsync(guildId);
            if (l.Count == 0)
            {
                if (guild == null)
                {
                    if (ThrowOnError)
                    {
                        throw new Exception($"Guild Not found: {guildId}");
                    }
                    else
                    {
                        return new DGuild
                        {
                            Name = "Unknown Server",
                            OwnerId = "000000000000000000",
                            Id = guildId.ToString()
                        };
                    }
                }
                else
                {
                    DGuild g = new DGuild();
                    g.Id = guild.Id.ToString();
                    g.OwnerId = guild.OwnerId.ToString();
                    g.Name = guild.Name;
                    DUser du = await GetOrCreateUserAsync(await guild.GetOwnerAsync());
                    Database.Insert(g);
                    return g;
                }
            }
            l[0].Name = guild.Name;
            Database.Update(l[0]);
            return l[0];
        }
        public static async Task<DUser> GetOrCreateUserAsync(IUser user)
        {
            var l = Database.Query<DUser>($"SELECT * FROM Users WHERE Id = ?", user.Id.ToString());
            if (l.Count == 0)
            {
                DUser u = await DUser.New(user);
                Database.Insert(u);
                return u;
            }
            l[0].Username = user.Username;
            l[0].Discrim = user.Discriminator;
            l[0].AvatarUrl = user.GetAvatarUrl();
            Database.Update(l[0]);
            return l[0];
        }
        public static async Task<DUser> GetOrCreateUserAsync(ulong userId, bool ThrowOnError = true)
        {
            var l = Database.Query<DUser>($"SELECT * FROM Users WHERE Id = ?", userId.ToString());
            var user = await ((IDiscordClient)Client).GetUserAsync(userId);
            if (l.Count == 0)
            {
                if (user == null)
                {
                    if (ThrowOnError)
                    {
                        throw new Exception($"User Not found: <@{userId}> ({userId})");
                    }
                    else
                    {
                        return new DUser
                        {
                            AvatarUrl = "",
                            Credits = 0,
                            Discrim = "0000",
                            FirstSeen = DateTime.Now,
                            Id = userId.ToString(),
                            IsStaff = false,
                            LastSeen = DateTime.Now,
                            Username = "Unknown User",
                            XP = 0
                        };
                    }
                }
                else
                {
                    DUser u = await DUser.New(user);
                    Database.Insert(u);
                    return u;
                }
            }
            l[0].AvatarUrl = user.GetAvatarUrl();
            Database.Update(l[0]);
            return l[0];
        }
    }
    public class BotConfig
    {
        public string Token;
        public string DefaultPrefix;
        public string OverridePrefix;
        public ulong[] Owners;
        public string ApiEndpoint;
        public int ApiPort;
        public string ApiMasterToken;
        public string RollbarToken;
        public string SentryDSN;
        public string Environment;
        public string DBotsOrgToken;
        public string TextMagicToken;
        public string DBansToken;
        public int CreditsPerDollar;
        public string StripeApiKey;
        public string StripeSecretKey;
        public string MashapeApiKey;
    }
}
