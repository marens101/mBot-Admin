﻿using Discord.Commands;
using mBot.Services.FanFictionService;
using System;
using System.Threading.Tasks;


namespace mBot.Modules.FanFictionModule
{
    [Group("ffnet")]
    public class FanFictionDotNetModule : ModuleBase
    {
        [Command()]
        public async Task GetStoryByID(int id, bool trunc = true)
        {
            await Context.Channel.TriggerTypingAsync();
            new FanFictionDotNetService().GetFanfic(Context, id, trunc);
        }

        [Command()]
        public async Task GetStoryByID(string url, bool trunc = true)
        {
            await Context.Channel.TriggerTypingAsync();
            int id = UrlToId(url);
            new FanFictionDotNetService().GetFanfic(Context, id, trunc);
        }

        public int UrlToId(string url)
        {
            url = url.Trim();
            string[] split = url.Split('/');
            try
            {
                string idstr = split[4];
                int id = Convert.ToInt32(idstr.Trim());
                if (id == null)
                    throw new NullReferenceException();
                return id;
            }
            catch
            {
                string idstr = split[2];
                int id = Convert.ToInt32(idstr.Trim());
                return id;
            }

        }
    }
}
