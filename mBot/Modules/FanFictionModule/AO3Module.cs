﻿using Discord.Commands;
using mBot.Services.FanFictionService;
using System;
using System.Threading.Tasks;

namespace mBot.Modules.FanFictionModule
{
    [Group("AO3")]
    public class AO3Module : ModuleBase
    {
        [Command()]
        public async Task GetStoryByID(int id, bool truncate = true)
        {
            await Context.Channel.TriggerTypingAsync();
            new AO3Service().GetFanfic(Context, id, truncate);
        }

        [Command()]
        public async Task GetStoryByID(string url, bool truncate = true)
        {
            await Context.Channel.TriggerTypingAsync();
            int id = UrlToId(url);
            new AO3Service().GetFanfic(Context, id, truncate);
        }

        public int UrlToId(string url)
        {
            url = url.Trim();
            string[] split = url.Split('/');
            try
            {
                string idstr = split[4];
                int id = Convert.ToInt32(idstr.Trim());
                if (id == null)
                    throw new NullReferenceException();
                return id;
            }
            catch
            {
                string idstr = split[2];
                int id = Convert.ToInt32(idstr.Trim());
                return id;
            }
            
        }
    }
}
