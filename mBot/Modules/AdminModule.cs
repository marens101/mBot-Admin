﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using mBot.Internal.Extensions;
using mBot.Models;
using mBot.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Modules
{
    [Group("admin")]
    [Alias("a")]
    [RequireBotOwner]
    public class AdminModule : ModuleBase
    {
        [Command("GiveEveryone")]
        [Alias("GrantEveryone", "ge")]
        public async Task GiveEveryoneRole(IRole role)
        {
            List<string> errList = new List<string>();
            string basestr = "Running...";
            var msg = await ReplyAsync(basestr);
            var users = await Context.Guild.GetUsersAsync();
            int complete = 0;
            int total = users.Count;
            int errors = 0;
            foreach (IUser u in users)
            {
                try
                {
                    await (u as IGuildUser).AddRoleAsync(role);
                    complete++;
                }
                catch
                {
                    errList.Add("An error occured while adding role to user: " + u.Username + "#" + u.Discriminator);
                    basestr = "Running..." + "\n" + errList.JoinAll();
                    errors++;
                }
                string tmsg = basestr + "\n" + " Completed " + complete + " of " + total + ". Total Errors: " + errors + '\n'
                        + "User was " + u.Username + "#" + u.Discriminator + "(" + u.Id + ")";
                await msg.ModifyAsync(c => c.Content = tmsg);
                await Task.Delay(1000); // Avoid Ratelimits
            }
            await msg.ModifyAsync(c => c.Content = errList.JoinAll() + '\n' + "Complete! Add role to " + (complete - errors) + " of " + total + " users successfully!");
        }

        [Command("SetAllNicks")]
        [Alias("san", "allnick", "allnicks", "SetNicks")]
        public async Task SetAllNicks(string nick = "none", params IGuildUser[] exclude)
        {
            StringBuilder errList = new StringBuilder();
            string basestr = "Running...";
            var msg = await ReplyAsync(basestr);
            var gusers = await Context.Guild.GetUsersAsync();
            var ex = exclude.ToList();
            var users = gusers.ToList().Except(ex);
            int complete = 0;
            int total = users.Count();
            int errors = 0;
            bool resetting = false;
            if (nick.ToLower() == "none")
                resetting = true;
            foreach (IUser u in users)
            {
                try
                {
                    if(resetting)
                        await (u as IGuildUser).ModifyAsync(n => n.Nickname = u.Username);
                    else
                        await (u as IGuildUser).ModifyAsync(n => n.Nickname = nick);
                    complete++;
                }
                catch
                {
                    errList.AppendLine("An error occured while modifying nick of user: " + u.Username + "#" + u.Discriminator);
                    basestr = "Running..." + "\n\n" + errList.ToString();
                    errors++;
                }
                string tmsg = basestr + "\n\n" + " Completed " + complete + " of " + total + ". Total Errors: " + errors + '\n'
                        + "User was " + u.Username + "#" + u.Discriminator + "(" + u.Id + ")";
                await msg.ModifyAsync(c => c.Content = tmsg);
                await Task.Delay(1000); // Avoid Ratelimits
            }
            await msg.ModifyAsync(c => c.Content = errList.ToString() + "\n\n" + "Complete! Set nick for " + (complete - errors) + " of " + total + " users successfully!");
        }

        [Command("SetAutoRole")]
        public async Task SetAutoRole(ulong id)
        {
            DGuild Guild = Config.Database.Get<DGuild>(Context.Guild.Id.ToString());
            Guild.AutoRoleId = id.ToString();
            Config.Database.Update(Guild);
            await ReplyAsync("<:ok1:331993694867554305> " + "AutoRole Set");
        }

        [Command("DeleteAutoRole")]
        [Alias("RemoveAutoRole")]
        public async Task DeleteAutoRole()
        {
            DGuild Guild = Config.Database.Get<DGuild>(Context.Guild.Id.ToString());
            Guild.AutoRoleId = null;
            Config.Database.Update(Guild);
            await ReplyAsync("<:ok1:331993694867554305> " + "AutoRole Removed");
        }

        [Command("AddAddressBook")]
        public async Task AddAdderssBook(IUser user, string num)
        {
            DUser u = Config.Database.Get<DUser>(user.Id.ToString());
            u.PhoneNumber = num;
            Config.Database.Update(u);
            await ReplyAsync("Done!");
        }

        [Command("SendText")]
        [Alias("txt")]
        public async Task SendText(string msg, string to)
        {
            /*SendingResult res = await new AdminTextMessageService().SendTextAsync(Context, msg, new string[] { to });
            if (res.Success)
            {
                await ReplyAsync("Success! Message Id: " + res.Id);
            }
            else
            {
                await ReplyAsync("Error! Exception Message: " + res.ClientException.Message);
            }*/
        }

        [Command("LeaveGuild")]
        [RequireBotOwner]
        public async Task LeaveAsync(ulong id)
        {
            var g = await Context.Client.GetGuildAsync(id);
            await g.LeaveAsync();
            await ReplyAsync($"Left Guild: \"{g.Name}\" ({g.Id})");
        }

        [Command("RoleOrder")]
        [Alias("ro")]
        public async Task RoleOrder()
        {
            await Context.Channel.TriggerTypingAsync();
            var roles = Context.Guild.Roles;
            Dictionary<string, int> roledict = new Dictionary<string, int> { };
            foreach (IRole r in roles)
            {
                roledict.Add(r.Name.Replace("@", ""), r.Position);
            }
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("```");
            foreach (KeyValuePair<string, int> i in roledict.OrderByDescending(i => i.Value))
            {
                sb.AppendLine(i.Value + ": " + i.Key.ToString());
            }
            sb.AppendLine("```");
            await ReplyAsync(sb.ToString());
        }

        [Command("RoleIds")]
        [Alias("ri")]
        public async Task RoleIds()
        {
            await Context.Channel.TriggerTypingAsync();
            var roles = Context.Guild.Roles;
            Dictionary<ulong, string> roledict = new Dictionary<ulong, string>();
            foreach (IRole r in roles)
            {
                roledict.Add(r.Id, r.Name.Replace("@", ""));
            }
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("```");
            foreach (KeyValuePair<ulong, string> i in roledict.OrderByDescending(i => i.Value))
            {
                sb.AppendLine(i.Key + ": " + i.Value);
            }
            sb.AppendLine("```");
            await ReplyAsync(sb.ToString());
        }

        [Command("MyRoles")]
        [Alias("myroleids", "mri", "mr")]
        public async Task MyRoles(IUser user = null)
        {
            user = user ?? Context.User;
            await Context.Channel.TriggerTypingAsync();
            var iguser = user as IGuildUser;
            var roles = iguser.RoleIds;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("```");
            foreach (var id in roles)
            {
                var role = Context.Guild.GetRole(id);
                sb.AppendLine(role.Id + ": " + role.Name.Replace("@", ""));
            }
            sb.AppendLine("```");
            await ReplyAsync(sb.ToString());
        }

        [Command("SetUsername")]
        public async Task SetUsername([Remainder] string name)
        {
            await Context.Client.CurrentUser.ModifyAsync(f => f.Username = name);
            await Context.Channel.SendMessageAsync("<:ok1:331993694867554305>");
        }

        [Command("Relay")]
        public async Task AdminRelayMessage(ulong srvid, ulong chanid, [Remainder] string msg)
        {
            await Context.Channel.TriggerTypingAsync();
            var srv = await Context.Client.GetGuildAsync(srvid);
            var igchan = await srv.GetTextChannelAsync(chanid);
            await igchan.TriggerTypingAsync();
            var application = await Context.Client.GetApplicationInfoAsync();
            EmbedBuilder emMsgRelay = new EmbedBuilder()
            {
                Title = "Incoming Message from marens101",
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                ThumbnailUrl = application.Owner.GetAvatarUrl(),
                Color = new Discord.Color(240, 71, 71),
                Description = msg
            };
            await igchan.SendMessageAsync("", embed: emMsgRelay.Build());
        }

        [Command("PermCheck")]
        [Alias("PermsCheck", "CheckPerm", "CheckPerms")]
        public async Task CheckPerms(IUser user)
        {
            Console.WriteLine("PermCheck Active...");
            await Context.Channel.TriggerTypingAsync();
            var application = await Context.Client.GetApplicationInfoAsync();
            var iuser = user ?? Context.User;
            var perms = (iuser as SocketGuildUser).GuildPermissions;
            EmbedBuilder emPermCheck = new EmbedBuilder()
            {
                Title = "Permissions Check",
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                //ThumbnailUrl = user.GetAvatarUrl(),
                Color = new Discord.Color(240, 71, 71),
                Description = $"Checking Permissions..."
            };
            await Context.Channel.TriggerTypingAsync();
            var efbPermAdministrator = new EmbedFieldBuilder
            {
                Name = "Permission: Administrator",
                Value = $"Value: {perms.Administrator.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermAdministrator);
            var efbPermManageGuild = new EmbedFieldBuilder
            {
                Name = "Permission: ManageGuild",
                Value = $"Value: {perms.ManageGuild.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermManageGuild);
            var efbPermManageChans = new EmbedFieldBuilder
            {
                Name = "Permission: ManageChannels",
                Value = $"Value: {perms.ManageChannels.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermManageChans);
            var efbPermManageMessages = new EmbedFieldBuilder
            {
                Name = "Permission: ManageMessages",
                Value = $"Value: {perms.ManageMessages.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermManageMessages);
            var efbPermManageRoles = new EmbedFieldBuilder
            {
                Name = "Permission: ManageRoles",
                Value = $"Value: {perms.ManageRoles.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermManageRoles);
            var efbPermBanMembers = new EmbedFieldBuilder
            {
                Name = "Permission: BanMembers",
                Value = $"Value: {perms.BanMembers.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermBanMembers);
            await ReplyAsync("", embed: emPermCheck.Build());
        }

        [Command("say")]
        //[Summary("")]
        public async Task Say(params string[] msgs)
        {
            foreach(string s in msgs)
            {
                await ReplyAsync(s);
            }
        }

        [Command("sqlexec")]
        [Alias("execsql")]
        public async Task ExecSql([Remainder] string Command)
        {
            var res = Config.Database.Execute(Command);
            var eb = new EmbedBuilder()
            {
                Title = "SQL Command Executed",
                Color = Color.Red,
                Fields =
                {
                    new EmbedFieldBuilder
                    {
                        Name = "Command Executed: ",
                        Value = "```sql" + '\n'
                            + Command + '\n'
                            + "```"
                    },
                    new EmbedFieldBuilder
                    {
                        Name = "Response: ",
                        Value = "```" + '\n'
                            + res + '\n'
                            + "```"
                    }
                }
            };
            await ReplyAsync("", embed: eb.Build());
        }

        [Command("sqlselect")]
        [Alias("sqlquery")]
        public async Task QuerySql([Remainder] string Command)
        {
            DataTable dt = Config.Database.QueryToDataTable(Command);
            string Table = dt.ToStringTable();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("**__SQL Query Executed__**");
            sb.AppendLine();
            sb.AppendLine("Query Executed: ");
            sb.AppendLine("```sql");
            sb.AppendLine(Command);
            sb.AppendLine("```");
            sb.AppendLine();
            sb.AppendLine("Result: ");
            sb.AppendLine("```");
            sb.AppendLine(Table);
            sb.AppendLine("```");
            await ReplyAsync(sb.ToString());
            
        }

        [Command("CreateCreditCode")]
        [Alias("ccc")]
        public async Task CreateCreditCode(int CreditsPerCode, int NumberOfCodes = 1)
        {
            var m = await ReplyAsync($"Creating {NumberOfCodes} code(s) for {CreditsPerCode} credits.");
            await Context.Channel.TriggerTypingAsync();
            List<DCreditCode> Codes = new List<DCreditCode>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Here are your {NumberOfCodes} code(s), each worth {CreditsPerCode} credits.");
            sb.AppendLine("```");
            for(int i = 0; i < NumberOfCodes; i++)
            {
                DCreditCode c = DCreditCode.New(CreditsPerCode);
                Codes.Add(c);
                sb.AppendLine(c.Key);
                Config.Database.Insert(c);
            };
            sb.AppendLine("```");
            await m.ModifyAsync(c => c.Content = sb.ToString());
        }
    }
}
