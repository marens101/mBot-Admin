﻿using Discord.Commands;
using mBot.Services;
using System.Threading.Tasks;

namespace mBot.Modules
{
    public class SearchModule : ModuleBase
    {
        [Command("Urban")]
        public async Task UrbanDict(string search)
        {
            await new UrbanService().UrbanSearch(Context, search);
        }
    }
}
