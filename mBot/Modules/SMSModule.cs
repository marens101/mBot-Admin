﻿using Discord.Commands;
using mBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextMagicClient;

namespace mBot.Modules
{
    public class SMSModule : ModuleBase
    {
        /*[Command("sms")]
        [CreditCost(3)]
        public async Task SendSms(long phoneNumber, [Remainder] string Message)
        {
            var user = await Config.GetOrCreateUserAsync(Context.User);
            var sendingOptions = new TextMagicClient.Model.SendingOptions();
            if (Message.Length > 130)
            {
                string oldMsg = Message;
                Message = Message.Substring(0, 130);
                await ReplyAsync("Because the message was more than 130 characters, it has been trimmed down to 130 characters." + '\n'
                    + "Old Message:" + '\n'
                    + "```" + '\n'
                    + oldMsg + '\n'
                    + "```" + '\n'
                    + "New Message:" + '\n'
                    + "```" + '\n'
                    + Message + '\n'
                    + "```");
            }

            sendingOptions.Phones = new string[] { phoneNumber.ToString() };
            sendingOptions.Text = $"<{user.UsernameDiscrim}> {Message}";
            sendingOptions.From = "mBot";
            var price = Config.TextMagicClient.GetPrice(sendingOptions);
            //await ReplyAsync($"SMS Price: {price.Total.ToString()}");
            user.Credits = user.Credits - 3;
            Config.Database.Update(user);
            await ReplyAsync($"You have been charged 3 credits. Your new balance is {user.Credits}");
            var link = Config.TextMagicClient.SendMessage(sendingOptions);

            if (link.Success)
            {
                await ReplyAsync($"The message has been successfully sent");
            }
            else
            {
                await ReplyAsync($"Message was not sent due to following exception: {link.ClientException.Message}." + '\n'
                    + "If this keeps happening, please contact marens101#4158, who can be  found by running /support.");
            }
            DSMSLog logEntry = new DSMSLog()
            {
                FromUserId = Context.User.Id.ToString(),
                ToNumber = phoneNumber.ToString(),
                Message = Message,
                Timestamp = DateTime.Now,
                CreditCharge = 3,
                SMSId = link.Id.ToString()
            };
            Config.Database.Insert(logEntry);
        }*/
        public void sendMessage(string phoneNumber, string Message)
        {

        }
    }
}
