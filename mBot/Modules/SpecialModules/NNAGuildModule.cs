﻿using Discord.Commands;
using mBot.Objects.ConfigObjects;
using System.Threading.Tasks;


namespace mBot.Modules.SpecialModules
{
    [Group("NNA")]
    [RequireGuild(366187551557550081)]
    public class TwitterModule : ModuleBase
    {
        [Command("twitter")]
        public async Task Twitter()
        {
            var auth = new TwitterAuth(TwitterAuth.AuthAccount.NNA);

            Tweetinvi.Auth.SetUserCredentials(auth.ConsumerKey, auth.ConsumerSecret, auth.userAccessToken, auth.userAccessSecret);
            //ITwitterCredentials creds = new TwitterCredentials(auth.ConsumerKey, auth.ConsumerSecret, auth.userAccessToken, auth.userAccessSecret);
            //Auth.SetCredentials(creds);
            //ITwitterCredentials creds = new TwitterCredentials("POIoW0PbBz2DjnFhRyV7zeqiK", "aPQn9iCPBlsoIPcNuZgkOxyiOwYCLOcxQmbXJ74Pg2KMVURndv", "941870760045420544-qZnj5QL5R6Hbe2Ffq3VhU4QCpjrKfeg", "x2NH2wsTDpGMDr7DN3Awx3O20fScAC84hEU0uQV5Sccny");

            var user = Tweetinvi.User.GetAuthenticatedUser();
            string reply = "Username: " + user.ScreenName + "\n"
                            + "URL: " + "https://twitter.com/NNA_Party" + "\n"
                            //+ "Location: " + user.Location + "\n"
                            //+ "ID: " + user.Id + "\n"
                            + "Tweets Count: " + user.StatusesCount + "\n"
                            + "Friends Count: " + user.FriendsCount + "\n"
                            + "Followers Count: " + user.FollowersCount + "\n"
                            + "Favourites Count: " + user.FavouritesCount + "\n"
                            //[Broken!!!] + "Latest Tweet: " + user.Status.Text ?? "No Tweets Yet"
                            ;
            await ReplyAsync(reply);
        }

        [Command("tweet")]
        [RequireBotOwner]
        public async Task Tweet([Remainder] string text)
        {
            var auth = new TwitterAuth(TwitterAuth.AuthAccount.NNA);
            Tweetinvi.Auth.SetUserCredentials(auth.ConsumerKey, auth.ConsumerSecret, auth.userAccessToken, auth.userAccessSecret);
            var user = Tweetinvi.User.GetAuthenticatedUser();

            var tweet = user.PublishTweet(text);

            await ReplyAsync("Tweeted! URL: " + tweet.Url);
        }
    }
}
