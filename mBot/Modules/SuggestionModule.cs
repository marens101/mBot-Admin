﻿using Discord;
using Discord.Commands;
using mBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Modules
{
    public class SuggestionModule : ModuleBase
    {
        [Command("suggest")]
        [Priority(1)]
        [Summary("Makes a suggestion.")]
        public async Task Suggest([Remainder] string msg)
        {
            DGuild conf = Config.Database.Get<DGuild>(Context.Guild.Id.ToString());
            if (conf.SuggestionChannelId == null)
            {
                throw new Exception("This server doesn't have access to the Suggestion Module!");
            }

            var suggestChan = await Context.Guild.GetTextChannelAsync(Convert.ToUInt64(conf.SuggestionChannelId));
            var Suggestion = DGuildSuggestion.New(conf, msg, Context.User);
            var em = Suggestion.BuildEmbed().Build();
            var smsg = await suggestChan.SendMessageAsync("", embed: em);
            Suggestion.MessageId = smsg.Id.ToString();
            Config.Database.Update(Suggestion);
            Emote voteYes = Emote.Parse("<:DBLTickYes:445878626941141004>");
            await Task.Delay(250);
            Emote voteNo = Emote.Parse("<:DBLCrossNo:445878691046883330>");
            await smsg.AddReactionAsync(voteYes);
            await smsg.AddReactionAsync(voteNo);
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }

        [Command("setsuggestionproperty")]
        [Alias("ssuggest", "ssp", "setsuggestprop")]
        [RequireUserPermission(GuildPermission.Administrator)]
        [Summary("Sets a property on a given suggestion.")]
        public async Task SetSuggest(int suggestionId, string property, [Remainder] string newValue)
        {
            DGuild conf = await Config.GetOrCreateGuildAsync(Context.Guild);
            if (conf.SuggestionChannelId == null)
            {
                throw new Exception("This server doesn't have access to the Suggestion Module!");
            }

            var Suggestion = Config.Database.Get<DGuildSuggestion>(suggestionId);
            if (Suggestion == null)
            {
                throw new Exception("Suggestion Not Found");
            }
            else if (Suggestion.GuildId != Context.Guild.Id.ToString())
            {
                throw new Exception("Suggestion Not Found");
            }

            var suggestChan = await Context.Guild.GetTextChannelAsync(Convert.ToUInt64(conf.SuggestionChannelId));
            string pUpper = property.ToUpper();
            string sUpper = newValue.ToUpper();
            if (pUpper == "STATUS")
            {
                if (sUpper == "APPROVED")
                {
                    Suggestion.Status = SuggestionStatus.Approved;
                }
                else if (sUpper == "COMPLETED")
                {
                    Suggestion.Status = SuggestionStatus.Completed;
                }
                else if (sUpper == "DENIED")
                {
                    Suggestion.Status = SuggestionStatus.Denied;
                }
                else if (sUpper == "PENDING")
                {
                    Suggestion.Status = SuggestionStatus.Pending;
                }
                else if (sUpper == "ONHOLD")
                {
                    Suggestion.Status = SuggestionStatus.OnHold;
                }
                else if (sUpper == "INDISCUSSION")
                {
                    Suggestion.Status = SuggestionStatus.InDiscussion;
                }
                else
                {
                    throw new Exception("Invalid status. Valid options are `APPROVED`, `COMPLETED`, `DENIED`, `PENDING`, `ONHOLD` and `INDISCUSSION`.");
                }
            }
            else if (pUpper == "MESSAGE")
            {
                Suggestion.Comment = newValue;
            }
            else if (pUpper == "EMBED")
            {
                if (sUpper == "REBUILD")
                {
                    // Do nothing except not throwing the error, it'll rebuild
                }
                else
                {
                    throw new Exception("Action/Property Not Found. Valid options are `REBUILD`");
                }
            }
            else
            {
                throw new Exception("Property Not Found. Valid options are `STATUS` and `MESSAGE`");
            }
            var em = Suggestion.BuildEmbed();
            var smsg = await suggestChan.GetMessageAsync(Convert.ToUInt64(Suggestion.MessageId));
            var msg = smsg as IUserMessage;
            Config.Database.Update(Suggestion);
            await msg.ModifyAsync(m => m.Embed = em.Build());
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }
        [Command("setsuggestchan")]
        [Alias("ssc")]
        [RequireBotOwner]
        public async Task SetSuggestChan(ITextChannel chan)
        {
            var g = await Config.GetOrCreateGuildAsync(Context.Guild);
            g.SuggestionChannelId = chan.Id.ToString();
            Config.Database.Update(g);
            await ReplyAsync("Done!");
        }
        [Command("getsuggestion")]
        [Alias("getsuggest", "suggestion")]
        public async Task GetSuggestion(int suggestionId)
        {
            DGuild conf = await Config.GetOrCreateGuildAsync(Context.Guild);
            if (conf.SuggestionChannelId == null)
            {
                throw new Exception("This server doesn't have access to the Suggestion Module!");
            }

            var Suggestion = Config.Database.Get<DGuildSuggestion>(suggestionId);
            if (Suggestion == null)
            {
                throw new Exception("Suggestion Not Found");
            }
            else if (Suggestion.GuildId != Context.Guild.Id.ToString())
            {
                throw new Exception("Suggestion Not Found");
            }

            var em = Suggestion.BuildEmbed();
            await ReplyAsync("", embed: em.Build());
        }
        [Command("AddVoteReactions")]
        [Alias("avr")]
        public async Task AddVoteReactions(ulong msgId = 0)
        {
            if (msgId == 0)
            {
                throw new NotImplementedException("Known Issue (NotImplementedException) - See issue MB-3");
            }
            else
            {
                var msg = (await Context.Channel.GetMessageAsync(msgId) as IUserMessage);
                await AddVoteReactions(msg);
                var rsp = await ReplyAsync("Done!");
                await Task.Delay(2500);
                await rsp.DeleteAsync();
                await Context.Message.DeleteAsync();
            }

        }


        public async Task AddVoteReactions(IUserMessage msg)
        {
            Emote voteYes = Emote.Parse("<:DBLTickYes:445878626941141004>");
            Emote voteNo = Emote.Parse("<:DBLCrossNo:445878691046883330>");
            await msg.AddReactionAsync(voteYes);
            await Task.Delay(250);
            await msg.AddReactionAsync(voteNo);
        }
    }
}
