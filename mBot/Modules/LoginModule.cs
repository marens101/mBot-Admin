﻿using Discord;
using Discord.Commands;
using Discord.Webhook;
using mBot.Internal.Extensions;
using mBot.Models;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Modules.LoginModule
{
    [Group("Login")]
    [RequireContext(ContextType.Guild)]
    public class LoginModule : ModuleBase
    {
        [Command(), Priority(0)]
        [Summary("Logs you in to the server if the server owner has arranged it with @marens101#4158")]
        public async Task Login(string code = null)
        {
            await Context.Channel.TriggerTypingAsync();
            string guildId = Convert.ToString(Context.Guild.Id);
            var confquery = Config.Database.Query<DLoginConf>("SELECT * FROM LoginConfs WHERE GuildId = ?", guildId);
            var codequery = Config.Database
                .Query<DLoginCode>("SELECT * FROM LoginCodes WHERE GuildId = ?", guildId)
                .Where(c => c.Code == code).ToList();
            var user = await Config.GetOrCreateUserAsync(Context.User);
            IUserMessage reply = null;
            if (confquery.Count != 0)
            {
                DLoginConf conf = confquery.FirstOrDefault();
                if (conf.LoginChannelId == Context.Channel.Id.ToString())
                {
                    ulong rta = Convert.ToUInt64(conf.RoleToAddId);
                    ulong rtr = Convert.ToUInt64(conf.RoleToRemoveId);
                    var hook = new DiscordWebhookClient(Convert.ToUInt64(conf.WebhookId), conf.WebhookToken);
                    if (conf.Type == LoginType.None)
                    {
                        await LoginAsync(Context, rta, rtr);
                        await hook.SendMessageAsync($"User Logged In: {user.UsernameDiscrim} ({Context.User.Id})");
                        reply = await ReplyAsync("Logged in successfully");
                    }
                    else if(conf.Type == LoginType.RefCode)
                    {
                        if (code == null)
                        {
                            await LoginAsync(Context, rta, rtr);
                            await hook.SendMessageAsync($"User Logged In: {user.UsernameDiscrim} ({Context.User.Id})" + '\n'
                                + "No referral code was used.");
                            reply = await ReplyAsync("Logged in successfully");
                        }
                        else if(codequery.Count != 0)
                        {
                            var CodeObj = codequery.FirstOrDefault();
                            AgeLoginCode(CodeObj);
                            await LoginAsync(Context, rta, rtr);
                            var owner = await Config.GetOrCreateUserAsync(Convert.ToUInt64(CodeObj.OwnerId));
                            await hook.SendMessageAsync(
                                $"User Logged In: {user.UsernameDiscrim} ({Context.User.Id})" + "\n"
                                + $"Referral Code Used: `{CodeObj.Code}`, owned by {owner.UsernameDiscrim}, now used {CodeObj.Uses} times.");
                            await ReplyAsync($"Logged in successfully with referral code `{CodeObj.Code}`");
                        }
                        else
                        {
                            await ReplyAsync("Oh No!!! That code couldn't be found! Please try again, and if you don't have a working code than run `/login` without one.");
                        }
                    }
                    else if(conf.Type == LoginType.AuthCode)
                    {
                        if(code == null)
                        {
                            reply = await ReplyAsync("Sorry, but this server requires a valid authorisation code to proceed. Please obtain one, then try again");
                        }
                        else if (codequery.Count != 0)
                        {
                            var CodeObj = codequery.FirstOrDefault();
                            if (CodeObj.Expired == true)
                            {
                                reply = await ReplyAsync("Sorry, but this server requires a valid authorisation code to proceed, and that one has expired. Please obtain a valid one, then try again");
                            }
                            else if (CodeObj.MaxUses == CodeObj.Uses)
                            {
                                reply = await ReplyAsync("Sorry, but this server requires a valid authorisation code to proceed, and that one has been used the maximum number of allowed times. Please obtain a valid one, then try again");
                            }
                            else if (CodeObj.MaxUses == null)
                            {
                                AgeLoginCode(CodeObj);
                                await LoginAsync(Context, rta, rtr);
                                var owner = await Config.GetOrCreateUserAsync(Convert.ToUInt64(CodeObj.OwnerId));
                                await hook.SendMessageAsync(
                                    $"User Logged In: {user.UsernameDiscrim} ({Context.User.Id})" + "\n"
                                    + $"Auth Code Used: `{CodeObj.Code}`, owned by {owner.UsernameDiscrim}, now used {CodeObj.Uses} times. Code expiry status: {CodeObj.Expired}");
                                reply = await ReplyAsync($"Logged in successfully with auth code {CodeObj.Code}");
                            }
                            else
                            {
                                AgeLoginCode(CodeObj);
                                await LoginAsync(Context, rta, rtr);
                                var owner = await Config.GetOrCreateUserAsync(Convert.ToUInt64(CodeObj.OwnerId));
                                await hook.SendMessageAsync(
                                    $"User Logged In: {user.UsernameDiscrim} ({Context.User.Id})" + "\n"
                                    + $"Auth Code Used: `{CodeObj.Code}`, owned by {owner.UsernameDiscrim}, now used {CodeObj.Uses} of {CodeObj.MaxUses} times. Code expiry status: {CodeObj.Expired}");
                                reply = await ReplyAsync($"Logged in successfully with auth code {CodeObj.Code}");
                            }
                        }
                        else
                        {
                            reply = await ReplyAsync("Sorry, but this server requires a valid authorisation code to proceed, and that one couldn't be found. Please obtain a valid one, then try again");
                        }
                    }
                    else
                    {
                        reply = await ReplyAsync("Something went wrong. Please report this.");
                    }
                    Task _ = DeleteMessagesAfter(10,
                        Context.Message,
                        reply);
                }
                else
                {
                    await ReplyAsync("Whoops! Wrong channel!");
                }
            }
            else
            {
                await ReplyAsync("Whoops! Invalid server!");
            }
        }

        public async Task LoginAsync(ICommandContext Context, ulong RoleToAddId, ulong RoleToRemoveId, IUser user = null)
        {
            IGuildUser iuser = (user ?? Context.User) as IGuildUser;
            if (iuser == null)
            {
                throw new System.Exception("Failed to get User");
            }
            var roletoadd = iuser.Guild.GetRole(RoleToAddId);
            var roletoremove = iuser.Guild.GetRole(RoleToRemoveId);
            await iuser.RemoveRoleAsync(roletoremove);
            //await Task.Delay(5000);
            await iuser.AddRoleAsync(roletoadd);
            //await Context.Guild.FindRoles("").FirstOrDefault();
        }

        public void AgeLoginCode(DLoginCode code)
        {
            code.Uses++;
            Config.Database.Update(code);
        }

        [Command("generate"), Priority(1)]
        [Alias("new")]
        [Summary("Creates a login code")]
        public async Task CreateCode(int uses = 1)
        {
            var confquery = Config.Database.Query<DLoginConf>("SELECT * FROM LoginConfs WHERE GuildId = ?", Context.Guild.Id.ToString());
            if (confquery.Count != 0)
            {
                var conf = confquery.FirstOrDefault();
                if (conf.Type == LoginType.None)
                    throw new System.Exception("This guild's login configuration does not use codes");
                if (conf.CodeCreationRequirements == LoginCodeCreationRequirements.ManageGuild && !(Context.User as IGuildUser).GuildPermissions.ManageGuild)
                    throw new System.Exception("You aren't authorised!");
                if (conf.CodeCreationRequirements == LoginCodeCreationRequirements.Role && !(Context.User as IGuildUser).RoleIds.Contains(Convert.ToUInt64(conf.RequiredRole)))
                    throw new System.Exception("You aren't authorised!");
                LoginCodeType type = new LoginCodeType();
                switch (conf.Type)
                {
                    case LoginType.RefCode:
                        type = LoginCodeType.RefCode;
                        break;
                    case LoginType.AuthCode:
                        if (uses == 1)
                            type = LoginCodeType.SingleUseAuth;
                        else
                            type = LoginCodeType.MultiUseAuth;
                        break;
                }
                LoginCodeCreationConfig cconf = new LoginCodeCreationConfig
                {
                    MaxUses = uses,
                    type = type,
                    user = Context.User
                };
                DLoginCode code = conf.GenerateLoginCode(cconf);
                Config.Database.Insert(code);
                string maxuses()
                {
                    if (code.MaxUses == null)
                    {
                        return "null";
                    }
                    else
                        return code.MaxUses.ToString();
                }
                await ReplyAsync($"Success! Code: `{code.Code}`, Max Uses: `{maxuses()}`. Type: `{code.Type}`");
            }
            else
            {
                await ReplyAsync("This guild has not arranged a login system. Please contact marens101#4158");
            }
        }

        [Command("revoke"), Priority(1)]
        [Summary("Revokes a login code")]
        public async Task RevokeCode(string code)
        {
            var user = await Config.GetOrCreateUserAsync(Context.User);
            await Context.Channel.TriggerTypingAsync();
            string guildId = Convert.ToString(Context.Guild.Id);
            var confquery = Config.Database.Query<DLoginConf>("SELECT * FROM LoginConfs WHERE GuildId = ?", guildId);
            var codequery = Config.Database
                .Query<DLoginCode>("SELECT * FROM LoginCodes WHERE GuildId = ?", guildId)
                .Where(c => c.Code == code).ToList();
            if(confquery.Count == 0)
            {
                await ReplyAsync("Whoops! Invalid server!");
            }
            else
            {
                var conf = confquery.FirstOrDefault();
                if (conf.Type == LoginType.None)
                    throw new System.Exception("This guild's login configuration does not use codes");
                if (conf.CodeCreationRequirements == LoginCodeCreationRequirements.ManageGuild && !(Context.User as IGuildUser).GuildPermissions.ManageGuild)
                    throw new System.Exception("You aren't authorised!");
                if (conf.CodeCreationRequirements == LoginCodeCreationRequirements.Role && !(Context.User as IGuildUser).RoleIds.Contains(Convert.ToUInt64(conf.RequiredRole)))
                    throw new System.Exception("You aren't authorised!");
                if (codequery.Count == 0)
                {
                    await ReplyAsync("Whoops! Code not found on this server!");
                }
                else
                {
                    var CodeObj = codequery.FirstOrDefault();
                    var owner = await Config.GetOrCreateUserAsync(Convert.ToUInt64(CodeObj.OwnerId));
                    var hook = new DiscordWebhookClient(Convert.ToUInt64(conf.WebhookId), conf.WebhookToken);
                    Config.Database.Delete(CodeObj);
                    await ReplyAsync("Code deleted.");
                    await hook.SendMessageAsync(
                                $"Code Deleted: `{CodeObj.Code}` (Deleted by {user.UsernameDiscrim})" + "\n"
                                + $"Code Usage: Used {CodeObj.Uses}/{CodeObj.MaxUses} times. Expiry status: {CodeObj.Expired}");
                }
            }
        }

        [Command("list"), Priority(1)]
        [Summary("Lists login codes")]
        public async Task ListCodes()
        {
            var confquery = Config.Database.Query<DLoginConf>("SELECT * FROM LoginConfs WHERE GuildId = ?", Context.Guild.Id.ToString());
            if (confquery.Count != 0)
            {
                var conf = confquery.FirstOrDefault();
                if (conf.Type == LoginType.None)
                    throw new System.Exception("This guild's login configuration does not use codes");
                if (conf.CodeCreationRequirements == LoginCodeCreationRequirements.ManageGuild && !(Context.User as IGuildUser).GuildPermissions.ManageGuild)
                    throw new System.Exception("You aren't authorised!");
                if (conf.CodeCreationRequirements == LoginCodeCreationRequirements.Role && !(Context.User as IGuildUser).RoleIds.Contains(Convert.ToUInt64(conf.RequiredRole)))
                    throw new System.Exception("You aren't authorised!");

                var codequery = Config.Database
                    .Query<DLoginCode>("SELECT * FROM LoginCodes WHERE GuildId = ?", Context.Guild.Id.ToString());

                DataTable dt = new DataTable();
                dt.Columns.Add("Code");
                dt.Columns.Add("Owner");
                dt.Columns.Add("Uses");
                dt.Columns.Add("Max. Uses");
                dt.Columns.Add("Expired?");

                foreach (DLoginCode code in codequery)
                {
                    var owner = await Config.GetOrCreateUserAsync(Convert.ToUInt64(code.OwnerId));
                    DataRow row = dt.NewRow();
                    row["Code"] = code.Code;
                    row["Owner"] = owner.UsernameDiscrim;
                    row["Uses"] = code.Uses;
                    row["Max. Uses"] = code.MaxUses;
                    row["Expired?"] = code.Expired;
                    dt.Rows.Add(row);
                }
                await ReplyAsync("```\n" + dt.ToStringTable() + "```");
            }
            else
            {
                await ReplyAsync("Error: No Config.");
            }
        }

        [Command("override"), Priority(1)]
        [Alias("user")]
        [RequireUserPermission(GuildPermission.ManageRoles)]
        [Summary("Manually approves a user, bypassing the login system. Requires ManageRoles.")]
        public async Task SetupLogins(IUser user)
        {
            var confquery = Config.Database.Query<DLoginConf>("SELECT * FROM LoginConfs WHERE GuildId = ?", Context.Guild.Id.ToString());
            if (confquery.Count == 0)
                throw new System.Exception("No Config Found!");
            var conf = confquery.FirstOrDefault();
            ulong rta = Convert.ToUInt64(conf.RoleToAddId);
            ulong rtr = Convert.ToUInt64(conf.RoleToRemoveId);
            var hook = new DiscordWebhookClient(Convert.ToUInt64(conf.WebhookId), conf.WebhookToken);
            var approver = await Config.GetOrCreateUserAsync(Context.User);
            var subject = await Config.GetOrCreateUserAsync(user);
            await LoginAsync(Context, rta, rtr, user);
            await hook.SendMessageAsync($"User Logged In: {subject.UsernameDiscrim} ({subject.Id})" + '\n'
                + $"Logged in via manual approval from {approver.UsernameDiscrim} ({approver.Id})");
            await ReplyAsync("Done!");
        }

        [Command("setup"), Priority(1)]
        [RequireBotOwner]
        [Summary("Sets up logins, with the current channel as the login channel.")]
        public async Task SetupLogins(
            ITextChannel LogOutputChannel, 
            IRole RoleToAdd, 
            IRole RoleToRemove, 
            LoginType loginType = LoginType.AuthCode)
        {
            var hook = await LogOutputChannel.CreateWebhookAsync("Loggin Logger");
            var conf = new DLoginConf();
            conf.CodeCreationRequirements = LoginCodeCreationRequirements.None;
            conf.GuildId = Context.Guild.Id.ToString();
            conf.LoginChannelId = Context.Channel.Id.ToString();
            conf.RoleToAddId = RoleToAdd.Id.ToString();
            conf.RoleToRemoveId = RoleToRemove.Id.ToString();
            conf.Type = loginType;
            conf.WebhookId = hook.Id.ToString();
            conf.WebhookToken = hook.Token;
            Config.Database.Insert(conf);
            await ReplyAsync("Done!");
        }

        [Command("disable"), Priority(1)]
        [RequireBotOwner]
        [Summary("Deletes all login config for the current guild.")]
        public async Task SetupLogins(bool DeleteCodes = true, bool DeleteWebhook = false)
        {
            var conf = Config.Database.FindWithQuery<DLoginConf>("SELECT * FROM LoginConfs WHERE GuildId = ?", Context.Guild.Id.ToString());
            Config.Database.Execute("DELETE FROM LoginConfs WHERE GuildId = ?", Context.Guild.Id);
            await ReplyAsync("Config Deleted.");
            if (DeleteCodes)
            {
                int deleted = Config.Database.Execute("DELETE FROM LoginCodes WHERE GuildId = ?", Context.Guild.Id);
                await ReplyAsync($"{deleted} codes deleted.");
            }
            if (DeleteWebhook)
            {
                try
                {
                    var hook = new DiscordWebhookClient(Convert.ToUInt64(conf.WebhookId), conf.WebhookToken);
                    await hook.DeleteWebhookAsync();
                    await ReplyAsync("Webhook deleted.");
                }
                catch (Exception e)
                {
                    await ReplyAsync("Error deleting webhook: " + e.Message);
                }
            }
            await ReplyAsync("Done!");
        }

        public async Task DeleteMessagesAfter(int seconds, params IMessage[] msgs)
        {
            await Task.Delay(seconds * 1000);
            foreach(IMessage msg in msgs)
            {
                try
                {
                    await msg.DeleteAsync();
                }
                catch
                {

                }
            }
        }
    }
}
