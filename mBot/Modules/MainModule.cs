﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace mBot.Modules.MainModule
{
    public class MainModule : ModuleBase
    {
        private DiscordSocketClient client;

        [Command("invite")]
        [Summary("Returns the invite of the bot.")]
        public async Task Invite()
        {
            var application = await Context.Client.GetApplicationInfoAsync();

            var emInvite = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                Color = new Discord.Color(114, 137, 218),
                Title = $"Invite {application.Name} to your server!",
                Url = "http://rb.marens101.com/m101bot",
                Timestamp = DateTime.UtcNow,
                Footer = new EmbedFooterBuilder
                {
                    Text = $"Also, join the support server with /support"
                }
                //ThumbnailUrl = ""
            };
            await ReplyAsync("", embed: emInvite.Build());
        }

        [Command("support")]
        [Summary("Returns an invite for the marens101 Admin Bot Support Server.")]
        public async Task Support()
        {
            var msgg = await ReplyAsync("Hang on a moment, getting the deets...");
            var application = await Context.Client.GetApplicationInfoAsync();
            /*var supSrv = client.GetGuild(291819411814219776);
            var supChan = supSrv.GetChannel(291819411814219776);
            var isup = supChan as ITextChannel;
            var isrv = Context.Guild as IGuild;
            var supChans = supSrv.TextChannels;
            var inv = await isup.CreateInviteAsync();
            string supInv = inv.Url;
            Console.WriteLine(supInv);
            Console.WriteLine(supInv);*/
            var emInvite = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                Color = new Discord.Color(114, 137, 218),
                Title = $"marens101 Admin Bot Support",
                //Url = supSrvInvite,
                Timestamp = DateTime.UtcNow,
                Footer = new EmbedFooterBuilder
                {
                    Text = "Invite links subject to change at any time" //"Support Server invite link valid for 1/2 an hour from time of creation",
                }
                //ThumbnailUrl = ""
            };
            var emInviteCode = new EmbedFieldBuilder
            {
                Name = "marens101 Admin Bot Support Server",
                Value = $"Invite Link: discord.gg/rE7kEcW" + "\n" + "OR" + "\n" + "rb.marens101.com/m101botsupport",
                IsInline = true
            };
            var emInviteEmail = new EmbedFieldBuilder
            {
                Name = "marens101 Admin Bot Support Email",
                Value = $"Send any comments or queries to:" + "\n" + "botsupport@marens101.com",
                IsInline = true
            };
            emInvite.AddField(emInviteCode);
            emInvite.AddField(emInviteEmail);
            await ReplyAsync("", embed: emInvite.Build());
            await msgg.DeleteAsync();
            await ReplyAsync("https://discord.gg/rE7kEcW");
            //await msgg.
        }

        [Command("info")]
        [Alias("i")]
        [RequireBotOwner]
        [Summary("Returns information about the Bot.")]
        public async Task Info()
        {
            await Context.Channel.TriggerTypingAsync();
            var application = await Context.Client.GetApplicationInfoAsync();
            var GuildUsers = Context.Guild.GetUsersAsync();
            EmbedAuthorBuilder emInfoAuth = new EmbedAuthorBuilder()
            {
                Name = application.Name,
                IconUrl = application.IconUrl,
                Url = "http://rb.marens101.com/m101bot"
            };

            EmbedFooterBuilder emInfoFoot = new EmbedFooterBuilder()
            {
                Text = "Bot Created By marens101",
                //IconUrl = "https://lh3.googleusercontent.com/libtouj7OyT7vBGDn519ms_YX5rA_LqfyOoLNpH27IsfMCdy8Q9l9rxoPW-YQebADFXI7cBD3E4xn-aGFXvs8gKIkHUAhMGLZpDE8NKEZxY0Wonock9FozuH6LEq5YZS5zDyyxopgRYvqP4K2UTjNOqcjn0hotT-Ktig-wQrIV-dsBUx4DJZ2ovbs-THZJlf-pyqYBCSJd1IXgjCSA11CjZPGPL7zvv6Ps5dlgMUlm7pJBS4RwDmMvlOIPrg_I6iCdXWzrw9-nxZWdHW-9VQaWCDrOpmFh2r0aNs7K0JUXCVBoytlJ75wdAHCCyIeUWL1s8YWJgANbshZa2BHQ5BddQ3Huclf_3CDGxIorz227PC0uL9C3Sr_fr6fRdzEYBInptAJPutzer0gX5PkXL-JdumrcekU9mpd9daMBKeiyggsmEjtxQSYvuQRtjSkNAWzc3u0YMOr7GHklEcWwrMYeF193g2TAtBS8Rqi6wzgtWj0jB4FfQ4NsKTCse0E5piPkGi7qiDCIzmuNXxz9vYZOWztEn56RqcusuJBCS0ITkkb3wKlbumcqwLn5qcVD_W-FNhQ8Rp=w1366-h589"
            };

            EmbedFieldBuilder emInfoFieldUptime = new EmbedFieldBuilder()
            {
                Name = "UpTime:",
                Value = $"Bot started at {Process.GetCurrentProcess().StartTime.ToLocalTime().ToString(@"MMMM dd, yyyy a\t hh:mm.ss tt zzz")}"
            };

            EmbedFieldBuilder emInfoFieldGuildCreation = new EmbedFieldBuilder()
            {
                Name = "Server Creation Date & Time",
                Value = Context.Guild.CreatedAt.UtcDateTime.ToString(@"MMMM dd, yyyy a\t hh:mm.ss tt UTC")
            };

            EmbedBuilder emInfo = new EmbedBuilder()
            {
                //https://discordapp.com/oauth2/authorize?client_id=284196779044503553&scope=bot&permissions=8
                Author = emInfoAuth,
                Description = "This command will have lots of stats when it is complete!",
                Color = new Discord.Color(114, 137, 218),
                //This SHOULD be Discord Blue. Conversion: http://www.colorhexa.com\/7289da (Without the backslash "\")
                Title = "Info",
                Footer = emInfoFoot,
                Timestamp = DateTimeOffset.UtcNow,
                ThumbnailUrl = application.IconUrl,

            };

            emInfo.AddField(emInfoFieldUptime);
            emInfo.AddField(emInfoFieldGuildCreation);

            //emInfo.AddField(emInfoFieldGuildUsers);

            //emInfo.AddField(emFieldInfo);
            await ReplyAsync("", embed: emInfo.Build());
        }

        [Command("ping")]
        [Summary("Offers a response of 'Pong' with the latency.")]
        public async Task Ping()
        {
            var application = await Context.Client.GetApplicationInfoAsync();
            var emInvite = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                Color = new Discord.Color(114, 137, 218),
                Title = $"Ping",
                Description = $"Pong! {(Context.Client as DiscordSocketClient).Latency} ms :ping_pong:",
                Timestamp = DateTime.UtcNow,
                //ThumbnailUrl = ""
            };
            await ReplyAsync("", embed: emInvite.Build());
        }

        [Command("whois")]
        [Summary("Returns nformation about a user.")]
        public async Task whois([Remainder, Summary("The user for the whois query")] IUser User = null)
        {
            var user = User ?? Context.User;
            var iu = user as IGuildUser;

            var eb = new EmbedBuilder();
            eb.Author = new EmbedAuthorBuilder();
            eb.Author.Name = user.Username + "#" + user.Discriminator;
            eb.Author.IconUrl = user.GetAvatarUrl();
            eb.Title = "Whois";
            eb.ThumbnailUrl = user.GetAvatarUrl();

            eb.AddField("Username#Discrim", user.Username + "#" + user.Discriminator, true);
            eb.AddField("Is Bot?", Convert.ToString(user.IsBot), true);
            eb.AddField("Status", user.Status.ToString(), true);
            eb.AddField("Playing", user.Activity.Name ?? "Not Set", true);
            eb.AddField("User ID", user.Id.ToString(), true);
            eb.AddField("Created At", user.CreatedAt.UtcDateTime.ToString(), true);
            eb.AddField("Joined Server At", iu.JoinedAt.ToString(), true);
            eb.AddField("Current Voice Channel", iu.VoiceChannel.Name ?? "None", true);

            await ReplyAsync("", embed: eb.Build());
        }

        [Command("prefix")]
        [Summary("Shows the current bot prefix.")]
        public async Task Prefix()
        {
            var g = await Config.GetOrCreateGuildAsync(Context.Guild);
            await ReplyAsync($"The current prefix in this guild is `{g.Prefix}`");
        }

        [Command("prefix")]
        [Summary("Sets the bot prefix within this server.")]
        [RequireUserPermission(GuildPermission.ManageGuild)]
        public async Task Prefix(string newPrefix)
        {
            var g = await Config.GetOrCreateGuildAsync(Context.Guild);
            string oldPrefix = g.Prefix;
            g.Prefix = newPrefix;
            Config.Database.Update(g);
            await ReplyAsync($"The prefix for this guild has been changed from `{oldPrefix}` to `{newPrefix}`");
        }
    }
}
