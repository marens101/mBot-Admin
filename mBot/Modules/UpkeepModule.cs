﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Modules
{
    public class UpkeepModule : ModuleBase
    {
        [Command("ReloadDbl")]
        [Summary("Reloads DBL")]
        [RequireBotOwner]
        public async Task UpdateDbl()
        {
            int GuildCount = (await ((IDiscordClient)Config.Client).GetGuildsAsync()).Count;
            await Config.DblApi.UpdateStats(GuildCount);
            var me = await Config.DblApi.GetMeAsync();
            await ReplyAsync(
                "<:ok1:331993694867554305>" + "Discord Bot List Updated" + '\n'
                + "New Guild Count: " + GuildCount + '\n'
                + "Link: https://discordbots.org/bot/" + me.Id
                );
        }
        [Command("die")]
        [Summary("Shuts down the bot")]
        [RequireBotOwner]
        public async Task Die()
        {
            try
            {
                await ReplyAsync("Stopping Webserver...");
                await Config.WebHost.StopAsync();
            }
            catch (Exception e)
            {
                await ReplyAsync($"Error Stopping Webserver: {e.Message}");
            }
            await ReplyAsync("Shutting down...");
            Program.Quit(Program.ExitCode.Unassigned);

        }
        [Command("Dbl")]
        [Summary("Returns stats from Discord Bot List")]
        public async Task CheckDbl()
        {
            var me = await Config.DblApi.GetMeAsync();
            var stats = await me.GetStatsAsync();
            await Context.Channel.SendMessageAsync(
                "Discord Bot List Stats:" + '\n'
                + "Guild Count: " + stats.GuildCount + '\n'
                + "Link: https://discordbots.org/bot/" + me.Id + '\n'
                + "Voting Link: https://discordbots.org/bot/" + me.Id + "/vote" + '\n'
                + "Score: " + me.Points + '\n'
                + "Tags: " + string.Join(", ", me.Tags)
                );
        }
        [Command("updatestatus")]
        [Summary("Updates the playing status.")]
        public async Task UpdateStatus(string type = "", [Remainder] string status = null)
        {
            status = status ?? Config.DefaultStatus;
            switch (type.ToUpper())
            {
                case "PLAYING":
                    await Config.Client.SetGameAsync(status, type: ActivityType.Playing);
                    break;
                case "WATCHING":
                    await Config.Client.SetGameAsync(status, type: ActivityType.Watching);
                    break;
                case "LISTENING":
                    await Config.Client.SetGameAsync(status, type: ActivityType.Listening);
                    break;
                case "STREAMING":
                    await Config.Client.SetGameAsync(status, type: ActivityType.Streaming);
                    break;
                default:
                    await Config.Client.SetGameAsync(status, type: ActivityType.Playing);
                    break;
            }
        }
        [Command("version")]
        [Summary("Returns the currently running version of the bot")]
        public async Task ShowVersion()
        {
            await ReplyAsync($"Currently running mBot v{Config.Version}");
        }
    }
}
