﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using mBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Modules
{
    [Group("help")]
    [Alias("h")]
    public class HelpModule : ModuleBase
    {
        private readonly CommandService _service;

        public HelpModule(CommandService service)
        {
            _service = service;
        }

        [Command()]
        [Summary("List of all available commands.")]
        public async Task HelpList()
        {
            string prefix;
            if(Context.Channel is IDMChannel)
            {
                prefix = Config.BotConfig.DefaultPrefix;
            }
            else
            {
                prefix = (await Config.GetOrCreateGuildAsync(Context.Guild)).Prefix;
            }
            var builder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Description = "These are the commands you can use"
            };
            foreach (var module in CommandsList.Modules)
            {
                string description = null;
                foreach (var cmdstr in module.Commands)
                {
                    try
                    {
                        var cmds = _service.Search(Context, cmdstr);
                        if (cmds.IsSuccess)
                        {
                            var cmd = cmds.Commands.OrderByDescending(t => t.Alias).ToList().FirstOrDefault();
                            var result = await cmd.CheckPreconditionsAsync(Context);
                            if (result.IsSuccess)
                                description += $"{prefix}{cmd.Alias}\n";
                        }
                    }
                    catch (Exception e)
                    {
                        //description += e.Message + " (" + cmdstr + ')';
                    }
                }
                if (!string.IsNullOrWhiteSpace(description))
                {
                    builder.AddField(x =>
                    {
                        x.Name = module.ModuleName;
                        x.Value = description;
                        x.IsInline = true;
                    });
                }
            }
            await ReplyAsync("", false, builder.Build());
        }

        [Command()]
        [Summary("Info on a certain command")]
        public async Task HelpAsync([Remainder] string command)
        {
            var result = _service.Search(Context, command);

            if (!result.IsSuccess)
            {
                await ReplyAsync($"Sorry, I couldn't find a command like **{command}**.");
                return;
            }
            var conf = Config.Database.Get<DGuild>(Context.Guild.Id.ToString());
            string prefix = conf.Prefix;
            var builder = new EmbedBuilder()
            {
                Color = new Discord.Color(114, 137, 218),
                Description = $"Here are some commands like **{command}**"
            };

            foreach (var match in result.Commands)
            {
                var cmd = match.Command;
                List<string> param = new List<string>();
                foreach (ParameterInfo p in cmd.Parameters)
                {
                    if (p.IsOptional)
                    {
                        param.Add(p.Name + " (optional)");
                    }
                    else
                    {
                        param.Add(p.Name);
                    };
                }
                builder.AddField(x =>
                {
                    x.Name = string.Join("/", cmd.Aliases);
                    x.Value = $"Parameters: {string.Join(", ", param)}\n" +
                              $"Summary: {cmd.Summary ?? "Summary Not Found"}";
                    x.IsInline = false;
                });
            }

            await ReplyAsync("", false, builder.Build());
        }
    }
}
