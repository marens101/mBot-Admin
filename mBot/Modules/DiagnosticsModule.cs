﻿using Discord;
using Discord.Commands;
using mBot.ConfigService;
using mBot.Models;
using mBot.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Modules
{
    public class DiagnosticsModule : ModuleBase
    {
        [Command("Diagnose")]
        [RequireContext(ContextType.Guild)]
        [Summary("Attempts to diagnose common issues that may prevent the bot from functioning.")]
        public async Task Diagnose(bool verbose = false)
        {
            // Initialise
            DiagnosticResult r = new DiagnosticResult();
            IGuildUser BotUser = await Context.Guild.GetUserAsync(Context.Client.CurrentUser.Id);
            IGuildUser User = await Context.Guild.GetUserAsync(Context.User.Id);
            ISelfUser SelfUser = BotUser as ISelfUser;
            r.Guild = await Config.GetOrCreateGuildAsync(Context.Guild);
            r.User = await Config.GetOrCreateUserAsync(Context.User);
            r.BotGuildPermissions = BotUser.GuildPermissions;
            r.BotChannelPermissions = BotUser.GetPermissions(Context.Channel as IGuildChannel);
            r.UserGuildPermissions = User.GuildPermissions;
            r.ScanPerms(verbose);
            r.Channel = MTextChannel.New(Context.Channel as ITextChannel);
            r.Roles = new List<MRole>();
            foreach(IRole role in Context.Guild.Roles)
            {
                r.Roles.Add(MRole.New(role));
            };
            r.Mapping.CallerId = r.User.Id;
            r.Mapping.GuildId = r.Guild.Id;
            r.Mapping.FileName = $"{r.Mapping.DiagnosticCode}_{r.Guild.Id}_{r.Mapping.Timestamp.Year}-{r.Mapping.Timestamp.Month}-{r.Mapping.Timestamp.Day}.diagdump.json";
            File.WriteAllText($@"data\Diagnostics\Dumps\{r.Mapping.FileName}", JsonConvert.SerializeObject(r));
            Config.Database.Insert(r.Mapping);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("If the bot is missing any of the following permissions, some commands may not function correctly:");
            sb.AppendLine("```");
            foreach (KeyValuePair<string, bool> p in r.CommonPerms)
            {
                sb.AppendLine($"Permission: `{p.Key}`, Value: `{p.Value}`");
            }
            sb.AppendLine("```");
            sb.AppendLine("If no permissions were listed, then maybe the issue is something else.");
            sb.AppendLine("These diagnostics are still in beta, so contact mBot Support at https://support.mbot.party, or in the support server at https://discord.gg/rE7kEcW");
            sb.AppendLine($"Don't forget to quote the diagnostic code: `{r.DiagnosticCode}`");
            await ReplyAsync(sb.ToString());
        }
    }
}
