﻿using Discord;
using Discord.Commands;
using mBot.Internal;
using mBot.Internal.Extensions;
using mBot.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Modules
{
    [RequireContext(ContextType.Guild)]
    public class ModerationModule : ModuleBase
    {
        [Command("ban")]
        [RequireUserPermission(GuildPermission.BanMembers)]
        [Summary("Bans a user")]
        public async Task Ban(IUser user, [Remainder] string reason = "No Reason Provided")
        {
            var g = await Config.GetOrCreateGuildAsync(Context.Guild);
            var Banner = await Config.GetOrCreateUserAsync(Context.User);
            var Target = await Config.GetOrCreateUserAsync(user);
            try
            {
                await Context.Guild.GetTextChannelAsync(Convert.ToUInt64(g.ModChanId));
            }
            catch
            {
                throw BadModlogException(g);
            }
            ITextChannel modchan = await Context.Guild.GetTextChannelAsync(Convert.ToUInt64(g.ModChanId));
            if(modchan == null)
            {
                throw BadModlogException(g);
            }
            DInfraction infrac = new DInfraction()
            {
                Message = reason,
                ModId = Banner.Id,
                Type = InfractionType.Ban,
                UserId = Target.Id,
                Timestamp = DateTime.Now,
                GuildId = Context.Guild.Id.ToString()
            };
            Config.Database.Insert(infrac);
            var ro = new RequestOptions
            {
                AuditLogReason = 
                    $"Banned By {Context.User.Username}#{Context.User.Discriminator} ({Context.User.Id})" + '\n'
                     + $"Case Number: {infrac.Id}" + '\n'
                     + $"Provided Reason: {reason}"
            };
            await Context.Guild.AddBanAsync(user, 0, ro.AuditLogReason, ro);
            await Context.Channel.TriggerTypingAsync();
            var msg = await modchan.SendMessageAsync("", embed: infrac.GetEmbed().Build());
            infrac.MessageId = msg.Id.ToString();
            Config.Database.Update(infrac);
            await ReplyAsync("", embed: infrac.GetEmbed().Build());

        }

        [Command("unban")]
        [RequireUserPermission(GuildPermission.BanMembers)]
        [Summary("Unans a user")]
        public async Task Unan(ulong userId, [Remainder] string reason = "No Reason Provided")
        {
            var g = await Config.GetOrCreateGuildAsync(Context.Guild);
            var Unbanner = await Config.GetOrCreateUserAsync(Context.User);
            var Ban = await Context.Guild.GetBanAsync(userId);
            if (Ban == null)
                throw new Exception("Ban not found");
            var Target = await Config.GetOrCreateUserAsync(Ban.User);
            try
            {
                await Context.Guild.GetTextChannelAsync(Convert.ToUInt64(g.ModChanId));
            }
            catch
            {
                throw BadModlogException(g);
            }
            ITextChannel modchan = await Context.Guild.GetTextChannelAsync(Convert.ToUInt64(g.ModChanId));
            if (modchan == null)
            {
                throw BadModlogException(g);
            }
            DInfraction infrac = new DInfraction()
            {
                Message = reason,
                ModId = Unbanner.Id,
                Type = InfractionType.Unban,
                UserId = Target.Id,
                Timestamp = DateTime.Now,
                GuildId = Context.Guild.Id.ToString(),
            };
            Config.Database.Insert(infrac);
            var ro = new RequestOptions
            {
                AuditLogReason =
                    $"Unanned By {Context.User.Username}#{Context.User.Discriminator} ({Context.User.Id})" + '\n'
                     + $"Case Number: {infrac.Id}" + '\n'
                     + $"Provided Reason: {reason}" + '\n'
                     + $"Existing Ban Reason: {Ban.Reason ?? "Not provided"}"
            };
            await Context.Channel.TriggerTypingAsync();
            await Context.Guild.RemoveBanAsync(userId, ro);
            var msg = await modchan.SendMessageAsync("", embed: infrac.GetEmbed().Build());
            infrac.MessageId = msg.Id.ToString();
            Config.Database.Update(infrac);
            await ReplyAsync("", embed: infrac.GetEmbed().Build());
        }

        [Command("kick")]
        [RequireUserPermission(GuildPermission.KickMembers)]
        [Summary("Kicks a user")]
        public async Task Kick(IUser user, [Remainder] string reason = "No Reason Provided")
        {
            var g = await Config.GetOrCreateGuildAsync(Context.Guild);
            var Kicker = await Config.GetOrCreateUserAsync(Context.User);
            var Target = await Config.GetOrCreateUserAsync(user);
            try
            {
                await Context.Guild.GetTextChannelAsync(Convert.ToUInt64(g.ModChanId));
            }
            catch
            {
                throw BadModlogException(g);
            }
            ITextChannel modchan = await Context.Guild.GetTextChannelAsync(Convert.ToUInt64(g.ModChanId));
            if (modchan == null)
            {
                throw BadModlogException(g);
            }
            DInfraction infrac = new DInfraction()
            {
                Message = reason,
                ModId = Kicker.Id,
                Type = InfractionType.Kick,
                UserId = Target.Id,
                Timestamp = DateTime.Now,
                GuildId = Context.Guild.Id.ToString()
            };
            Config.Database.Insert(infrac);
            var ro = new RequestOptions
            {
                AuditLogReason =
                    $"Kicked By {Context.User.Username}#{Context.User.Discriminator} ({Context.User.Id})" + '\n'
                     + $"Case Number: {infrac.Id}" + '\n'
                     + $"Provided Reason: {reason}"
            };
            await (user as IGuildUser).KickAsync(reason);
            await Context.Channel.TriggerTypingAsync();
            var msg = await modchan.SendMessageAsync("", embed: infrac.GetEmbed().Build());
            infrac.MessageId = msg.Id.ToString();
            Config.Database.Update(infrac);
            await ReplyAsync("", embed: infrac.GetEmbed().Build());
        }

        [Command("warn")]
        [RequireUserPermission(GuildPermission.KickMembers)]
        [Summary("Warns a user")]
        public async Task Warn(IUser user, [Remainder] string reason)
        {
            var g = await Config.GetOrCreateGuildAsync(Context.Guild);
            var Warner = await Config.GetOrCreateUserAsync(Context.User);
            var Target = await Config.GetOrCreateUserAsync(user);
            try
            {
                await Context.Guild.GetTextChannelAsync(Convert.ToUInt64(g.ModChanId));
            }
            catch
            {
                throw BadModlogException(g);
            }
            ITextChannel modchan = await Context.Guild.GetTextChannelAsync(Convert.ToUInt64(g.ModChanId));
            if (modchan == null)
            {
                throw BadModlogException(g);
            }
            DInfraction infrac = new DInfraction()
            {
                Message = reason,
                ModId = Warner.Id,
                Type = InfractionType.Warn,
                UserId = Target.Id,
                Timestamp = DateTime.Now,
                GuildId = Context.Guild.Id.ToString()
            };
            Config.Database.Insert(infrac);
            await Context.Channel.TriggerTypingAsync();
            var msg = await modchan.SendMessageAsync("", embed: infrac.GetEmbed().Build());
            infrac.MessageId = msg.Id.ToString();
            Config.Database.Update(infrac);
            await ReplyAsync("", embed: infrac.GetEmbed().Build());
            try
            {
                var DMC = await (Target as IGuildUser).GetOrCreateDMChannelAsync();
                await DMC.SendMessageAsync("", embed: infrac.GetDmEmbed().Build());
            }
            catch
            {
                throw new Exception("Error DMing User");
            }
        }

        [Command("infsearch")]
        [Alias("infractionsearch", "searchinfractions")]
        [Summary("Searches for infractions involving the specified user.")]
        public async Task InfractionSearch(IUser user)
        {
            var rows = Config.Database.Query<DInfraction>("SELECT * FROM Infractions WHERE GuildId = ? AND UserId = ?", Context.Guild.Id.ToString(), user.Id.ToString());
            DataTable dt = rows.ToDataTable();
            string str = dt.ToStringTable();
            await ReplyAsync("```\n" + str + "\n```");
        }

        [Command("modlogchan")]
        [Alias("modlogchannel", "setmodlog", "modlog", "modlogs")]
        [RequireUserPermission(GuildPermission.ManageChannels)]
        [Summary("Sets the modlog channel.")]
        [Priority(0)]
        public async Task SetModlogChannel(ITextChannel Channel = null)
        {
            ITextChannel chan = Channel ?? Context.Channel as ITextChannel;
            await Context.Channel.TriggerTypingAsync();
            var g = await Config.GetOrCreateGuildAsync(Context.Guild);
            g.ModChanId = chan.Id.ToString();
            Config.Database.Update(g);
            await ReplyAsync($"Mod Channel set to \"{chan.Name}\" with id \"{chan.Id}\"");
        }

        [Command("InfLink")]
        [Alias("InfUrl")]
        public async Task GetOrCreateInfCode(IUser user)
        {
            var q = Config.Database.Query<DInfractionCode>("SELECT * FROM InfractionCodes WHERE UserId = ? AND GuildId = ?", user.Id.ToString(), Context.Guild.Id.ToString());
            if(q.Count == 0)
            {
                    var code = new DInfractionCode()
                    {
                        GuildId = Context.Guild.Id.ToString(),
                        UserId = user.Id.ToString()
                    };
                    Config.Database.Insert(code);
                    await ReplyAsync(code.Url);
                }
            else
            {
                await ReplyAsync(q[0].Url);
            }
        }

        public async Task<bool> DBansScanUser(IUser user)
        {
            var values = new Dictionary<string, string>
            {
               { "token", Config.BotConfig.DBansToken },
               { "userid", user.Id.ToString() }
            };
            var content = new FormUrlEncodedContent(values);
            var response = await Config.HttpClient.PostAsync("https://bans.discordlist.net/api", content);
            var responseString = await response.Content.ReadAsStringAsync();
            if (responseString.ToUpper() == "TRUE")
                return true;
            else
                return false;
        }
        public static Exception BadModlogException(DGuild g)
        {
            return new System.Exception($"Invalid Modlog Channel. Set with `{g.Prefix}modlogchan [Channel]`");
        }
    }
}
