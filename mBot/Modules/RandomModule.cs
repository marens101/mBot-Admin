﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Modules
{
    public class RandomModule : ModuleBase
    {
        public class Dice : ModuleBase
        {
            [Command("d4")]
            [Summary("Rolls a standard d4 dice.")]
            public async Task RollD4(int NumToRoll = 1)
            {
                await Context.Channel.TriggerTypingAsync();
                var eb = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        Name = Config.Application.Name,
                        IconUrl = Config.Application.IconUrl,
                        Url = "https://invite.mbot.party"
                    },
                    Color = new Discord.Color(114, 137, 218),
                    Title = $"Dice Roll",
                    Description = $"Rolled '{NumToRoll}' dice of type 'd4'",
                    Timestamp = DateTime.UtcNow,
                };
                
                for (int i = 1; i <= NumToRoll; i++)
                {
                    int r = Config.Rand.Next(1, 5);
                    eb.AddField($"Dice {i}", r.ToString(), true);
                }
                await ReplyAsync("Results", embed: eb.Build());
            }

            [Command("dice")]
            [Alias("roll", "d6")]
            [Summary("Rolls a standard d6 dice.")]
            public async Task RollD6(int NumToRoll = 1)
            {
                await Context.Channel.TriggerTypingAsync();
                var eb = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        Name = Config.Application.Name,
                        IconUrl = Config.Application.IconUrl,
                        Url = "https://invite.mbot.party"
                    },
                    Color = new Discord.Color(114, 137, 218),
                    Title = $"Dice Roll",
                    Description = $"Rolled '{NumToRoll}' dice of type 'd6'",
                    Timestamp = DateTime.UtcNow,
                };
                
                for (int i = 1; i <= NumToRoll; i++)
                {
                    int r = Config.Rand.Next(1, 7);
                    eb.AddField($"Dice {i}", r.ToString(), true); 
                }
                await ReplyAsync("Results", embed: eb.Build());
            }

            [Command("d8")]
            [Summary("Rolls a standard d8 dice.")]
            public async Task RollD8(int NumToRoll = 1)
            {
                await Context.Channel.TriggerTypingAsync();
                var eb = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        Name = Config.Application.Name,
                        IconUrl = Config.Application.IconUrl,
                        Url = "https://invite.mbot.party"
                    },
                    Color = new Discord.Color(114, 137, 218),
                    Title = $"Dice Roll",
                    Description = $"Rolled '{NumToRoll}' dice of type 'd8'",
                    Timestamp = DateTime.UtcNow,
                };
                
                for (int i = 1; i <= NumToRoll; i++)
                {
                    int r = Config.Rand.Next(1, 9);
                    eb.AddField($"Dice {i}", r.ToString(), true);
                }
                await ReplyAsync("Results", embed: eb.Build());
            }

            [Command("d10")]
            [Summary("Rolls a standard d10 dice.")]
            public async Task RollD10(int NumToRoll = 1)
            {
                await Context.Channel.TriggerTypingAsync();
                var eb = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        Name = Config.Application.Name,
                        IconUrl = Config.Application.IconUrl,
                        Url = "https://invite.mbot.party"
                    },
                    Color = new Discord.Color(114, 137, 218),
                    Title = $"Dice Roll",
                    Description = $"Rolled '{NumToRoll}' dice of type 'd10'",
                    Timestamp = DateTime.UtcNow,
                };
                
                for (int i = 1; i <= NumToRoll; i++)
                {
                    int r = Config.Rand.Next(1, 11);
                    eb.AddField($"Dice {i}", r.ToString(), true);
                }
                await ReplyAsync("Results", embed: eb.Build());
            }

            [Command("d12")]
            [Summary("Rolls a standard d12 dice.")]
            public async Task RollD12(int NumToRoll = 1)
            {
                await Context.Channel.TriggerTypingAsync();
                var eb = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        Name = Config.Application.Name,
                        IconUrl = Config.Application.IconUrl,
                        Url = "https://invite.mbot.party"
                    },
                    Color = new Discord.Color(114, 137, 218),
                    Title = $"Dice Roll",
                    Description = $"Rolled '{NumToRoll}' dice of type 'd12'",
                    Timestamp = DateTime.UtcNow,
                };
                
                for (int i = 1; i <= NumToRoll; i++)
                {
                    int r = Config.Rand.Next(1, 13);
                    eb.AddField($"Dice {i}", r.ToString(), true);
                }
                await ReplyAsync("Results", embed: eb.Build());
            }

            [Command("d20")]
            [Summary("Rolls a standard d20 dice.")]
            public async Task RollD20(int NumToRoll = 1)
            {
                await Context.Channel.TriggerTypingAsync();
                var eb = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        Name = Config.Application.Name,
                        IconUrl = Config.Application.IconUrl,
                        Url = "https://invite.mbot.party"
                    },
                    Color = new Discord.Color(114, 137, 218),
                    Title = $"Dice Roll",
                    Description = $"Rolled '{NumToRoll}' dice of type 'd20'",
                    Timestamp = DateTime.UtcNow,
                };
                
                for (int i = 1; i <= NumToRoll; i++)
                {
                    int r = Config.Rand.Next(1, 21);
                    eb.AddField($"Dice {i}", r.ToString(), true);
                }
                await ReplyAsync("Results", embed: eb.Build());
            }

            [Command("percentile")]
            [Alias("d100")]
            [Summary("Rolls a standard d10 percentile dice.")]
            public async Task RollD10Percentile(int NumToRoll = 1)
            {
                await Context.Channel.TriggerTypingAsync();
                var eb = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        Name = Config.Application.Name,
                        IconUrl = Config.Application.IconUrl,
                        Url = "https://invite.mbot.party"
                    },
                    Color = new Discord.Color(114, 137, 218),
                    Title = $"Dice Roll",
                    Description = $"Rolled '{NumToRoll}' dice of type 'd10 percentile'",
                    Timestamp = DateTime.UtcNow,
                };
                for (int i = 1; i <= NumToRoll; i++)
                {
                    int r = (Config.Rand.Next(1, 11)) * 10;
                    eb.AddField($"Dice {i}", r.ToString(), true);
                }
                await ReplyAsync("Results", embed: eb.Build());
            }
        }

        public class Coins : ModuleBase
        {
            [Command("flipcoin")]
            [Alias("flip", "coin", "coinflip")]
            [Summary("Flips a coin.")]
            public async Task coinflip()
            {
                var application = await Context.Client.GetApplicationInfoAsync();
                var id = await ReplyAsync("Flipping...");
                await Context.Channel.TriggerTypingAsync();
                Random gen = new Random();
                bool result = gen.Next(100) < 50 ? true : false;
                await Task.Delay(1000);
                var emCoinFlip = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        Name = application.Name,
                        IconUrl = application.IconUrl,
                        Url = "http://rb.marens101.com/m101bot"
                    },
                    Color = new Discord.Color(114, 137, 218),
                    Title = $"Coin Flip",
                    //Description = $"Pong! {(Context.Client as DiscordSocketClient).Latency} ms :ping_pong:",
                    Timestamp = DateTime.UtcNow,
                    //ThumbnailUrl = ""
                };
                if (result == true)
                {
                    emCoinFlip.Description = "Heads";
                    await id.DeleteAsync();
                    await ReplyAsync("", embed: emCoinFlip.Build());
                }
                else
                {
                    emCoinFlip.Description = "Tails";
                    await id.DeleteAsync();
                    await ReplyAsync("", embed: emCoinFlip.Build());
                }

            }

            [Command("Decide")]
            [Summary("Makes a decision.")]
            public async Task Decide([Remainder] string Decision = null)
            {
                string[] decideCoin = new string[]
                {
                "http://mbot.gitlab.io/staticresources/decide/do.png",
                "http://mbot.gitlab.io/staticresources/decide/hell.png"
                };
                Random rnd = new Random();
                int num = rnd.Next(decideCoin.Length);
                var application = await Context.Client.GetApplicationInfoAsync();
                var emInvite = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        Name = application.Name,
                        IconUrl = application.IconUrl,
                        Url = "http://rb.marens101.com/m101bot"
                    },
                    Color = new Discord.Color(114, 137, 218),
                    Title = $"Decision Maker",
                    ImageUrl = decideCoin[num],
                    Timestamp = DateTime.UtcNow,
                    //ThumbnailUrl = ""
                };
                await ReplyAsync("", embed: emInvite.Build());
            }
        }

        public class Strings : ModuleBase
        {
            [Command("guid")]
            [Alias("uuid")]
            public async Task GetGuid()
            {
                await ReplyAsync(Guid.NewGuid().ToString());
            }
        }
    }
}
