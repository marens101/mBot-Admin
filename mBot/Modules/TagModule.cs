﻿using Discord;
using Discord.Commands;
using mBot.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Modules
{
    [Group("tag")]
    [Alias("t")]
    public class TagModule : ModuleBase
    {
        [Command("add"), Summary("Creates a Tag"), Priority(1)]
        [Alias("create")]
        public async Task CreateTag(string  tagName, [Remainder] string tagText)
        {
            var gsearch = Config.Database.Query<DTag>("SELECT * FROM Tags WHERE GuildId = ?", Context.Guild.Id.ToString());
            var search = gsearch.Where(t => t.Key.ToUpper() == tagText.ToUpper()).ToList();
            if (search.Count != 0)
            {
                throw new System.Exception("Tag Already Exists");
            }

            DTag Tag = new DTag
            {
                GuildId = Context.Guild.Id.ToString(),
                Key = tagName,
                OwnerId = Context.User.Id.ToString(),
                UseCount = 0,
                Value = tagText
            };

            Config.Database.Insert(Tag);

            await ReplyAsync($"Tag Created!\nKey:\n```\n{Tag.Key}\n```\nValue:\n```\n{Tag.Value}\n```");
        }

        [Command("remove"), Summary("Removes specified Tag"), Priority(1)]
        public async Task RemoveTag([Summary("tag to remove"), Remainder] string tag)
        {
            var gsearch = Config.Database.Query<DTag>("SELECT * FROM Tags WHERE GuildId = ?", Context.Guild.Id.ToString());
            var search = gsearch.Where(t => t.Key.ToUpper() == tag.ToUpper()).ToList();
            if (search.Count == 0)
            {
                throw new System.Exception("Tag Not Found");
            }
            Config.Database.Delete(search[0]);
        }

        [Command("list"), Summary("Lists all tags"), Priority(1)]
        public async Task ListTags()
        {
            var search = Config.Database.Query<DTag>("SELECT * FROM Tags WHERE GuildId = ?", Context.Guild.Id.ToString());
            if (search.Count == 0)
            {
                throw new System.Exception("No Tags Found");
            }
            var application = Config.Application;
            var eb = new EmbedBuilder();
            eb.Author = new EmbedAuthorBuilder();
            eb.Author.Name = application.Name;
            eb.Author.IconUrl = application.IconUrl;
            eb.Color = Color.Purple;
            eb.Title = $"List Of All Tags in server: {Context.Guild.Name}";
            


            foreach (DTag t in search)
            {
                DUser owner = await Config.GetOrCreateUserAsync(Convert.ToUInt64(t.OwnerId));
                
                EmbedFieldBuilder efb = new EmbedFieldBuilder
                {
                    Name = t.Key,
                    Value = $"Created By: {owner.UsernameDiscrim ?? $"[User Not Found, <@{t.OwnerId}>]"}" + "\n"
                              + $"Times Used: {t.UseCount}",
                    IsInline = false
                };
                eb.AddField(efb);
            }

            await ReplyAsync("", embed: eb.Build());
        }

        [Command(""), Summary("Displays the value of the Tag specified if it exists"), Priority(0)]
        public async Task SearchTag([Summary("tag to search"), Remainder] string tag)
        {
            var gsearch = Config.Database.Query<DTag>("SELECT * FROM Tags WHERE GuildId = ?", Context.Guild.Id.ToString());
            var search = gsearch.Where(t => t.Key.ToUpper() == tag.ToUpper()).ToList();
            if (search.Count == 0)
            {
                throw new System.Exception("Tag Not Found");
            }
            else
            {
                DTag t = search.FirstOrDefault();
                t.UseCount++;
                Config.Database.Update(t);
                await Context.Channel.SendMessageAsync($"Contents of tag `{t.Key}`:\n```\n" + t.Value + "\n```");
            }
        }
    }
}
