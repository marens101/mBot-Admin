﻿using Discord.Commands;
using mBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Modules
{
    [Group("perms")]
    [RequireUserPermission(Discord.GuildPermission.Administrator)]
    public class PermissionsModule : ModuleBase
    {
        [Command("Add")]
        [RequireBotOwner]
        public async Task AddPermNode(string guild, string user, string role, string node)
        {
            DPermissionOverride perm = new DPermissionOverride()
            {
                GuildId = guild,
                Node = node,
                RoleId = role,
                UserId = node,
                Value = "true"
            };
            Config.Database.Insert(perm);
            await ReplyAsync($"Done! Id = {perm.Id}");
        }
    }
}
