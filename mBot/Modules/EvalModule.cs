﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Webhook;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;

namespace mBot.Modules.EvalModule
{
    public class EvalModule : ModuleBase
    {
        [Command("eval")]
        [RequireBotOwner]
        public async Task Eval([Remainder] string script)
        {
            script.Trim();
            
            var globals = new EvalGlobals
            {
                Context = Context
            };
            ScriptOptions options = ScriptOptions.Default;
            options.AddImports("mBot", "Discord", "Discord.Net", "Discord.Commands", 
                "Discord.WebSocket", "Discord.Webhook", "Discord.Rest", "Discord.API",
                "mBot.Models", "mBot.Modules", "mBot.Services", "mBot.API", "mBot.Internal", 
                "mBot.mBotAPI", "mBot.Controllers", "mBot.Objects", "mBot.Templates");
            var evalTask = await CSharpScript.EvaluateAsync(script, globals: globals, options: options);
            await ReplyAsync(evalTask.ToString());
        }


        public class EvalGlobals
        {
            public ICommandContext Context;
            //public mBot._System.ErrorHandling.ErrorEvent ErrorEvent;
        }
    }

}
