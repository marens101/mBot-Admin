﻿using Discord;
using Discord.Commands;
using mBot.Objects.FunObjects;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace mBot.Modules
{
    public class JokeModule : ModuleBase
    {
        [Command("joke")]
        [Summary("Gets a random joke.")]
        public async Task randomJoke()
        {
            await Context.Channel.TriggerTypingAsync();
            var joke = GetRandomJoke();
            var application = await Context.Client.GetApplicationInfoAsync();
            var builder = newJokeEmbed(application);
            builder.Footer = new EmbedFooterBuilder().WithText($"Joke ID: {joke.id}");
            builder.Description = joke.joke;
            var build = builder.Build();
            await ReplyAsync("", embed: build);
        }

        [Command("joke")]
        [Summary("Gets the specified joke.")]
        public async Task getJoke(string id)
        {
            await Context.Channel.TriggerTypingAsync();
            var joke = GetJoke(id);
            var application = await Context.Client.GetApplicationInfoAsync();
            var builder = newJokeEmbed(application);
            builder.Footer = new EmbedFooterBuilder
            {
                Text = $"Joke ID: {joke.id}"
            };
            builder.Description = joke.joke;
            var build = builder.Build();
            await ReplyAsync("", embed: build);

        }

        public ICanHazDadJoke GetRandomJoke()
        {
            RestClient client = new RestClient();
            RestRequest request = new RestRequest(@"https://icanhazdadjoke.com/", Method.GET);
            request.AddHeader("Accept", "application/json");
            IRestResponse response = client.Execute(request);
            return JsonConvert.DeserializeObject<ICanHazDadJoke>(response.Content);
        }

        public ICanHazDadJoke GetJoke(string id)
        {
            RestClient client = new RestClient();
            RestRequest request = new RestRequest($@"https://icanhazdadjoke.com/j/{id}", Method.GET);
            request.AddHeader("Accept", "application/json");
            IRestResponse response = client.Execute(request);
            return JsonConvert.DeserializeObject<ICanHazDadJoke>(response.Content);
        }

        public EmbedBuilder newJokeEmbed(IApplication application)
        {
            EmbedBuilder builder = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder()
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://mbot.party/"
                },
                Color = Color.Purple,
                Timestamp = DateTime.Now,


            };
            var emb = builder.Build();
            return builder;
        }
    }
}
