﻿using Discord.Commands;
using mBot.Objects.ConfigObjects;
using System.Threading.Tasks;
using Tweetinvi;

namespace mBot.Modules
{
    [Group("twitter")]
    public class TwitterModule : ModuleBase
    {
        [Command("info")]
        public async Task Twitter()
        {
            var auth = new TwitterAuth (TwitterAuth.AuthAccount.mBot);

            Auth.SetUserCredentials(auth.ConsumerKey, auth.ConsumerSecret, auth.userAccessToken, auth.userAccessSecret);
            //ITwitterCredentials creds = new TwitterCredentials(auth.ConsumerKey, auth.ConsumerSecret, auth.userAccessToken, auth.userAccessSecret);
            //Auth.SetCredentials(creds);
            var user = User.GetAuthenticatedUser();
            string reply = "Username: " + user.ScreenName + "\n"
                            + "URL: " + "https://twitter.com/mBotAdmin" + "\n"
                            //+ "Location: " + user.Location + "\n"
                            //+ "ID: " + user.Id + "\n"
                            + "Tweets Count: " + user.StatusesCount + "\n"
                            + "Friends Count: " + user.FriendsCount + "\n"
                            + "Followers Count: " + user.FollowersCount + "\n"
                            + "Favourites Count: " + user.FavouritesCount + "\n"
                            //[Broken!!!] + "Latest Tweet: " + user.Status.Text ?? "No Tweets Yet"
                            ;
            await ReplyAsync(reply);
        }

        [Command("tweet")]
        [RequireBotOwner]
        public async Task Tweet([Remainder] string text)
        {
            var auth = new TwitterAuth(TwitterAuth.AuthAccount.NNA);
            Auth.SetUserCredentials(auth.ConsumerKey, auth.ConsumerSecret, auth.userAccessToken, auth.userAccessSecret);
            var user = User.GetAuthenticatedUser();
            
            var tweet = user.PublishTweet(text);
            
            await ReplyAsync("Tweeted! URL: " + tweet.Url);
        }
    }
}
