﻿using Discord;
using Discord.Commands;
using mBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Modules
{
    public class CreditsModule : ModuleBase
    {
        [Command("redeem")]
        [Alias("redeemcredits")]
        [Summary("Redeems credits from a credit code.")]
        public async Task RedeemCredits(string CreditCode)
        {
            var l = Config.Database.Query<DCreditCode>("SELECT * FROM CreditCodes WHERE Key = ?", CreditCode);
            if (l.Count == 0)
                throw new Exception("Sorry, but that code wasn't found.");
            var c = l[0];
            if (c.Used)
                throw new Exception("Sorry, but that code has already been used.");
            c.Used = true;
            c.UsedAt = DateTime.Now;
            c.UsedBy = Context.User.Id.ToString();
            var u = await Config.GetOrCreateUserAsync(Context.User);
            int oldCredits = u.Credits;
            u.Credits += c.Value;
            Config.Database.Update(u);
            Config.Database.Update(c);
            await ReplyAsync(
                $"You have successfully redeemed a credit code, worth {c.Value} Credits!" + '\n'
                + $"You now have {u.Credits} Credits!");
        }

        [Command("Balance")]
        [Alias("checkbalance", "bal", "$")]
        [Summary("Checks your credit balance.")]
        public async Task CheckBalance(IUser User = null)
        {
            var user = User ?? Context.User;
            var u = await Config.GetOrCreateUserAsync(user);
            await ReplyAsync($"{user.Mention} has {u.Credits} credits");
        }
    }
}
