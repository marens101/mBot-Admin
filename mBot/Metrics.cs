﻿using App.Metrics;
using App.Metrics.Gauge;
using mBot.Internal.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot
{
    public class Metrics
    {
        public static void Init()
        {
            MetricsRoot = new MetricsBuilder()
                .OutputMetrics.AsPrometheusPlainText()
                .Build();

            CurrentActiveGuilds = NewGuage("current_active_guilds");
            TotalGuilds = NewGuage("total_guilds");
            TotalUsers = NewGuage("total_users");
            TotalInfractions = NewGuage("total_infractions");

        }
        public static IMetricsRoot MetricsRoot { get; private set; }

        private static GaugeOptions NewGuage(string name) => new GaugeOptions
            {
                Name = name
            };

        public static GaugeOptions CurrentActiveGuilds { get; set; }
        public static void SetCurrentActiveGuilds(int newValue)
            => MetricsRoot.Measure.Gauge.SetValue(CurrentActiveGuilds, newValue);

        public static GaugeOptions TotalGuilds { get; set; }
        public static void SetTotalGuilds(int newValue)
            => MetricsRoot.Measure.Gauge.SetValue(TotalGuilds, newValue);

        public static GaugeOptions TotalUsers { get; set; }
        public static void SetTotalUsers(int newValue)
            => MetricsRoot.Measure.Gauge.SetValue(TotalUsers, newValue);

        public static GaugeOptions TotalInfractions { get; set; }
        public static void SetTotalInfractions(int newValue)
            => MetricsRoot.Measure.Gauge.SetValue(TotalInfractions, newValue);

        public static void QuickRefresh()
        {
            var uq = Config.Database.QueryToDataTable("SELECT Count(*) FROM Users");
            SetTotalUsers(Convert.ToInt32(uq.Rows[uq.Rows.Count - 1][0]));

            var gq = Config.Database.QueryToDataTable("SELECT Count(*) FROM Guilds");
            SetTotalGuilds(Convert.ToInt32(gq.Rows[gq.Rows.Count - 1][0]));

            var iq = Config.Database.QueryToDataTable("SELECT Count(*) FROM Infractions");
            SetTotalInfractions(Convert.ToInt32(iq.Rows[iq.Rows.Count - 1][0]));
        }

        public static async Task Refresh()
        {
            QuickRefresh();
            // nothing intensive just yet
            await Task.Delay(0); // get rid of that warning
        }

        public static async Task<string> GetForPrometheus()
        {
            var snapshot = MetricsRoot.Snapshot.Get();
            //  var formatter = MetricsRoot.OutputMetricsFormatters.FirstOrDefault(w => w.MediaType.SubType == "vnd.appmetrics.metrics.prometheus");
            var formatter = new App.Metrics.Formatters.Prometheus.MetricsPrometheusTextOutputFormatter();
            using (var stream = new MemoryStream())
            {
                await formatter.WriteAsync(stream, snapshot);

                var result = Encoding.UTF8.GetString(stream.ToArray());

                return result;
            }
        }
    }
}
