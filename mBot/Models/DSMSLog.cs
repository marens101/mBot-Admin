﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("SMSLog")]
    public class DSMSLog
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string FromUserId { get; set; }
        public string ToUserId { get; set; }
        public string ToNumber { get; set; }
        [MaxLength(918)]
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }
        public int CreditCharge { get; set; }
        public string SMSId { get; set; }
    }
}
