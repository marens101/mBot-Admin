﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("InfractionCodes")]
    public class DInfractionCode
    {
        public DInfractionCode()
        {
            Code = Guid.NewGuid().ToString();
        }
        [PrimaryKey, Unique]
        public string Code { get; set; }
        public string UserId { get; set; }
        public string GuildId { get; set; }
        [Ignore]
        public string Url
        {
            get
            {
                return $"https://panel.mbot.party/infractions.php?code=" + Code;
            }
        }
    }
}
