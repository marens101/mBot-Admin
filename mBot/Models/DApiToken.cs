﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("ApiTokens")]
    public class DApiToken
    {
        [Unique, PrimaryKey]
        public string Token { get; set; }
        public string Description { get; set; }

        public bool CanAccessRL { get; set; } = false;
    }
}
