﻿using Discord;
using Discord.Commands;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("PermissionOverrides")]
    public class DPermissionOverride
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string UserId { get; set; }
        public string GuildId { get; set; }
        public string RoleId { get; set; }

        public string Node { get; set; }
        public string Value { get; set; }
        public static bool Evaluate(ICommandContext ctxt, string node)
        {
            IGuildUser igu = ctxt.User as IGuildUser;
            string uid = ctxt.User.Id.ToString();
            string gid = ctxt.Guild.Id.ToString();
            List<string> rids = igu.RoleIds.Cast<string>().ToList();
            var Overrides = Config.Database.Query<DPermissionOverride>(
                "SELECT * FROM PermissionOverrides WHERE GuildId = ? OR UserId = ?",
                ctxt.Guild.Id.ToString(),
                ctxt.User.Id.ToString());

            string[] treeIn = node.ToLower().Split('.');
            foreach (DPermissionOverride o in Overrides)
            {
                string[] tree = o.Node.ToLower().Split('.');
                bool nodeMatches = true;
                int i = 0;
                foreach(string n in tree)
                {
                    if (n != treeIn[i] && n != "*")
                        nodeMatches = false;
                    if (n == "*")
                        break;
                }
                if (nodeMatches)
                {
                    bool userExplicit = o.UserId == uid;
                    bool userAllowed = userExplicit || o.UserId == "*";
                    bool guildExplicit = o.GuildId == gid;
                    bool guildAllowed = guildExplicit || o.UserId == "*";
                    if (userExplicit && guildAllowed)
                        return true;
                    else if (guildExplicit && userAllowed)
                        if (o.RoleId == null)
                            return true;
                        else if (rids.Contains(o.RoleId))
                            return true;
                }
            }
            return false;
        }
    }
}
