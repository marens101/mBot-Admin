﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    public enum InfractionType
    {
        Warn,
        Kick,
        Ban,
        Unban
    }
}
