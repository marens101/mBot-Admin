﻿using Discord;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("Users")]
    public class DUser
    {
        public static async Task<DUser> New(IUser u)
        {
            DUser d = new DUser();
            d.Id = u.Id.ToString();
            d.FirstSeen = DateTime.Now;
            d.Username = u.Username;
            d.Discrim = u.Discriminator;
            d.Credits = 0;
            d.XP = 0;
            d.AvatarUrl = u.GetAvatarUrl() ?? PlaceholderAvatarUrl;
            d.IsStaff = false;
            d.LastSeen = DateTime.Now;
            return d;
        }
        [PrimaryKey, Unique]
        public string Id { get; set; }
        public DateTime FirstSeen { get; set; }
        public DateTime LastSeen { get; set; }
        public string Username { get; set; }
        public string Discrim { get; set; }
        [Ignore]
        public string UsernameDiscrim
        {
            get
            {
                return this.Username + "#" + Discrim;
            }
        }
        [Ignore]
        public string Mention
        {
            get
            {
                return $"<@{Id}>";
            }
        }
        public int Credits { get; set; } = 0;
        [MaxLength(512)]
        public string AvatarUrl { get; set; }
        public int XP { get; set; } = 0;
        public bool IsStaff { get; set; } = false;
        public string PhoneNumber { get; set; }
        public static string PlaceholderAvatarUrl = "https://static.marens101.com/mBot/avatar-not-set.png";
    }
}
