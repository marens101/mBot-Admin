﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("FilteredWords")]
    public class DFilteredWord
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string GuildId { get; set; }
        public string Word { get; set; }
        public bool Active { get; set; }
        public bool IssueWarning { get; set; }
    }
}
