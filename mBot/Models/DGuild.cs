﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace mBot.Models
{
    [Table("Guilds")]
    public class DGuild
    {
        [PrimaryKey, Unique]
        public string Id { get; set; }
        public string Name { get; set; }
        public string OwnerId { get; set; }
        public string Prefix { get; set; } = Config.BotConfig.DefaultPrefix;
        public string AutoRoleId { get; set; }
        public string SuggestionChannelId { get; set; }
        public string ModChanId { get; set; }
        [Ignore]
        public bool UsesLogins
        {
            get
            {
                var q = Config.Database.Query<DLoginConf>("SELECT * FROM LoginConfs WHERE GuildId = ?", Id);
                if (q.Count == 0)
                    return false;
                else
                    return true;
            }
        }
        public List<DFilteredWord> GetFilteredWords(bool OnlyActive = true)
        {
            var query = "SELECT * FROM FilteredWords WHERE Id = ?";
            if (OnlyActive)
                query = "SELECT * FROM FilteredWords WHERE Id = ? AND OnlyActive = 1";
            var res = Config.Database.Query<DFilteredWord>(query, Id);
            return res;
        }
    }
}
