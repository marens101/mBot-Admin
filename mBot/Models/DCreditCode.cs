﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("CreditCodes")]
    public class DCreditCode
    {
        public static DCreditCode New(int Credits)
        {
            var Code = new DCreditCode();
            Code.CreatedAt = DateTime.Now;
            Code.Key = Guid.NewGuid().ToString();
            Code.Used = false;
            Code.Value = Credits;
            
            return Code;
        }
        [PrimaryKey, Unique]
        public string Key { get; set; }
        public int Value { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool Used { get; set; }
        public string UsedBy { get; set; }
        public DateTime UsedAt { get; set; }
    }
}
