﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("RlmeUsers")]
    public class DRlmeUser
    {
        [Unique, PrimaryKey]
        public string Id { get; set; }
        public string UploadUrl { get; set; } = "https://ratelimited.me/";
        [MaxLength(512)]
        public string EncryptedToken { get; set; }
        public byte[] Key { get; set; }
        public byte[] IV { get; set; }
        public bool IsStaff { get; set; }
    }
}
