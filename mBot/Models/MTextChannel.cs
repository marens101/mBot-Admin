﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    public class MTextChannel
    {
        public static MTextChannel New(ITextChannel t)
        {
            MTextChannel d = new MTextChannel();
            d.Id = t.Id;
            d.Name = t.Name;
            d.GuildName = t.Guild.Name;
            d.GuildId = t.GuildId;
            return d;
        }
        public ulong Id;
        public string Name;
        public ulong GuildId;
        public string GuildName;
    }
}
