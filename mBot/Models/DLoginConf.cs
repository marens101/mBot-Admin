﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("LoginConfs")]
    public class DLoginConf
    {
        public string GuildId { get; set; }
        public string LoginChannelId { get; set; }
        public string RoleToAddId { get; set; }
        public string RoleToRemoveId { get; set; }
        public string WebhookId { get; set; }
        public string WebhookToken { get; set; }
        public LoginType Type { get; set; }
        public LoginCodeCreationRequirements CodeCreationRequirements { get; set; }
        public DLoginCode GenerateLoginCode(LoginCodeCreationConfig conf)
        {
            DLoginCode code = new DLoginCode();
            Random rand = new Random();
            string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string str = new string(Enumerable.Repeat(chars, 6)
                  .Select(s => s[rand.Next(s.Length)]).ToArray());
            code.Code = str;
            code.OwnerId = conf.user.Id.ToString();
            code.Uses = 0;
            code.Type = conf.type;
            code.GuildId = GuildId;
            switch (conf.type)
            {
                case LoginCodeType.SingleUseAuth:
                    code.MaxUses = 1;
                    break;
                case LoginCodeType.MultiUseAuth:
                    code.MaxUses = conf.MaxUses;
                    break;
                case LoginCodeType.RefCode:
                    code.MaxUses = null;
                    break;
            }
            return code;
        }
        public string RequiredRole { get; set; }
    }

    public enum LoginCodeCreationRequirements
    {
        None,
        ManageGuild,
        Role
    }

    public enum LoginType
    {
        None,
        RefCode,
        AuthCode,
    }
}
