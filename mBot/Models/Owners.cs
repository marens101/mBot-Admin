﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    public class Owners
    {
        public Owners(IUser AppOwner)
        {
            Init();
            AddOwner(AppOwner);
        }
        public Owners(ulong[] OwnerIds)
        {
            Init();
            foreach (ulong u in OwnerIds)
            {
                try
                {
                    IUser us = Config.Client.GetUser(u);
                    AddOwner(us);
                }
                catch
                {
                    string msg = $"Bot Owner could not be found matching ID {u}";
                    new ErrorHandling.ErrorEvent(msg, LogSeverity.Error).Report();
                }
                
            }
        }
        public void Init()
        {
            List = new List<IUser>();
        }

        public void AddOwner(IUser User)
        {
            List.Add(User);
        }
        public void AddOwner(IUser[] Users)
        {
            foreach (IUser u in Users)
            {
                List.Add(u);
            }
        }
        public void AddOwner(DiscordSocketClient client, ulong UsrId)
        {
            AddOwner(client.GetUser(UsrId) as IUser);
        }
        public void AddOwner(DiscordSocketClient client, ulong[] UsrId)
        {
            foreach (ulong l in UsrId)
            {
                AddOwner(client.GetUser(l) as IUser);
            }
        }

        public List<IUser> List;
        public bool IsOwner(ulong UserId)
        {
            foreach (IUser u in List)
            {
                if (u.Id == UserId)
                {
                    return true;
                }
            }
            return false;
        }
        public bool IsOwner(IUser User)
        {
            foreach (IUser u in List)
            {
                if (u.Id == User.Id)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
