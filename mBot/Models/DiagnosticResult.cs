﻿using Discord;
using mBot.Internal.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    public class DiagnosticResult
    {
        public DiagnosticResult()
        {
            Random rand = new Random();
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            bool AlreadyExists = true;
            string str;
            Mapping = new DiagnosticResultMap()
            {
                Timestamp = DateTime.Now
            };
            while (AlreadyExists)
            {
                str = new string(Enumerable.Repeat(chars, 9)
                  .Select(s => s[rand.Next(s.Length)]).ToArray());
                var t = Config.Database.Query<DiagnosticResultMap>("SELECT DiagnosticCode FROM DiagnosticResults WHERE DiagnosticCode = ?", str);
                AlreadyExists = !(t.Count == 0);
                Mapping.DiagnosticCode = str;
            }
            DiagnosticCode = Mapping.DiagnosticCode;
        }
        public string DiagnosticCode;
        public DUser User;
        public GuildPermissions UserGuildPermissions;
        public DGuild Guild;
        public List<MRole> Roles;
        public GuildPermissions BotGuildPermissions;
        public MTextChannel Channel;
        public ulong ChannelId;
        public ChannelPermissions BotChannelPermissions;
        public Dictionary<string, bool> CommonPerms = new Dictionary<string, bool>();
        public DiagnosticResultMap Mapping;

        public void ScanPerms(bool verbose)
        {
            var CPerms = BotChannelPermissions;
            var GPerms = BotGuildPermissions;
            var UPerms = UserGuildPermissions;

            Process(ref CommonPerms, GPerms.AddReactions, "Guild_AddReactions", verbose);
            Process(ref CommonPerms, GPerms.Administrator, "Guild_Administrator", verbose);
            Process(ref CommonPerms, GPerms.BanMembers, "Guild_Ban", verbose);
            Process(ref CommonPerms, CPerms.CreateInstantInvite, "Channel_CreateInstantInvite", verbose);
            Process(ref CommonPerms, GPerms.CreateInstantInvite, "Guild_CreateInstantInvite", verbose);
            Process(ref CommonPerms, GPerms.KickMembers, "Guild_Kick", verbose);
            Process(ref CommonPerms, CPerms.ManageMessages, "Channel_ManageMessages", verbose);
            Process(ref CommonPerms, GPerms.ManageChannels, "Guild_ManageChannels", verbose);
            Process(ref CommonPerms, GPerms.ManageMessages, "Guild_ManageMessages", verbose);
            Process(ref CommonPerms, GPerms.ManageNicknames, "Guild_ManageNicknames", verbose);
            Process(ref CommonPerms, GPerms.ManageRoles, "Guild_ManageRoles", verbose);
            Process(ref CommonPerms, CPerms.SendMessages, "Channel_SendMessages", verbose);
            Process(ref CommonPerms, GPerms.SendMessages, "Guild_SendMessages", verbose);
            Process(ref CommonPerms, CPerms.UseExternalEmojis, "Channel_UseExternalEmojis", verbose);
            Process(ref CommonPerms, GPerms.UseExternalEmojis, "Guild_UseExternalEmojis", verbose);
        }
        public void Process(ref Dictionary<string, bool> Dict, bool Perm, string PermName, bool Verbose)
        {
            if ((!Perm) || (Perm && Verbose))
            {
                Dict.Add(PermName, Perm);
            }
        }
    }
}
