﻿using Discord;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("GuildSuggestions")]
    class DGuildSuggestion
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string GuildId { get; set; }
        public string PosterId { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Suggestion { get; set; }
        public SuggestionStatus Status { get; set; }
        public string Comment { get; set; }
        public string MessageId { get; set; }

        public static DGuildSuggestion New(DGuild Guild, string msg, IUser owner)
        {
            DGuildSuggestion s = new DGuildSuggestion();
            s.Suggestion = msg;
            s.PosterId = owner.Id.ToString();
            s.TimeStamp = DateTime.Now;
            s.GuildId = Guild.Id;
            s.Status = SuggestionStatus.Pending;
            Config.Database.Insert(s);
            return s;
        }
        public EmbedBuilder BuildEmbed()
        {
            var Owner = Config.Database.Get<DUser>(PosterId);
            EmbedBuilder eb = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = Owner.UsernameDiscrim,
                    IconUrl = Owner.AvatarUrl
                },
                Description = this.Suggestion,
                Title = "Suggestion",
                Timestamp = TimeStamp.ToUniversalTime(),
            };
            eb.AddField("Suggestion ID", this.Id, true);
            eb.AddField("Status", Status.ToString(), true);
            switch (Status)
            {
                case SuggestionStatus.Approved:
                    eb.Color = Color.DarkGreen;
                    break;
                case SuggestionStatus.Completed:
                    eb.Color = Color.Green;
                    break;
                case SuggestionStatus.Denied:
                    eb.Color = Color.Red;
                    break;
                case SuggestionStatus.Pending:
                    eb.Color = new Color(114, 137, 218);
                    break;
                case SuggestionStatus.OnHold:
                    eb.Color = Color.Orange;
                    break;
                case SuggestionStatus.InDiscussion:
                    eb.Color = Color.DarkBlue;
                    break;
            }
            if (Comment != null)
            {
                eb.AddField("Message", Comment);
            }
            else if (Comment == null && Status == SuggestionStatus.Denied)
            {
                eb.AddField("Message", Comment ?? $"Message not set. Denier, please provide a reason with `/ssp {Id} MESSAGE [denial_reason_here]`.");
            }
            return eb;
        }
    }
}
