﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("RlmeLogs")]
    class DRlmeLog
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Action { get; set; }
        public string Input { get; set; }
        public string Output { get; set; }
        public DateTime Timestamp { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
        public string ErrorReason { get; set; }
    }
}
