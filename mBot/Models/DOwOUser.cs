﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("OwOUsers")]
    public class DOwOUser
    {
        [Unique, PrimaryKey]
        public string Id { get; set; }
        public string UploadUrl { get; set; } = "https://owo.whats-th.is/";
        public string ShortenUrl { get; set; } = "https://awau.moe/";
        [MaxLength(512)]
        public string EncryptedToken { get; set; }
        public byte[] Key { get; set; }
        public byte[] IV { get; set; }
    }
}
