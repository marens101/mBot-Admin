﻿using Discord;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("LoginCodes")]
    public class DLoginCode
    {
        [PrimaryKey, Unique]
        public string Code { get; set; }
        [Indexed]
        public string GuildId { get; set; }
        public int Uses { get; set; }
        public string OwnerId { get; set; }
        public int? MaxUses { get; set; }
        public LoginCodeType Type { get; set; }
        [Ignore]
        public bool Expired
        {
            get
            {
                if (Type == LoginCodeType.RefCode)
                    return false;
                else if (MaxUses == null)
                    return false;
                else if (MaxUses <= Uses)
                    return true;
                else
                    return false;
            }
        }
    }

    public struct LoginCodeCreationConfig
    {
        public LoginCodeType type;
        public IUser user;
        public int? MaxUses;
    }

    public enum LoginCodeType
    {
        RefCode,
        SingleUseAuth,
        MultiUseAuth,
    }
}
