﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Models
{
    public class ApiInfraction
    {
        public string Type { get; set; }
        public int InfractionId { get; set; }
        public string Target { get; set; }
        public ulong TargetId { get; set; }
        public string Mod { get; set; }
        public ulong ModId { get; set; }
        public string Reason { get; set; }
        public string Server { get; set; }
        public ulong ServerId { get; set; }
        //public DateTime Timestamp { get; set; }
        public string Timestamp { get; set; }
    }
}
