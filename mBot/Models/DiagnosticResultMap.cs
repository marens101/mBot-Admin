﻿using mBot.ConfigService;
using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("DiagnosticResults")]
    public class DiagnosticResultMap
    {
        [PrimaryKey, Unique]
        public string DiagnosticCode { get; set; }
        public string GuildId { get; set; }
        public string CallerId { get; set; }
        public string FileName { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
