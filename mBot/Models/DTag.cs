﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("Tags")]
    public class DTag
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string GuildId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public int UseCount { get; set; }
        public string OwnerId { get; set; }
    }
}
