﻿using Discord;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    [Table("Infractions")]
    public class DInfraction
    {
        public DInfraction()
        {
            Code = Guid.NewGuid().ToString();
        }
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public InfractionType Type { get; set; }
        public string UserId { get; set; }
        public string ModId { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public DateTime Timestamp { get; set; }
        public string MessageId { get; set; }
        public string GuildId { get; set; }

        public EmbedBuilder GetEmbed()
        {
            var Target = Config.Database.Get<DUser>(UserId);
            var Mod = Config.Database.Get<DUser>(ModId);
            var eb = new EmbedBuilder();
            eb.Author = new EmbedAuthorBuilder()
            {
                IconUrl = Mod.AvatarUrl ?? DUser.PlaceholderAvatarUrl,
                Name = Mod.UsernameDiscrim,
                Url = "https://mbot.party"
            };
            eb.AddField("Reason", Message);
            eb.AddField("Target User", Target.UsernameDiscrim, true);
            eb.AddField("Target User Id", Target.Id, true);
            //eb.AddField("Target User Avatar", Target.AvatarUrl ?? "No Avatar");
            eb.ThumbnailUrl = Target.AvatarUrl ?? DUser.PlaceholderAvatarUrl;
            
            eb.Timestamp = Timestamp;
            switch (Type)
            {
                case InfractionType.Warn:
                    eb.Color = Color.Orange;
                    eb.Title = $"Warn - Case #{Id}";
                    break;
                case InfractionType.Kick:
                    eb.Color = Color.DarkOrange;
                    eb.Title = $"Kick - Case #{Id}";
                    break;
                case InfractionType.Ban:
                    eb.Color = Color.Red;
                    eb.Title = $"Ban - Case #{Id}";
                    break;
                case InfractionType.Unban:
                    eb.Color = Color.Green;
                    eb.Title = $"Unban - Case #{Id}";
                    break;
            }
            return eb;
        }
        public EmbedBuilder GetDmEmbed()
        {
            var Target = Config.Database.Get<DUser>(UserId);
            var Mod = Config.Database.Get<DUser>(ModId);
            var eb = new EmbedBuilder();
            var g = Config.Database.Get<DGuild>(GuildId);
            eb.Author = new EmbedAuthorBuilder()
            {
                IconUrl = Mod.AvatarUrl ?? DUser.PlaceholderAvatarUrl,
                Name = Mod.UsernameDiscrim,
                Url = "https://mbot.party"
            };
            eb.AddField("Reason", Message);
            eb.AddField("From User", Mod.UsernameDiscrim, true);
            eb.AddField("From User Id", Mod.Id, true);
            eb.Timestamp = Timestamp;
            switch (Type)
            {
                case InfractionType.Warn:
                    eb.Color = Color.Orange;
                    eb.Title = $"Warned in {g.Name} - Case #{Id}";
                    break;
                case InfractionType.Kick:
                    eb.Color = Color.DarkOrange;
                    eb.Title = $"Kicked in {g.Name} - Case #{Id}";
                    break;
                case InfractionType.Ban:
                    eb.Color = Color.Red;
                    eb.Title = $"Banned in {g.Name} - Case #{Id}";
                    break;
                case InfractionType.Unban:
                    eb.Color = Color.Green;
                    eb.Title = $"Unbanned in {g.Name} - Case #{Id}";
                    break;
            }
            return eb;
        }
    }
}
