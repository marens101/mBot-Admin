﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Models
{
    public class MRole
    {
        public static MRole New(IRole r)
        {
            MRole d = new MRole();
            d.Name = r.Name;
            d.Id = r.Id;
            d.Permissions = r.Permissions;
            d.Position = r.Position;
            d.GuildName = r.Guild.Name;
            d.GuildId = r.Guild.Id;
            d.IsHoisted = r.IsHoisted;
            d.IsManaged = r.IsManaged;
            d.IsMentionable = r.IsMentionable;
            return d;
        }

        public string Name;
        public ulong Id;
        public string GuildName;
        public ulong GuildId;
        public GuildPermissions Permissions;
        public int Position;
        public bool IsHoisted;
        public bool IsManaged;
        public bool IsMentionable;
    }
}
