﻿using Discord;
using mBot.ConfigService;
using Newtonsoft.Json;
using Rollbar;
using Sentry.Protocol;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot
{
    public class ErrorHandling
    {
        public class ErrorEvent
        {
            public ErrorEvent()
            {

            }
            public ErrorEvent(string StrEvent)
            {
                this.Event = new Exception(StrEvent);
            }
            public ErrorEvent(string StrEvent, LogSeverity ObjLevel)
            {
                this.Event = new Exception(StrEvent);
                this.Level = ObjLevel;
            }
            public ErrorEvent(string StrEvent, LogSeverity ObjLevel, Breadcrumb ObjBreadcrumb)
            {
                this.Event = new Exception(StrEvent);
                this.Level = ObjLevel;
                this.Breadcrumb = ObjBreadcrumb;
            }

            public ErrorEvent(Exception Exc)
            {
                this.Event = Exc;
            }
            public ErrorEvent(Exception Exc, LogSeverity ObjLevel)
            {
                this.Event = Exc;
                this.Level = ObjLevel;
            }
            public ErrorEvent(Exception Exc, LogSeverity ObjLevel, Breadcrumb ObjBreadcrumb)
            {
                this.Event = Exc;
                this.Level = ObjLevel;
                this.Breadcrumb = ObjBreadcrumb;
            }

            public LogSeverity? Level;
            public Exception Event;
            public Breadcrumb Breadcrumb;
        }

        public class mRollbar
        {
            public mRollbar()
            {
                Init();
            }
            public static void Init()
            {
                var RollConfig =
                    new RollbarConfig(Config.BotConfig.RollbarToken)
                    {
                        Environment = "Config.BotConfig.Environment"
                    };
                RollbarLocator.RollbarInstance
                    .Configure(RollConfig);
            }
            public static void Report(ErrorEvent Event)
            {
                try
                {
                    RollbarLocator.RollbarInstance.Log(
                        Event.Level?.ToRollbar() ?? Rollbar.ErrorLevel.Error, 
                        Event.Event);
                }
                catch
                {

                }
            }
            public static void Report(string msg)
            {
                try
                {
                    RollbarLocator.RollbarInstance.Log(
                        Rollbar.ErrorLevel.Error,
                        msg);
                }
                catch
                {

                }
            }
            public static void Report(string msg, LogSeverity level)
            {
                try
                {
                    RollbarLocator.RollbarInstance.Log(
                        level.ToRollbar(),
                        msg);
                }
                catch
                {

                }
            }
            public static void Report(Exception e)
            {
                try
                {
                    RollbarLocator.RollbarInstance.Log(
                        Rollbar.ErrorLevel.Error,
                        e);

                }
                catch
                {
                    
                }
            }
            public static void Report(Exception e, LogSeverity level)
            {
                try
                {
                    RollbarLocator.RollbarInstance.Log(
                        level.ToRollbar(),
                        e);
                }
                catch
                {

                }
            }
        }
        /*public class mSentry
        {
            public static void Init()
            {
                var rClient = new RavenClient(Config.BotConfig.SentryDSN);
                rClient.Environment = Config.BotConfig.Environment;
                rClient.Release = Config.Version;
                Config.RavenClient = rClient;
            }
            public static void Report(ErrorEvent Event)
            {
                var sevent = new SentryEvent(Event.Event);
                sevent.Level = Event.Level?.ToSentry() ?? SharpRaven.Data.ErrorLevel.Error;
                Config.RavenClient.Capture(sevent);
            }
            public static void Report(string msg)
            {
                var smsg = new SentryMessage(msg);
                var sevent = new SentryEvent(smsg);
                Config.RavenClient.Capture(sevent);

            }
            public static void Report(string msg, LogSeverity level)
            {
                var severity = level.ToSentry();
                var smsg = new SentryMessage(msg, severity);
                var sevent = new SentryEvent(smsg);
                Config.RavenClient.Capture(sevent);

            }
            public static void Report(Exception e)
            {
                var sevent = new SentryEvent(e);
                Config.RavenClient.Capture(sevent);
            }
            public static void Report(Exception e, LogSeverity level)
            {
                var severity = level.ToSentry();
                var sevent = new SentryEvent(e);
                sevent.Level = severity;
                Config.RavenClient.Capture(sevent);
            }
        }*/
    }
    public static class ErrorHandlingExtensions
    {
        public static /*SharpRaven.Data.ErrorLevel*/Object ToSentry(this LogSeverity Level)
        {

            //if (Level == LogSeverity.Critical)
            //    return SharpRaven.Data.ErrorLevel.Fatal;
            //else if (Level == LogSeverity.Error)
            //    return SharpRaven.Data.ErrorLevel.Error;
            //else if (Level == LogSeverity.Warning)
            //    return SharpRaven.Data.ErrorLevel.Warning;
            //else if (Level == LogSeverity.Error)
            //    return SharpRaven.Data.ErrorLevel.Info;
            //else if (Level == LogSeverity.Debug)
            //    return SharpRaven.Data.ErrorLevel.Debug;
            //else
            //    return SharpRaven.Data.ErrorLevel.Error;
            return null;
        }
        public static Rollbar.ErrorLevel ToRollbar(this LogSeverity Level)
        {
            if (Level == LogSeverity.Critical)
                return Rollbar.ErrorLevel.Critical;
            else if (Level == LogSeverity.Error)
                return Rollbar.ErrorLevel.Error;
            else if (Level == LogSeverity.Warning)
                return Rollbar.ErrorLevel.Warning;
            else if (Level == LogSeverity.Error)
                return Rollbar.ErrorLevel.Info;
            else if (Level == LogSeverity.Debug)
                return Rollbar.ErrorLevel.Debug;
            else
                return Rollbar.ErrorLevel.Error;
        }

        public static void Report(this ErrorHandling.ErrorEvent Event)
        {
            ErrorHandling.mRollbar.Report(Event);
            //ErrorHandling.mSentry.Report(Event);
        }
        public static void RollbarReport(this ErrorHandling.ErrorEvent Event)
        {
            ErrorHandling.mRollbar.Report(Event);
        }
        public static void SentryReport(this ErrorHandling.ErrorEvent Event)
        {
            //ErrorHandling.mSentry.Report(Event);
        }
    }
}
