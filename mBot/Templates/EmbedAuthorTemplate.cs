﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;

namespace mBot.Templates
{
    public class EmbedAuthorTemplate
    {
        public static Discord.EmbedAuthorBuilder DefaultEmbedAuthor(IApplication application = null)
        {
            application = application ?? Config.Application;
            EmbedAuthorBuilder builder = new EmbedAuthorBuilder
            {
                Name = application.Name,
                IconUrl = application.IconUrl,
                Url = "http://rb.marens101.com/m101bot"
            };
            //var auth = builder.Build();
            return builder;
        }
    }
}
