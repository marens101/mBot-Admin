﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot
{
    public static class CommandsList
    {
        public static List<Module> Modules = new List<Module>
        {
            new Module("Credits", "redeem", "balance"),
            new Module("Fun", "joke", "flipcoin", "roledice", "decide", "ship"),
            new Module("Help", "help", "diagnose"),
            new Module("Misc", "info", "support", "ping", "whois", "prefix"),
            new Module("Mod", "ban", "kick", "warn", "modlog", "infsearch"),
            new Module("Server", "server newchan", "server invite", "server info", 
                "server giverole", "server takerole", "suggest", "ssp"),
            new Module("Tags", "tag", "tag add", "tag remove", "tag list"),
            new Module("No Self Promo", "nsp instagram", "nsp sector new", 
                "nsp sector stats", "nsp user new", "nsp quota check", 
                "nsp quota add", "nsp quota reset")

            //new Module("", "", ""),

        };
        public class Module
        {
            public Module(string ModuleName, params string[] Commands)
            {
                this.ModuleName = ModuleName;
                this.Commands = Commands.ToList().OrderBy(c => c).ToList();
            }
            public string ModuleName { get; set; }
            public List<string> Commands { get; set; }
        }
    }
}