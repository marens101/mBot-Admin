using Discord;
using Discord.Commands;
using mBot.ConfigService;
using mBot.Internal.Extensions;
using mBot.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Intergrations.RLME
{
    [Group("RLME")]
    [Alias("RL")]
    public class DIModule : ModuleBase
    {
        [Command()]
        [Priority(-1)]
        public async Task IndexCmd([Remainder] string CatchAll = null)
        {
            await ReplyAsync("RATELIMITED (or RLME) is a POMF-Compatible file host. The main website is at https://ratelimited.me, and the invite to the Discord Server can be found there. You can sign up at https://ratelimited.me/signup.");
        }

        [Group("set")]
        public class Set : ModuleBase
        {
            [Command()]
            [Priority(-1)]
            public async Task ListSetCmds([Remainder] string CatchAll = null)
            {
                await ReplyAsync("Availiable subcommands under `/RLME Set`: `token`, `t`, `uploader`, `u`");
            }


            [Command("Token")]
            [Alias("t")]
            [Priority(1)]
            public async Task SetToken(string input)
            {
                if (Context.Channel is IDMChannel)
                    await new RlmeService().SetToken(Context, Context.User, input);
                else
                {
                    var x = await ReplyAsync(Context.User.Mention + " Uhhh... For security, we only allow tokens to be set from within DM channels. Sorry!");
                    try
                    {
                        await Context.Message.DeleteAsync();
                    }
                    catch
                    {
                        await x.ModifyAsync(m => m.Content =
                            "Uhhh...For security, we only allow tokens to be set from within DM channels. Sorry!" + "\n"
                            + "Also, it appears I don't have the permissions to delete your message containing your token. You might want to get rid of that!");
                    }
                }
            }

            [Command("DefaultUploader")]
            [RequireRLMEAccount]
            [Alias("u", "uploader", "baseupload")]
            [Priority(1)]
            public async Task SetUpload(string input)
            {
                await new RlmeService().SetDefaultUploader(Context, Context.User, input);
            }
        }

        [Command("Reset")]
        [RequireRLMEAccount]
        [Priority(1)]
        public async Task Reset()
        {
            Config.Database.Delete<DRlmeUser>(Context.User.Id.ToString());
            await ReplyAsync("Your RLME Account and all configuration has been removed from our servers");
        }

        [Command("Colour")] //Go Aussie Spelling!
        [Alias("Color")]    //Fine, americans can have their special spelling *sigh*.
        [RequireRLMEMainGuild]
        [RequireRLMEStaff]
        public async Task ApplyColourRole(string HexIn, IUser user)
        {
            if (!(HexIn.TrimStart('#').Length == 6))
                throw new System.Exception("Not a valid hex code.");
            IGuild g = Context.Guild;
            var AllRoles = g.Roles;
            string hex;
            if (HexIn.StartsWith("#"))
                hex = HexIn.TrimStart('#').ToUpper();
            else
                hex = HexIn.ToUpper();
            IRole role = AllRoles.FirstOrDefault(f => f.Name == $"Colour: #{hex}");
            if (role == null)
            {
                Discord.Color c = (Discord.Color)ColorTranslator.FromHtml("#" + hex);
                role = await Context.Guild.CreateRoleAsync($"Colour: #{hex}", color: c, isMentionable: false);
                try
                {
                    IRole hasRole = AllRoles.FirstOrDefault(r => r.Id == RLMEConfig.StaticSav.HasRoleId);
                    if (hasRole == null)
                        throw new Exception("Could not locate the `Has Token` role.");
                    await role.ModifyAsync(d => d.Position = (hasRole.Position + 1));
                }
                catch(Exception e)
                {
                    await ReplyAsync("An error occured while attempting to hoist the new role above the `Has Token` role." + "\n"
                        + "Error Details: " + e.Message);
                }

            }

            await (user as IGuildUser).AddRoleAsync(role);
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }

        [Command("has")]
        [RequireRLMEStaff]
        [RequireRLMEGuild]
        [Priority(1)]
        public async Task SetUserHasTokenRole(IUser user)
        {
            var g = await Context.Client.GetGuildAsync(RLMEConfig.StaticSav.PublicGuildId);
            IRole role = g.GetRole(RLMEConfig.StaticSav.HasRoleId);
            await (user as IGuildUser).AddRoleAsync(role);
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }

        [Command("GetUser")]
        [RequireRLMEStaff]
        public async Task GetUser(IUser user)
        {
            var g = await Context.Client.GetGuildAsync(RLMEConfig.StaticSav.PublicGuildId);
            var usr = await g.GetUserAsync(user.Id);
            if (usr == null)
            {
                await ReplyAsync("That user wasn't found within the main RLME guild.");
            }
            else
            {
                await ReplyAsync("That user is in the RLME Main Guild!");
            }
        }

        [Command("AddStaff")]
        [RequireRLMELT]
        [Priority(1)]
        public async Task AddStaff(IUser user)
        {
            var u = Config.Database.Get<DRlmeUser>(user.Id);
            u.IsStaff = true;
            Config.Database.Update(u);
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }

        [Command("suggest")]
        [RequireRLMEGuild]
        [Priority(1)]
        public async Task Suggest([Remainder] string msg)
        {
            var suggestChan = await Context.Guild.GetTextChannelAsync(RLMEConfig.StaticSav.SuggestionChanId);
            var Suggestion = await RLMEObjects.Suggestion.New(msg, Context.User);
            var em = Suggestion.BuildEmbed().Build();
            var smsg = await suggestChan.SendMessageAsync("", embed: em);
            Suggestion.MsgId = smsg.Id;
            RLMEConfig.StaticSav.Suggestions[Suggestion.SuggestionId] = Suggestion;
            RLMEConfig.Save();
            Emote voteYes = Emote.Parse("<:DBLTickYes:445878626941141004>");
            Emote voteNo = Emote.Parse("<:DBLCrossNo:445878691046883330>");
            await smsg.AddReactionAsync(voteYes);
            await smsg.AddReactionAsync(voteNo);
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }

        [Command("suggestprop")]
        [Alias("ssuggest", "ssp", "setsuggestprop")]
        [RequireRLMEMainGuild]
        [RequireRLMEStaff]
        public async Task SetSuggest(int suggestionId, string property, [Remainder] string status)
        {
            var Suggestion = RLMEConfig.StaticSav.Suggestions[suggestionId];
            if(Suggestion == null)
            {
                throw new Exception("Suggestion Not Found");
            }
            var suggestChan = await Context.Guild.GetTextChannelAsync(RLMEConfig.StaticSav.SuggestionChanId);
            string pUpper = property.ToUpper();
            if (pUpper == "STATUS")
            {
                string sUpper = status.ToUpper();
                if (sUpper == "APPROVED")
                    Suggestion.Status = RLMEObjects.SuggestionStatus.Approved;
                else if(sUpper == "COMPLETED")
                    Suggestion.Status = RLMEObjects.SuggestionStatus.Completed;
                else if (sUpper == "DENIED")
                    Suggestion.Status = RLMEObjects.SuggestionStatus.Denied;
                else if (sUpper == "PENDING")
                    Suggestion.Status = RLMEObjects.SuggestionStatus.Pending;
                else if (sUpper == "ONHOLD")
                    Suggestion.Status = RLMEObjects.SuggestionStatus.OnHold;
                else if (sUpper == "INDISCUSSION")
                    Suggestion.Status = RLMEObjects.SuggestionStatus.InDiscussion;
                else
                    throw new Exception("Invalid status. Valid options are `APPROVED`, `COMPLETED`, `DENIED`, `PENDING`, `ONHOLD` and `INDISCUSSION`.");
            }
            else if(pUpper == "DREASON")
            {
                Suggestion.DenialReason = status;
            }
            else
            {
                throw new Exception("Property Not Found. Valid options are `STATUS` and `DREASON` (denial reason)");
            }
            var em = Suggestion.BuildEmbed();
            var smsg = await suggestChan.GetMessageAsync(Suggestion.MsgId);
            var msg = smsg as IUserMessage;
            await msg.ModifyAsync(m => m.Embed = em.Build());
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }


        [Command("RemStaff")]
        [RequireRLMELT]
        [Priority(1)]
        public async Task RemoveStaff(IUser user)
        {
            var u = Config.Database.Get<DRlmeUser>(user.Id);
            u.IsStaff = false;
            Config.Database.Update(u);
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }

        [Command("upload")]
        [Alias("u")]
        [RequireRLMEAccount]
        [Priority(1)]
        public async Task Upload(/*string url = null*/)
        {
            var attachment = Context.Message.Attachments.FirstOrDefault();
            if (attachment.Size > 20 * 1024 * 1024)
                throw new Exception("File Too Large: Must be smaller than 20mb");

            using (WebClient client = new WebClient())
            {
                byte[] data = client.DownloadData(attachment.Url);
                await new RlmeService().UploadFile(Context, data, attachment.Filename);
            }
        }

        [Command("approve")]
        [RequireRLMEStaff]
        [RequireRLMEMainGuild]
        [Priority(1)]
        public async Task Approve(IGuildUser user)
        {
            var guild = Context.Guild as IGuild;
            var role = Context.Guild.GetRole(RLMEConfig.StaticSav.HasRoleId);
            await user.AddRoleAsync(role);
            var modlog = (await Context.Guild.GetTextChannelsAsync()).FirstOrDefault(c => c.Id == RLMEConfig.StaticSav.TokenLogsChanId);
            await modlog.SendMessageAsync(
                "A token was approved!" + '\n'
                + $"Requestor: {user.Mention}" + '\n'
                + $"Granter: {Context.User.Mention}");
        }

        [Command("deny")]
        [RequireRLMEStaff]
        [RequireRLMEMainGuild]
        [Priority(1)]
        public async Task Deny(IGuildUser user, [Remainder] string reason)
        {
            var guild = Context.Guild as IGuild;
            var role = Context.Guild.GetRole(RLMEConfig.StaticSav.HasRoleId);
            await user.AddRoleAsync(role);
            var modlog = (await Context.Guild.GetTextChannelsAsync()).FirstOrDefault(c => c.Id == RLMEConfig.StaticSav.TokenLogsChanId);
            await modlog.SendMessageAsync(
                    "A token was denied!" + '\n'
                    + $"Requestor: {user.Username}#{user.Discriminator} ({user.Id})" + '\n'
                    + $"Denier: {Context.User.Mention}" + '\n'
                    + $"Reason: {reason ?? "*No reason provided*"}"
                    );
        }


    }
}
