﻿using Discord;
using Discord.Commands;
using mBot.ConfigService;
using mBot.Intergrations.Pomf;
using mBot.Internal.Extensions;
using mBot.Models;
using mBot.Objects.EncryptionObjects;
using mBot.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Intergrations.RLME
{
    public class RlmeService
    {
        public async Task SetToken(ICommandContext Context, IUser user, string token)
        {
            var tl = Config.Database.Query<DRlmeUser>("SELECT * FROM RlmeUsers WHERE Id = ?", user.Id.ToString());
            if (tl.Count == 0)
            {
                DRlmeUser tu = new DRlmeUser();
                tu.Id = user.Id.ToString();
                Config.Database.Insert(tu);
            }
            DRlmeUser UserConf = Config.Database.Get<DRlmeUser>(user.Id.ToString());
            AesObjects.Config aesConfig;
            string EncryptedToken = new EncryptionService().AesEncrypt(token, out aesConfig);
            UserConf.Key = aesConfig.CreateAes().Key;
            UserConf.IV = aesConfig.CreateAes().IV;
            UserConf.EncryptedToken = EncryptedToken;
            Config.Database.Update(UserConf);
            await Context.Channel.SendMessageAsync("Your token has been set and encrypted successfully. Your encrypted token is: " + UserConf.EncryptedToken + "\n"
                + "If you ever feel the need to delete your token and all configuration, you can run \"`/rlme reset`\" to delete them from our servers immediately");
        }

        public async Task SetDefaultUploader(ICommandContext Context, IUser user, string newLink)
        {
            DRlmeUser UserConf = Config.Database.Get<DRlmeUser>(user.Id.ToString());
            UserConf.UploadUrl = newLink;
            Config.Database.Update(UserConf);
            await Context.Channel.SendMessageAsync("Your new default file uploader is: `" + UserConf.UploadUrl + "`");
        }

        public async Task UploadFile(ICommandContext Context, byte[] data, string fname)
        {
            string result = await UploadFileAsync(data, fname, Context.User);
            await Context.Channel.SendMessageAsync(result);
        }

        public string StripHTTP(string baseUrl)
        {
            string t1 = baseUrl.Replace("https://", "");
            string t2 = t1.Replace("http://", "");
            return t2;
        }

        private string GetToken(IUser user, out DRlmeUser conf)
        {
            conf = Config.Database.Get<DRlmeUser>(user.Id.ToString());
            if (conf == null)
            {
                throw new Exception("Token Not Set.");
            }
            string DecryptedToken = new EncryptionService().AesDecrypt(conf.EncryptedToken, new AesObjects.Config(conf.Key, conf.IV));
            return DecryptedToken;
        }

        public async Task<string> UploadFileAsync(byte[] data, string filename, IUser user)
        {
            var dl = data.Length;
            if (dl > 20 * 1024 * 1024 || dl < 0)
                throw new ArgumentException("The data needs to be less than 20MiB and greater than 0B long.");

            var fn = Path.GetFileName(filename);
            var Pomf = new PomfClient(
                ApiUrlStrings.Intergrations.RLME.UploadFileEndpoint, 
                GetToken(user, out DRlmeUser conf)
                );


            var resp = await Pomf.UploadAsync(data, fn);

            if (resp.success)
            {
                string configUrl = StripHTTP(conf.UploadUrl);
                if (!configUrl.EndsWith("/"))
                    configUrl = configUrl + "/";
                string text = resp.files.FirstOrDefault().url;
                string customRepl = text.Replace(configUrl, text);
                string snipHttps = StripHTTP(customRepl);
                string final = "https://" + configUrl + snipHttps;

                DRlmeLog Log = new DRlmeLog()
                {
                    Input = fn,
                    HttpStatusCode = resp.Response.StatusCode,
                    Output = final,
                    UserId = user.Id.ToString(),
                    Timestamp = DateTime.Now,
                    Action = "UPLOAD"
                };
                //Config.Database.Insert(Log);
                return final;
            }
            else
            {
                string error = resp.Response.Content;
                DRlmeLog Log = new DRlmeLog
                {
                    Input = fn,
                    ErrorReason = error,
                    UserId = user.Id.ToString(),
                    HttpStatusCode = resp.Response.StatusCode,
                    Timestamp = DateTime.Now,
                    Action = "UPLOAD"
                };
                //Config.Database.Insert(Log);
                return $"The API returned an error {Convert.ToInt32(resp.Response.StatusCode)}: {resp.Response.StatusCode}" + "\n"
                    + $"Error Message: \"`{error}`\"";
            }

        }
    }
}
