using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mBot.ConfigService;
using System.IO;
using Newtonsoft.Json;

namespace mBot.Intergrations.RLME
{
    /// <summary>
    /// Config Class, including the static current config.
    /// </summary>
    public class RLMEConfig
    {
        public static void Load()
        {
            if (!File.Exists(Paths.RLME.RlmeConfig))
            {
                var ttconf = new RLMEConfSav()
                {
                    AsaId = 367747734373007362,
                    HasRoleId = 363509277521870850,
                    InternalGuildId = 373757257286156302,
                    LtRoleId = 368783801020579841,
                    PublicGuildId = 363508876164726795,
                    ModLogChanID = 363509296790765568,
                    TokenLogsChanId = 433988541765386240,
                    SuggestionChanId = 385816817488232449,
                    Suggestions = new Dictionary<int, RLMEObjects.Suggestion>(),
                    LatestSuggestionId = 0,
                };
                File.WriteAllText(Paths.RLME.RlmeConfig, JsonConvert.SerializeObject(ttconf));
            }
            string json = File.ReadAllText(Paths.RLME.RlmeConfig);
            RLMEConfSav tconf = JsonConvert.DeserializeObject<RLMEConfSav>(json);
            StaticSav = tconf;
            LastRefreshAt = DateTime.Now;
        }

        public static void Save()
        {
            File.WriteAllText(Paths.RLME.RlmeConfig, JsonConvert.SerializeObject(StaticSav));
        }

        public static RLMEConfSav StaticSav;
        public static bool HasLoaded
            = false;
        public static DateTime LastRefreshAt;
    }

    /// <summary>
    /// The Config Object. NOT the static config, that is within RLMEConfig. This is a save file.
    /// </summary>
    public class RLMEConfSav
    {
        public ulong PublicGuildId;
        public ulong InternalGuildId;
        public ulong HasRoleId;
        public ulong AsaId;
        public ulong LtRoleId;
        public ulong ModLogChanID;
        public ulong SuggestionChanId;
        public ulong TokenLogsChanId;
        public Dictionary<int, RLMEObjects.Suggestion> Suggestions;
        public int LatestSuggestionId;
    }
}
