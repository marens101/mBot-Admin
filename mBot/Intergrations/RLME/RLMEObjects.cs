﻿using Discord;
using mBot.ConfigService;
using mBot.Objects.EncryptionObjects;
using mBot.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using mBot.Models;

namespace mBot.Intergrations.RLME
{
    public class RLMEObjects
    {
        public class UploadedFile
        {
            public IUser User;
            public string InputFileName;
            public string ResultUrl;
            public string TimeStamp_Iso;
            public HttpStatusCode StatusCode;
            public string ErrorReason;
        }

        public class Suggestion
        {
            public string Message;
            public DUser Owner;
            public DateTime creationTStamp;
            public DateTime lastUpdatedTStamp;
            public ulong MsgId;
            public int SuggestionId;
            public string DenialReason;
            public SuggestionStatus Status;
            public static async Task<Suggestion> New(string msg, IUser owner)
            {
                Suggestion s = new Suggestion();
                s.Message = msg;
                s.Owner = await Config.GetOrCreateUserAsync(owner);
                s.creationTStamp = DateTime.Now;
                s.lastUpdatedTStamp = DateTime.Now;
                RLMEConfig.StaticSav.LatestSuggestionId++;
                s.SuggestionId = RLMEConfig.StaticSav.LatestSuggestionId;
                return s;
            }
            public EmbedBuilder BuildEmbed()
            {
                EmbedBuilder eb = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        Name = this.Owner.UsernameDiscrim,
                        IconUrl = this.Owner.AvatarUrl
                    },
                    Description = this.Message,
                    Title = "Suggestion",
                    Timestamp = creationTStamp,
                };
                eb.AddField("Suggestion ID", this.SuggestionId.ToString(), true);
                eb.AddField("Status", Status.ToString(), true);
                switch (Status)
                {
                    case SuggestionStatus.Approved:
                        eb.Color = Color.DarkGreen;
                        break;
                    case SuggestionStatus.Completed:
                        eb.Color = Color.Green;
                        break;
                    case SuggestionStatus.Denied:
                        eb.Color = Color.Red;
                        eb.AddField("Denial Reason", DenialReason ?? $"Reason not set. Denier, please provide a reason with `/rlme ssp {SuggestionId} DREASON [denial_reason_here]`.");
                        break;
                    case SuggestionStatus.Pending:
                        eb.Color = new Color(114, 137, 218);
                        break;
                    case SuggestionStatus.OnHold:
                        eb.Color = Color.Orange;
                        break;
                    case SuggestionStatus.InDiscussion:
                        eb.Color = Color.Blue;
                        break;
                }
                return eb;
            }
        }
        public enum SuggestionStatus
        {
            Pending,
            Approved,
            Denied,
            Completed,
            OnHold,
            InDiscussion
        }
    }
}
