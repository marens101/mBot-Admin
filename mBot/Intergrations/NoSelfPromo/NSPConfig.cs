﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Intergrations.NoSelfPromo
{
    public class NSPConfig
    {
        public static void Init()
        {
            if(!File.Exists(Paths.NSP.ConfigJson))
            {
                var tc = new NSPJConfig();
                tc.Admins = new List<long>()
                {
                    264090266766409728, // brvken#8978
                    267098766232911882  //marens101#4158
                };
                tc.Quota = 5;
                tc.Save();
            }
            var conf = NSPJConfig.Load();
            LoadConfig(conf);
            Database = new SQLiteConnection(Paths.NSP.Database);
            //Database.CreateTable<NSPSector>();
            Database.CreateTable<NSPUser>();
        }
        public static SQLiteConnection Database { get; set; }
        public static List<long> Admins { get; internal set; }
        public static int Quota { get; internal set; }
        public static void LoadConfig(NSPJConfig conf)
        {
            Quota = conf.Quota;
            Admins = conf.Admins;
        }
    }

    public class NSPJConfig
    {
        public List<long> Admins { get; set; }
        public int Quota { get; set; }
        public void Save()
        {
            string json = JsonConvert.SerializeObject(this);
            File.WriteAllText(Paths.NSP.ConfigJson, json);
        }
        public static NSPJConfig Load()
        {
            string json = File.ReadAllText(Paths.NSP.ConfigJson);
            var conf = JsonConvert.DeserializeObject<NSPJConfig>(json);
            return conf;
        }
    }
}
