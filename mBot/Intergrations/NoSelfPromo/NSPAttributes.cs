﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Intergrations.NoSelfPromo
{
    public class NSPAttributes
    {
    }
    public class RequireNSPAdmin : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            if (NSPConfig.Admins.Contains((long)Context.User.Id))
            {
                return PreconditionResult.FromSuccess();
            }
            if (Config.BotConfig.Owners[0] == Context.User.Id)
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("Error: You are not registered as an admin.");
            }
        }
    }
    public class RequireNSPSectorLeader : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            if (NSPConfig.Admins.Contains((long)Context.User.Id))
            {
                return PreconditionResult.FromSuccess();
            }
            var sectors = NSPConfig.Database.Query<NSPSector>("SELECT * FROM NSPSector");
            if (sectors.Exists(n => n.LeaderId == (long)Context.User.Id))
            {
                return PreconditionResult.FromSuccess();
            }
            if (Config.BotConfig.Owners[0] == Context.User.Id)
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("Error: You are not registered as an admin.");
            }
        }
    }
    public class RequireNSPSectorLeaderOrGoverner : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            if (NSPConfig.Admins.Contains((long)Context.User.Id))
            {
                return PreconditionResult.FromSuccess();
            }
            var sectors = NSPConfig.Database.Query<NSPSector>("SELECT * FROM NSPSectors");
            if (sectors.Exists(n => n.LeaderId == (long)Context.User.Id))
            {
                return PreconditionResult.FromSuccess();
            }
            var users = NSPConfig.Database.Query<NSPUser>("SELECT * FROM NSPUsers");
            if (users.FirstOrDefault(u => u.Id == (long)Context.User.Id).IsGoverner)
            {
                return PreconditionResult.FromSuccess();
            }
            if (Config.BotConfig.Owners[0] == Context.User.Id)
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("Error: You are not registered as an admin.");
            }
        }
    }
}
