﻿//using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Intergrations.NoSelfPromo
{
    //[Table("NSPSectors")]
    public class NSPSector
    {
        //[PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public long LeaderId { get; set; }
        //[Ignore]
        public int MemberCount
        {
            get
            {
                return Members.Count;
            }
        }
        //[Ignore]
        public List<NSPUser> Members
        {
            get
            {
                var users = 
                    NSPConfig.Database.Query<NSPUser>(
                    "SELECT * FROM NSPUSers WHERE SectorId = ?", Id);
                return users;
            }
        }
    }
}
