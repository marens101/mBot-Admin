﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Intergrations.NoSelfPromo
{
    [Table("NSPUsers")]
    public class NSPUser
    {
        [PrimaryKey]
        public long Id { get; set; }
        //public int SectorId { get; set; }
        //public int QuotaMet { get; set; } = 0;
        public string Instagram { get; set; } = "";
        public bool IsModerator { get; set; } = false;
        public bool IsGoverner { get; set; } = false;
        //public string UserDiscrim { get; set; }
    }
}
