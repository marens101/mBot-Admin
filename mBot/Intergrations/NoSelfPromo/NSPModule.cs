﻿using Discord;
using Discord.Commands;
using mBot.Internal.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Intergrations.NoSelfPromo
{
    [Group("nsp")]
    [RequireGuild(530877993539731487)]
    public class NSPModule : ModuleBase
    {
        [Group("sql")]
        [RequireNSPAdmin]
        public class Sql : ModuleBase
        {
            [Command("select")]
            [Alias("query")]
            public async Task QuerySql([Remainder] string Command)
            {
                DataTable dt = NSPConfig.Database.QueryToDataTable(Command);
                string Table = dt.ToStringTable();
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("**__SQL Query Executed__**");
                sb.AppendLine();
                sb.AppendLine("Query Executed: ");
                sb.AppendLine("```sql");
                sb.AppendLine(Command);
                sb.AppendLine("```");
                sb.AppendLine();
                sb.AppendLine("Result: ");
                sb.AppendLine("```");
                sb.AppendLine(Table);
                sb.AppendLine("```");
                await ReplyAsync(sb.ToString());

            }
            [Command("exec")]
            public async Task ExecSql([Remainder] string Command)
            {
                var res = NSPConfig.Database.Execute(Command);
                var eb = new EmbedBuilder()
                {
                    Title = "SQL Command Executed",
                    Color = Color.Red,
                    Fields =
                {
                    new EmbedFieldBuilder
                    {
                        Name = "Command Executed: ",
                        Value = "```sql" + '\n'
                            + Command + '\n'
                            + "```"
                    },
                    new EmbedFieldBuilder
                    {
                        Name = "Response: ",
                        Value = "```" + '\n'
                            + res + '\n'
                            + "```"
                    }
                }
                };
                await ReplyAsync("", embed: eb.Build());
            }
        }

        /*[Group("quota")]
        public class Quota : ModuleBase
        {
            [Command("set")]
            [RequireNSPAdmin]
            public async Task SetQuota(int num)
            {
                NSPJConfig conf = NSPJConfig.Load();
                int oldQuota = conf.Quota;
                conf.Quota = num;
                conf.Save();
                NSPConfig.LoadConfig(conf);
                await ReplyAsync($"Quota changed from {oldQuota} to {conf.Quota}");
            }
            [Command("reset")]
            [RequireNSPAdmin]
            public async Task ResetQuota()
            {
                int res = NSPConfig.Database.Execute("UPDATE NSPUsers SET QuotaMet = 0");
                await ReplyAsync($"Quota Met set to 0 for {res} records.");
            }
            [Command("add")]
            [RequireNSPSectorLeaderOrGoverner]
            public async Task AddToMet(int num, IUser User)
            {
                var userMatch =
                    NSPConfig.Database.Query<NSPUser>(
                        "SELECT * FROM NSPUsers WHERE Id = ?",
                        (long)User.Id
                        );
                if (userMatch.Count == 0)
                    await ReplyAsync("User Not In Database");
                else
                {
                    var user = userMatch[0];
                    int oldMet = user.QuotaMet;
                    user.QuotaMet += num;
                    NSPConfig.Database.Update(user);
                    if (user.QuotaMet >= NSPConfig.Quota)
                        await ReplyAsync($"Updated from {oldMet} to {user.QuotaMet}." + '\n'
                            + "Quota has been met!");
                    else
                        await ReplyAsync($"Updated from {oldMet} to {user.QuotaMet}." + '\n'
                            + $"{NSPConfig.Quota - user.QuotaMet} to go!");
                }
            }
            [Command("check")]
            public async Task Check(IUser User = null)
            {
                User = User ?? Context.User;
                var userMatch =
                    NSPConfig.Database.Query<NSPUser>(
                        "SELECT * FROM NSPUsers WHERE Id = ?",
                        (long)User.Id
                        );
                if (userMatch.Count == 0)
                    await ReplyAsync("User Not In Database");
                else
                {
                    var user = userMatch[0];
                    await ReplyAsync($"Quota Met: {user.QuotaMet}/{NSPConfig.Quota}");
                }
            }
        }*/

        /*[Group("sector")]
        public class Sector //: ModuleBase
        {
            [Command("new")]
            [RequireNSPAdmin]
            public async Task New(IUser leader)
            {
                var sector = new NSPSector();
                sector.LeaderId = (long)leader.Id;
                NSPConfig.Database.Insert(sector);
                await ReplyAsync($"Sector {sector.Id} created, with leader {leader.Mention}");
            }
            [Command("stats")]
            public async Task Stats()
            {
                var sectors = NSPConfig.Database.Query<NSPSector>(
                    "SELECT * FROM NSPSectors");
                EmbedBuilder eb = new EmbedBuilder()
                {
                    Author = Templates.EmbedAuthorTemplate.DefaultEmbedAuthor(),
                    Color = Color.Blue,
                    Title = "Sector Stats"
                };
                //StringBuilder resp = new StringBuilder();
                //resp.AppendLine("Sector Stats:");
                foreach(NSPSector sector in sectors)
                {
                    var members = sector.Members;
                    var CombinedQuota = members.Count * NSPConfig.Quota;
                    int totalMetQuota = 0, CombinedQuotaAchieved = 0;
                    string metCombinedQuota = "not met";
                    foreach (NSPUser n in members)
                    {
                        if (n.QuotaMet >= NSPConfig.Quota)
                            totalMetQuota++;
                        CombinedQuotaAchieved += n.QuotaMet;
                    }
                    if (CombinedQuotaAchieved >= CombinedQuota)
                        metCombinedQuota = "met";
                    EmbedFieldBuilder efb = new EmbedFieldBuilder()
                    {
                        IsInline = true,
                        Name = "Sector " + sector.Id,
                        Value = "Members: " + members.Count + '\n'
                            //+ $"Group Quota: {CombinedQuotaAchieved}/{CombinedQuota} ({metCombinedQuota})" + '\n'
                            + $"Met Quota: {totalMetQuota}/{members.Count}"
                    };

                    eb.AddField(efb);
                }
                await ReplyAsync("", embed: eb.Build());
            }
        }*/

        [Group("user")]
        [Alias("member")]
        public class User : ModuleBase
        {
            [Command("New")]
            [Alias("Add")]
            [RequireNSPSectorLeaderOrGoverner]
            public async Task New(int sectorId, IUser user)
            {
                var sectorMatch = NSPConfig.Database.Query<NSPSector>(
                    "SELECT * FROM NSPSectors WHERE Id = ?", sectorId);
                if (sectorMatch.Count == 0)
                    await ReplyAsync("Sector Not Found");
                else
                {
                    var sector = sectorMatch[0];
                    var nuser = new NSPUser();
                    nuser.Id = (long)user.Id;
                    //nuser.SectorId = sectorId;
                    //nuser.QuotaMet = 0;
                    NSPConfig.Database.Insert(nuser);
                    await ReplyAsync($"User created"/* + ", with Sector {nuser.SectorId}." + '\n
                        + $"Sector {nuser.SectorId} now  has {sector.MemberCount} members."*/);
                }
                
            }
            [Command("BulkNew")]
            [Alias("BulkAdd")]
            [RequireNSPSectorLeaderOrGoverner]
            public async Task New(int sectorId, params IUser[] users)
            {
                var sectorMatch = NSPConfig.Database.Query<NSPSector>(
                    "SELECT * FROM NSPSectors WHERE Id = ?", sectorId);
                if (sectorMatch.Count == 0)
                    await ReplyAsync("Sector Not Found");
                else
                {
                    var sector = sectorMatch[0];
                    foreach (IUser user in users)
                    {
                        var nuser = new NSPUser();
                        nuser.Id = (long)user.Id;
                        //nuser.SectorId = sectorId;
                        //nuser.QuotaMet = 0;
                        NSPConfig.Database.Insert(nuser);
                    }
                    await ReplyAsync($"{users.Length} users created" /* + ", with Sector {sector.Id}." + '\n'
                        + $"Sector {sector.Id} now  has {sector.MemberCount} members."*/);
                }

            }
            [Command("delete")]
            [RequireNSPSectorLeaderOrGoverner]
            public async Task Delete(IUser User)
            {
                var userMatch =
                    NSPConfig.Database.Query<NSPUser>(
                        "SELECT * FROM NSPUsers WHERE Id = ?",
                        (long)User.Id
                        );
                if (userMatch.Count == 0)
                    await ReplyAsync("User Not In Database");
                else
                {
                    var user = userMatch[0];
                    Config.Database.Delete(user);
                }
            }
        }

        [Group("instagram")]
        public class Instagram : ModuleBase
        {
            [Command()]
            [Alias("set")]
            [Priority(2)]
            public async Task Set(string username)
            {
                var userMatch =
                    NSPConfig.Database.Query<NSPUser>(
                        "SELECT * FROM NSPUsers WHERE Id = ?",
                        (long)Context.User.Id
                        );
                if (userMatch.Count == 0)
                    await ReplyAsync("User Not In Database");
                else
                {
                    var user = userMatch[0];
                    username = username.Trim();
                    username = username.TrimStart('@');
                    username = username.Trim();
                    user.Instagram = username;
                    NSPConfig.Database.Update(user);
                    await ReplyAsync($"Instagram set to @{username}");
                }
            }
            [Command()]
            [Alias("set")]
            [Priority(1)]
            [RequireNSPSectorLeaderOrGoverner]
            public async Task Set(IUser targetUser, string username)
            {
                var userMatch =
                    NSPConfig.Database.Query<NSPUser>(
                        "SELECT * FROM NSPUsers WHERE Id = ?",
                        (long)targetUser.Id
                        );
                if (userMatch.Count == 0)
                    await ReplyAsync("User Not In Database");
                else
                {
                    var user = userMatch[0];
                    username = username.Trim();
                    username = username.TrimStart('@');
                    username = username.Trim();
                    user.Instagram = username;
                    NSPConfig.Database.Update(user);
                    await ReplyAsync($"Instagram set to @{username} for user {targetUser.Username}");
                }
            }
            [Command("check")]
            [Priority(3)]
            public async Task Check(IUser targetUser = null)
            {
                targetUser = targetUser ?? Context.User;
                var userMatch =
                    NSPConfig.Database.Query<NSPUser>(
                        "SELECT * FROM NSPUsers WHERE Id = ?",
                        (long)targetUser.Id
                        );
                if (userMatch.Count == 0)
                    await ReplyAsync("User Not In Database");
                else
                {
                    var user = userMatch[0];
                    await ReplyAsync($"Instagram Username: {user.Instagram}" + '\n'
                        + $"Link: https://instagram.com/{user.Instagram}");
                }
            }
        }
    }
}
