using Discord;
using Discord.Commands;
using mBot.ConfigService;
using mBot.Internal.Extensions;
using mBot.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Intergrations.DiscordImages
{
    [Group("di")]
    //[Alias("DiscordImages")]
    public class DIModule : ModuleBase
    {
        [Command()]
        [Priority(-1)]
        public async Task IndexCmd([Remainder] string CatchAll = null)
        {
            await ReplyAsync("See <https://DiscordImages.com>. Set a token with `/di set token <token>`");
        }

        [Group("set")]
        public class Set : ModuleBase
        {
            [Command()]
            [Priority(-1)]
            public async Task ListSetCmds([Remainder] string CatchAll = null)
            {
                await ReplyAsync("Availiable subcommands under `/di Set`: `token`, `t`, `uploader`, `u`");
            }


            [Command("Token")]
            [Alias("t")]
            [Priority(1)]
            public async Task SetToken(string input)
            {
                if (Context.Channel is IDMChannel)
                    await new DIService().SetToken(Context, Context.User, input);
                else
                {
                    var x = await ReplyAsync(Context.User.Mention + " Uhhh... For security, we only allow tokens to be set from within DM channels. Sorry!");
                    try
                    {
                        await Context.Message.DeleteAsync();
                    }
                    catch
                    {
                        await x.ModifyAsync(m => m.Content =
                            "Uhhh...For security, we only allow tokens to be set from within DM channels. Sorry!" + "\n"
                            + "Also, it appears I don't have the permissions to delete your message containing your token. You might want to get rid of that!");
                    }
                }
            }

            [Command("DefaultUploader")]
            [RequireDIAccount]
            [Alias("u", "uploader", "baseupload")]
            [Priority(1)]
            public async Task SetUpload(string input)
            {
                await new DIService().SetDefaultUploader(Context, Context.User, input);
            }
        }

        [Command("Reset")]
        [RequireDIAccount]
        [Priority(1)]
        public async Task Reset()
        {
            Config.Database.Delete<DRlmeUser>(Context.User.Id.ToString());
            await ReplyAsync("Your DiscordImages Account and all configuration has been removed from our servers");
        }

        [Command("upload")]
        [Alias("u")]
        [RequireDIAccount]
        [Priority(1)]
        public async Task Upload(/*string url = null*/)
        {
            var attachment = Context.Message.Attachments.FirstOrDefault();
            if (attachment.Size > 20 * 1024 * 1024)
                throw new Exception("File Too Large: Must be smaller than 20mb");

            using (WebClient client = new WebClient())
            {
                byte[] data = client.DownloadData(attachment.Url);
                string hash = Convert.ToBase64String(MD5.Create().ComputeHash(data));
                string alphaHash = hash.ToAlphanumeric();
                string shortHash = alphaHash.Substring(0, 6);
                var splitName = attachment.Filename.Split('.');
                string path = $@"BotSystem\Intergrations\OwO\Temp\{shortHash}.{splitName[splitName.Length - 1]}";
                File.WriteAllBytes(path, data);
                await new DIService().UploadFile(Context, path, attachment.Filename);
                File.Delete(path);
            }

        }
    }
}
