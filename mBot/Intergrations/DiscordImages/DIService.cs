using Discord;
using Discord.Commands;
using mBot.ConfigService;
using mBot.Intergrations.Pomf;
using mBot.Internal.Extensions;
using mBot.Models;
using mBot.Objects.EncryptionObjects;
using mBot.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Intergrations.DiscordImages
{
    public class DIService
    {
        public async Task SetToken(ICommandContext Context, IUser user, string token)
        {
            var tl = Config.Database.Query<DDIUser>("SELECT * FROM DIUsers WHERE Id = ?", user.Id.ToString());
            if (tl.Count == 0)
            {
                DDIUser tu = new DDIUser();
                tu.Id = user.Id.ToString();
                Config.Database.Insert(tu);
            }
            DDIUser UserConf = Config.Database.Get<DDIUser>(user.Id.ToString());
            AesObjects.Config aesConfig;
            string EncryptedToken = new EncryptionService().AesEncrypt(token, out aesConfig);
            UserConf.Key = aesConfig.CreateAes().Key;
            UserConf.IV = aesConfig.CreateAes().IV;
            UserConf.EncryptedToken = EncryptedToken;
            Config.Database.Update(UserConf);
            await Context.Channel.SendMessageAsync("Your token has been set and encrypted successfully. Your encrypted token is: " + UserConf.EncryptedToken + "\n"
                + "If you ever feel the need to delete your token and all configuration, you can run \"`/di reset`\" to delete them from our servers immediately");
        }

        public async Task SetDefaultUploader(ICommandContext Context, IUser user, string newLink)
        {
            DDIUser UserConf = Config.Database.Get<DDIUser>(user.Id.ToString());
            UserConf.UploadUrl = newLink;
            Config.Database.Update(UserConf);
            await Context.Channel.SendMessageAsync("Your new default file uploader is: `" + UserConf.UploadUrl + "`");
        }

        public async Task UploadFile(ICommandContext Context, string path, string fname)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                string result = await UploadFileAsync(fs, fname, Context.User);
                await Context.Channel.SendMessageAsync(result);
            }
        }


        public string StripHTTP(string baseUrl)
        {
            string t1 = baseUrl.Replace("https://", "");
            string t2 = t1.Replace("http://", "");
            return t2;
        }

        private string GetToken(IUser user, out DDIUser conf)
        {
            conf = Config.Database.Get<DDIUser>(user.Id.ToString());
            if (conf == null)
            {
                throw new Exception("Token Not Set.");
            }
            string DecryptedToken = new EncryptionService().AesDecrypt(conf.EncryptedToken, new AesObjects.Config(conf.Key, conf.IV));
            return DecryptedToken;
        }

        public async Task<string> UploadFileAsync(Stream s, string filename, IUser user)
        {
            var dl = s.Length - s.Position;
            if (dl > 20 * 1024 * 1024 || dl < 0)
                throw new ArgumentException("The data needs to be less than 20MiB and greather than 0B long.");

            var b64data = new byte[8];
            var rnd = new Random();
            for (var i = 0; i < b64data.Length; i++)
                b64data[i] = (byte)rnd.Next();


            var queryStrings = new Dictionary<string, string>
            {
                ["key"] = GetToken(user, out DDIUser conf)
            };

            var baseUri = new Uri(ApiUrlStrings.Intergrations.DiscordImages.UploadFileEndpoint);
            var uri = baseUri.AttachParameters(queryStrings);

            var req = new HttpRequestMessage(HttpMethod.Post, uri);
            var mpd = new MultipartFormDataContent(string.Concat("---upload-", Convert.ToBase64String(b64data), "---"));

            var bdata = new byte[dl];
            await s.ReadAsync(bdata, 0, bdata.Length);

            var fn = Path.GetFileName(filename);
            var sc = new ByteArrayContent(bdata);
            //sc.Headers.ContentType = new MediaTypeHeaderValue(MimeTypeMap.GetMimeType(Path.GetExtension(fn)));

            mpd.Add(sc, "files[]", string.Concat("\"", fn.ToLower(), "\""));

            req.Content = mpd;

            HttpClient client = new HttpClient();

            var res = await client.SendAsync(req);

            var strRes = await res.Content.ReadAsStringAsync();

            var json = JObject.Parse(strRes);
            await Program.Client_LogAsync(new LogMessage(LogSeverity.Info, "DI API", json.ToString()));
            if ((bool)json["success"])
            {
                var tfn = JsonConvert.DeserializeObject<DIObjects.UploadResponse>(json.ToString());
                string outpDomain = StripHTTP(conf.UploadUrl); //Domain to output the file with. i.e. https:// MYDOMAIN.COM/ file.png
                if (!outpDomain.EndsWith("/"))
                    outpDomain = outpDomain + "/"; //Add trailing slash if not already there
                string fileName = tfn.files.FirstOrDefault().name; //Get the file name. i.e. https:// mydomain.com/ FILE.PNG
                outpDomain = StripHTTP(outpDomain); //Strip https:// and http:// from output domain. i.e. HTTPS:// mydomain.com/ file.png
                string url = "https://" + outpDomain + fileName; // String it together. https:// + mydomain.com/ + file.png
                DDILog Log = new DDILog()
                {
                    Input = filename,
                    HttpStatusCode = res.StatusCode,
                    Output = url,
                    UserId = user.Id.ToString(),
                    Timestamp = DateTime.Now,
                    Action = "UPLOAD"
                };
                Config.Database.Insert(Log);
                return url;
            }
            else
            {
                string error = await res.Content.ReadAsStringAsync();
                Console.WriteLine("DIService.cs Line 126");
                DDILog Log = new DDILog
                {
                    Input = filename,
                    ErrorReason = error,
                    UserId = user.Id.ToString(),
                    HttpStatusCode = res.StatusCode,
                    Timestamp = DateTime.Now,
                    Action = "UPLOAD"
                };
                Console.WriteLine("DIService.cs Line 135");
                Config.Database.Insert(Log);
                return $"The API returned an error {Convert.ToInt32(res.StatusCode)}: {res.StatusCode}" + "\n"
                    + $"Error Message: \"`{error}`\"";

            }
                
        }

    }
}
