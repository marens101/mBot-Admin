﻿using Discord;
using mBot.ConfigService;
using mBot.Objects.EncryptionObjects;
using mBot.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using mBot.Models;

namespace mBot.Intergrations.DiscordImages
{
    public class DIObjects
    {
        public class UploadedFile
        {
            public IUser User;
            public string InputFileName;
            public string ResultUrl;
            public string TimeStamp_Iso;
            public HttpStatusCode StatusCode;
            public string ErrorReason;
        }

        public class UploadResponse
        {
            public bool success { get; set; }
            public List<File> files { get; set; }

            public class File
            {
                public string name { get; set; }
                public string size { get; set; }
                public string url { get; set; }
            }
        }
    }
}
