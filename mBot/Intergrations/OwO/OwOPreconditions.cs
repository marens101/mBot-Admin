﻿using Discord.Commands;
using mBot.ConfigService;
using mBot.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Intergrations.OwO
{
    public class RequireOwOAccount : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            var l = Config.Database.Query<DOwOUser>("SELECT * FROM OwOUsers WHERE Id = ?", Context.User.Id.ToString());
            if (l[0] == null)
            {
                return PreconditionResult.FromError("Error: You have not configured and OwO Account! Get started with `/OwO set token [YOUR_TOKEN_HERE]`." + "\n"
                    + "Don't worry! Your token is stored encrypted using a different key and salt for every user!" + "\n"
                    + "Note: Tokens can only be set in DMs for security purposes.");
            }
            else
            {
                return PreconditionResult.FromSuccess();
            }

        }
    }
}
