﻿using Discord;
using Discord.Commands;
using mBot.ConfigService;
using mBot.Intergrations.OwO;
using mBot.Intergrations.Pomf;
using mBot.Internal.Extensions;
using mBot.Models;
using mBot.Objects.EncryptionObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace mBot.Services.OwOService
{
    public class OwOService
    {
        public async Task SetToken(ICommandContext Context, IUser user, string token)
        {
            var tl = Config.Database.Query<DOwOUser>("SELECT * FROM OwOUsers WHERE Id = ?", user.Id.ToString());
            if (tl.Count == 0)
            {
                DOwOUser tu = new DOwOUser();
                tu.Id = user.Id.ToString();
                Config.Database.Insert(tu);
            }
            DOwOUser UserConf = Config.Database.Get<DOwOUser>(user.Id.ToString());
            UserConf = Config.Database.Get<DOwOUser>(user.Id.ToString());
            AesObjects.Config aesConfig;
            string EncryptedToken = new EncryptionService().AesEncrypt(token, out aesConfig);
            UserConf.Key = aesConfig.CreateAes().Key;
            UserConf.IV = aesConfig.CreateAes().IV;
            UserConf.EncryptedToken = EncryptedToken;
            Config.Database.Update(UserConf);
            await Context.Channel.SendMessageAsync("Your token has been set and encrypted successfully. Your encrypted token is: " + UserConf.EncryptedToken + "\n"
                + "If you ever feel the need to delete your token and all configuration, you can run \"`/owo reset`\" to delete them from our servers immediately");
        }

        public async Task SetDefaultShortener(ICommandContext Context, IUser user, string newLink)
        {
            DOwOUser UserConf = Config.Database.Get<DOwOUser>(user.Id.ToString());
            UserConf.ShortenUrl = newLink;
            Config.Database.Update(UserConf);
            await Context.Channel.SendMessageAsync("Your new default link shortener is: `" + UserConf.ShortenUrl + "`");
        }

        public async Task SetDefaultUploader(ICommandContext Context, IUser user, string newLink)
        {
            DOwOUser UserConf = Config.Database.Get<DOwOUser>(user.Id.ToString());
            UserConf.UploadUrl = newLink;
            Config.Database.Update(UserConf);
            await Context.Channel.SendMessageAsync("Your new default file uploader is: `" + UserConf.UploadUrl + "`");
        }

        public async Task ShortenUrl(ICommandContext Context, string UrlToShorten)
        {
            string result = await ShortenAsync(Context, UrlToShorten);
            await Context.Channel.SendMessageAsync(result);
        }

        public async Task UploadFile(ICommandContext Context, byte[] data, string fname)
        {
            string result = await UploadFileAsync(data, fname, Context.User);
            await Context.Channel.SendMessageAsync(result);
        }

        public async Task<string> ShortenAsync(ICommandContext Context, string UrlToShorten)
        {
            DOwOUser conf;
            string token = GetToken(Context.User, out conf);

            var QueryStrings = new Dictionary<string, string>();
            QueryStrings.Add("action", "shorten");
            QueryStrings.Add("apikey", token);
            QueryStrings.Add("url", UrlToShorten);
            Uri uri = new Uri(ApiUrlStrings.Intergrations.OwO.ShortenUrlEndpoint)
                .AttachParameters(QueryStrings);
            HttpClient httpClient = new HttpClient();
            var res = await httpClient.GetAsync(uri);
            httpClient.Dispose();
            if (res.StatusCode == HttpStatusCode.OK)
            {
                string configUrl = StripHTTP(conf.ShortenUrl);
                if (configUrl.EndsWith("/"))
                    configUrl = configUrl.TrimEnd('/');
                string text = await res.Content.ReadAsStringAsync();
                string customRepl = text.Replace("awau.moe", configUrl);
                string snipHttps = StripHTTP(customRepl);
                string final = "https://" + snipHttps;
                DOwOLog Log = new DOwOLog
                {
                    Input = UrlToShorten,
                    Output = final,
                    UserId = Context.User.Id.ToString(),
                    HttpStatusCode = res.StatusCode,
                    Timestamp = DateTime.Now,
                    Action = "SHORTEN"
                };
                Config.Database.Insert(Log);
                return final;
            }
            else
            {
                string error = await res.Content.ReadAsStringAsync();
                DOwOLog Log = new DOwOLog
                {
                    Input = UrlToShorten,
                    ErrorReason = error,
                    UserId = Context.User.Id.ToString(),
                    HttpStatusCode = res.StatusCode,
                    Timestamp = DateTime.Now,
                    Action = "SHORTEN"
                };
                Config.Database.Insert(Log);
                return $"The API returned an error {Convert.ToInt32(res.StatusCode)}: {res.StatusCode}" + "\n"
                    + $"Error Message: \"`{error}`\"";
            }
        }

        public string StripHTTP(string baseUrl)
        {
            string t1 = baseUrl.Replace("https://", "");
            string t2 = t1.Replace("http://", "");
            return t2;
        }

        private string GetToken(IUser user, out DOwOUser conf)
        {
            try
            {
                conf = Config.Database.Get<DOwOUser>(user.Id.ToString());
                string DecryptedToken = new EncryptionService().AesDecrypt(conf.EncryptedToken, new AesObjects.Config(conf.Key, conf.IV));
                return DecryptedToken;
            }
            catch
            {
                throw new Exception("Token Not Set.");
            }
        }
        /// <summary>
        /// Asynchronously uploads a file to whats-th.is
        /// </summary>
        /// <param name="data">Data to upload.</param>
        /// <param name="filename">Name of the file to upload.</param>
        /// <returns>Response from whats-th.is</returns>
        public async Task<string> UploadFileAsync(byte[] data, string filename, IUser user)
        {
            var dl = data.Length;
            if (dl > 20 * 1024 * 1024 || dl < 0)
                throw new ArgumentException("The data needs to be less than 20MiB and greater than 0B long.");

            var fn = Path.GetFileName(filename);
            var Pomf = new PomfClient(
                ApiUrlStrings.Intergrations.RLME.UploadFileEndpoint,
                GetToken(user, out DOwOUser conf)
                );


            var resp = await Pomf.UploadAsync(data, fn);
           

            if (resp.success)
            {
                string configUrl = StripHTTP(conf.UploadUrl);
                if (!configUrl.EndsWith("/"))
                    configUrl = configUrl + "/";
                string text = resp.files.FirstOrDefault().url;
                string customRepl = text.Replace(configUrl, text);
                string snipHttps = StripHTTP(customRepl);
                string final = "https://" + configUrl + snipHttps;

                DOwOLog Log = new DOwOLog()
                {
                    Input = fn,
                    HttpStatusCode = resp.Response.StatusCode,
                    Output = final,
                    UserId = user.Id.ToString(),
                    Timestamp = DateTime.Now,
                    Action = "UPLOAD"
                };
                Config.Database.Insert(Log);
                return final;
            }
            else
            {
                string error = resp.Response.Content;
                DOwOLog Log = new DOwOLog
                {
                    Input = fn,
                    ErrorReason = error,
                    UserId = user.Id.ToString(),
                    HttpStatusCode = resp.Response.StatusCode,
                    Timestamp = DateTime.Now,
                    Action = "UPLOAD"
                };
                Config.Database.Insert(Log);
                return $"The API returned an error {Convert.ToInt32(resp.Response.StatusCode)}: {resp.Response.StatusCode}" + "\n"
                    + $"Error Message: \"`{error}`\"";
            }

        }
    }
}
