﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Intergrations.EndlessHosting
{
    [Group("eh"), Alias("endlhost")]
    public class EHModule : ModuleBase
    {
        [Command("role")]
        [Summary("Toggles the service user role.")]
        public async Task Role()
        {
            var user = Context.User as IGuildUser;
            IRole role = Context.Guild.GetRole(428635455135678465);
            if(user.RoleIds.ToList().Contains(role.Id)) // has role, remove it
            {
                await user.RemoveRoleAsync(role);
                await ReplyAsync($"Role `{role.Name}` Removed!");
            }
            else // doesn't have role, add it
            {
                await user.AddRoleAsync(role);
                await ReplyAsync($"Role `{role.Name}` Added!");
            }
        }
    }
}
