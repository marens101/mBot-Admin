﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Intergrations.Pomf
{
    public class PomfUploadResponse
    {
        public bool success { get; set; }
        public List<File> files { get; set; }

        public class File
        {
            public bool success { get; set; }
            public int errorcode { get; set; }
            public string description { get; set; }
            public string hash { get; set; }
            public string name { get; set; }
            public string url { get; set; }
            public int? size { get; set; }
        }

        public IRestResponse Response { get; set; }
    }
}
