﻿using mBot.Internal.Extensions;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace mBot.Intergrations.Pomf
{
    public class PomfClient
    {
        public PomfClient(string endpoint, string token = null)
        {
            Endpoint = endpoint;
            Token = token;
        }
        public string Endpoint { get; set; }
        public string Token { get; set; }
        public async Task<PomfUploadResponse> UploadAsync(byte[] data, string filename)
        {
            var dl = data.Length;
            if (dl > 20 * 1024 * 1024 || dl < 0)
                throw new ArgumentException("The data needs to be less than 20MiB and greather than 0B long.");
            var client = new RestClient(Endpoint);
            var request = new RestRequest(Method.POST);
            if(Token != null)
                request.AddParameter("key", Token, ParameterType.QueryString);
            request.AddFile("files[]", data, filename);
            //request.AddHeader("Content-Type", "multipart/form-data");
            var response = client.Execute(request);

            var json = response.Content;
            var o = JsonConvert.DeserializeObject<PomfUploadResponse>(json);
            o.Response = response;
            return o;
        }
    }
}
