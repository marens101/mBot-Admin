﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class RequireBotOwner : PreconditionAttribute
    {
        public override async Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider prov)
        {
            if (Config.Owners.IsOwner(context.User))
                return PreconditionResult.FromSuccess();
            else
                return PreconditionResult.FromError("You are not the bot owner!");
        }
    }
}
