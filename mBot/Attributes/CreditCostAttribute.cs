﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CreditCostAttribute : PreconditionAttribute
    {
        private readonly int _Cost;

        /// <summary>
        /// Requires a user to have the amount of credits before running the command, and optionally makes the charge.
        /// </summary>
        /// <param name="cost">How much the command should cost.</param>
        public CreditCostAttribute(int cost)
        {
            _Cost = cost;
        }
        public override async Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider prov)
        {
            var user = await Config.GetOrCreateUserAsync(context.User);
            if (user.Credits < _Cost)
                return PreconditionResult.FromError(
                    $"You don't have enough credits for that!" + '\n'
                    + $"Cost: {_Cost}" + '\n' 
                    + $"Your Balance: {user.Credits}" + '\n' 
                    + $"You Need: {_Cost - user.Credits}");
            return PreconditionResult.FromSuccess();
        }
    }
}
