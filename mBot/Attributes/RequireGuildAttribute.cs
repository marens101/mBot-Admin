﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class RequireGuildAttribute : PreconditionAttribute
    {
        private readonly ulong[] _guildIds;

        /// <summary>
        /// Only returns true if the guild id is the same as one passed as a paramater
        /// </summary>
        /// <param name="guildIds">A guild id to whitelist</param>
        public RequireGuildAttribute(params ulong[] guildIds)
        {
            _guildIds = guildIds;
        }
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider prov)
        {
            if (_guildIds.Contains(context.Guild.Id))
                return Task.FromResult(PreconditionResult.FromSuccess());
            return Task.FromResult(
                PreconditionResult.FromError($"Command is not availiable in this server."));
        }
    }
}
