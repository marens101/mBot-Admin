﻿using Discord;
using Discord.Commands;
using mBot.Internal.Extensions;
using mBot.ConfigService;
using mBot.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace mBot.Services
{
    /*class AdminTextMessageService
    {
        /// <summary>
        /// Sends a text to the mobile numbers provided in the `string[] to` paramater
        /// </summary>
        /// <param name="Context">An ICommandContext object</param>
        /// <param name="msg">The message to send</param>
        /// <param name="to">An array of strings representing the numbers to send to</param>
        /// <param name="usr">The user initiating the action, if blank defaults to Context.User</param>
        /// <returns>A SendingResult object from the server. Don't forget to check bool ErrorResult.Success!</returns>
        public async Task<SendingResult> SendTextAsync(ICommandContext Context, string msg, string[] to, IUser usr = null)
        {
            IUser performer = usr ?? Context.User;
            await LogSendRequest(Context, performer, to, msg);
            if (!performer.IsOwner())
                throw new System.Exception("You aren't authorised to initiate the TextingService! Only Bot Administrators can do that!");
            string Token = Config.BotConfig.TextMagicToken;
            TextmagicRest.Client TxtClient = new TextmagicRest.Client("marens101", Token);
            SendingOptions optns = new SendingOptions
            {
                From = "mBot",
                Text = msg,
                Phones = to,
            };
            var result = TxtClient.SendMessage(optns);
            if (result.Success)
            {
                Console.WriteLine($"Message ID {result.Id} has been successfully sent");
                return result;
            }
            else
            {
                await ErrLog.WriteLine($"Message was not sent due to following exception: {result.ClientException.Message}");
                ErrorHandling.mRollbar.Report(result.ClientException);
                return result;
                
            }
        }

        public async Task LogSendRequest(ICommandContext Context, IUser performer, string[] Array, string msg)
        {
            string csv = ArrayToCSV(Array);

            string alrt =
                $"    !ALERT!                                 !ALERT!: Text Sending by {performer.Username}#{performer.Discriminator}" + Environment.NewLine
                + $"    !ALERT!                                 !ALERT!: Text Sending to {csv}" + Environment.NewLine
                + $"    !ALERT!                                 !ALERT!: Text Sending content \"{msg}\""
                ;

            Console.Beep();
            Console.WriteLine(alrt);
            Console.Beep();
            //await SysLog.WriteLine(alrt);
            Console.Beep();
            //await Sys.Log.Texting($"    !ALERT!                                 !ALERT!: Text Sending by {Context.User.Username}#{Context.User.Discriminator}");
            //await NotificationService.OnSendTextRequest(Context, csv, msg, performer);
        }

        string ArrayToCSV(string[] array)
        {
            string to = "";
            foreach (string s in array)
            {
                to = to + s + ", ";
            }
            string rtn = to.Trim();
            rtn = rtn.Trim(',');
            rtn = rtn.Trim();
            return rtn;
        }
    }*/
}
