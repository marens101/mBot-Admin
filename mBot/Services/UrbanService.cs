﻿using Discord.Commands;
using mBot.Internal.Extensions;
using mBot.Objects.SearchObjects;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace mBot.Services
{
    public class UrbanService
    {
        public async Task UrbanSearch(ICommandContext Context, string search)
        {
            await Context.Channel.TriggerTypingAsync();
            if (Config.BotConfig.MashapeApiKey.IsNullOrWhitespace())
                throw new System.Exception("Mashape API Key Not Found");
            string html = string.Empty;
            string url = $@"https://mashape-community-urban-dictionary.p.mashape.com/define?term={search}";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Accept = "application/json";
            request.Headers.Add("X-Mashape-Key", Config.BotConfig.MashapeApiKey);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            string json = reader.ReadToEnd();
            UrbanObjects.RootObject obj = JsonConvert
                .DeserializeObject<UrbanObjects.RootObject>(json);

            var res = obj.list.FirstOrDefault();
            await Context.Channel.SendMessageAsync("Definition: " + res.definition + "\n"
                                                     + "Example: " + res.example);
        }
    }
}
