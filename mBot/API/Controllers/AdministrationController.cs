﻿using mBot.ConfigService;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace mBot.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AdministrationController : Controller
    {
        [HttpGet]
        [ActionName("CreateToken")]
        public string CreateToken(string MasterToken, string Description)
        {
            if (MasterToken == Config.BotConfig.ApiMasterToken)
                return JsonConvert.SerializeObject(mBotAPI.Auth.TokenMgmnt.CreateToken(Description));
            return "{\"Success\":\"false\",\"message\":\"Error: Invalid Master Token!\"}";
        }

        [HttpGet]
        [ActionName("CheckStatus")]
        public HttpResponseMessage CheckStatus()
        {
            var state = Config.Client.LoginState;
            switch (state)
            {
                case Discord.LoginState.LoggedIn: // 200
                    return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
                case Discord.LoginState.LoggingIn: // 202
                    return new HttpResponseMessage(System.Net.HttpStatusCode.Accepted);
                case Discord.LoginState.LoggedOut: // 503
                    return new HttpResponseMessage(System.Net.HttpStatusCode.ServiceUnavailable);
                case Discord.LoginState.LoggingOut: // 503
                    return new HttpResponseMessage(System.Net.HttpStatusCode.ServiceUnavailable);
                default: // 406
                    return new HttpResponseMessage(System.Net.HttpStatusCode.NotAcceptable);
            }
        }
    }
}
