﻿using Discord;
using Discord.WebSocket;
using mBot.ConfigService;
using mBot.Intergrations.RLME;
using mBot.Internal.Extensions;
using mBot.mBotAPI.Auth;
using mBot.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace mBot.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class MetricsController : Controller
    {
        [HttpGet]
        [ActionName("all")]
        public async Task<string> GetAllStats()
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();
            dict["users"] = (int)Metrics.TotalUsers.Value();
            dict["guilds"] = (int)Metrics.TotalGuilds.Value();
            dict["infractions"] = (int)Metrics.TotalInfractions.Value();
            return JsonConvert.SerializeObject(dict);
        }

        [HttpGet]
        [ActionName("prometheus")]
        public async Task<string> GetPrometheusMetrics()
        {
            string metricsStr = await Metrics.GetForPrometheus();
            return metricsStr;
        }



        public static HttpResponseMessage AccessDenied { get; }
            = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
    }
}
