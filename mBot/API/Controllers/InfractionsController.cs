﻿using Discord;
using Discord.WebSocket;
using mBot.ConfigService;
using mBot.Intergrations.RLME;
using mBot.Internal.Extensions;
using mBot.mBotAPI.Auth;
using mBot.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace mBot.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class InfractionsController : Controller
    {
        [HttpGet]
        [ActionName("get")]
        public async Task<string> GetInfraction(string code)
        {
            var q1 = Config.Database.Query<DInfractionCode>("SELECT * FROM InfractionCodes WHERE Code = ?", code);
            if (q1.Count != 0)
            {
                var q2 = Config.Database.Query<DInfraction>("SELECT * FROM Infractions WHERE UserId = ? AND GuildId = ?", q1[0].UserId, q1[0].GuildId);
                var res = new List<ApiInfraction>();
                foreach (var inf in q2)
                {
                    var target = await Config.GetOrCreateUserAsync(Convert.ToUInt64(inf.UserId), false);
                    var mod = await Config.GetOrCreateUserAsync(Convert.ToUInt64(inf.ModId), false);
                    var server = await Config.GetOrCreateGuildAsync(Convert.ToUInt64(inf.GuildId), false);
                    var rinf = new ApiInfraction
                    {
                        InfractionId = inf.Id,
                        Mod = mod.UsernameDiscrim,
                        ModId = mod.Id.ToUlong(),
                        Reason = inf.Message,
                        Server = server.Name,
                        ServerId = server.Id.ToUlong(),
                        Target = target.UsernameDiscrim,
                        TargetId = target.Id.ToUlong(),
                        Timestamp = inf.Timestamp.ToUniversalTime().ToString("dd MMMM yyyy"),// hh:mm tt") + " UTC",
                        Type = inf.Type.ToString()
                    };
                    res.Add(rinf);
                }
                return JsonConvert.SerializeObject(res.ToArray());
            }
            else
            {
                return "Invalid Code";
            }
        }

        

        public static HttpResponseMessage AccessDenied { get; }
            = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
    }
}
