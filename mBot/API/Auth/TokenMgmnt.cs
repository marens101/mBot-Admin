﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mBot.ConfigService;
using Newtonsoft.Json;
using mBot.Models;

namespace mBot.mBotAPI.Auth
{
    public class TokenMgmnt
    {
        /// <summary>
        /// Checks to see if a token exists for mBot API, and if it does return the properties of it.
        /// </summary>
        /// <param name="token">Input token to check</param>
        /// <param name="properties">Output token properties</param>
        /// <returns>Returns true if the token exists, and false if it doesn't</returns>
        public static bool GetToken(string token, out DApiToken properties)
        {
            var q = Config.Database.Query<DApiToken>("SELECT * FROM ApiTokens WHERE Token = ?", token);
            if (q[0] == null)
            {
                properties = null;
                return false;
            }
            properties = q[0];
            return true;
        }

        /// <summary>
        /// Creates a new token with no perms
        /// </summary>
        /// <returns>KeyValuePair with token and perms</returns>
        public static KeyValuePair<string, DApiToken> CreateToken(string Description)
        {
            DApiToken props = new DApiToken();
            props.Token = Guid.NewGuid().ToString();
            props.Description = Description;
            Config.Database.Insert(props);
            return new KeyValuePair<string, DApiToken>(props.Token, props);
        }
    }
}
