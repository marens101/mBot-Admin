﻿using mBot.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Internal.Extensions
{
    public static class ListExtension
    {
        /// <summary>
        /// Turns a List<string> into a string with all entries seperated by a provided seperator.
        /// </summary>
        /// <param name="List">The list to extract data from</param>
        /// <param name="seperator">A char seperator. '\n' works, and is the default</param>
        /// <returns>A string representation of the list, seperated by the provided seperator.</returns>
        public static string JoinAll(this List<string> List, char seperator = '\n')
        {
            StringBuilder builder = new StringBuilder();
            foreach(string s in List)
            {
                builder.Append(seperator + s);
            }
            string rtn = builder.ToString();
            rtn = rtn.Trim();
            rtn = rtn.Trim(seperator);
            rtn = rtn.Trim();
            return rtn;
        }


        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
        public static DataTable ToDataTable(this IList<DInfraction> data)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Id", Nullable.GetUnderlyingType(typeof(int)) ?? typeof(int));
            table.Columns.Add("Type", Nullable.GetUnderlyingType(typeof(string)) ?? typeof(string));
            //table.Columns.Add("Target", Nullable.GetUnderlyingType(typeof(string)) ?? typeof(string));
            table.Columns.Add("Mod", Nullable.GetUnderlyingType(typeof(string)) ?? typeof(string));
            table.Columns.Add("Reason", Nullable.GetUnderlyingType(typeof(string)) ?? typeof(string));
            table.Columns.Add("Timestamp", Nullable.GetUnderlyingType(typeof(string)) ?? typeof(string));
            foreach (DInfraction item in data)
            {
                DataRow row = table.NewRow();
                row["Id"] = item.Id;
                row["Type"] = item.Type.ToString();
                //row["Target"] = Config.Database.Get<DUser>(item.UserId).UsernameDiscrim;
                row["Mod"] = Config.Database.Get<DUser>(item.ModId).UsernameDiscrim;
                row["Reason"] = item.Message;
                row["Timestamp"] = item.Timestamp.ToShortDateString();
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
