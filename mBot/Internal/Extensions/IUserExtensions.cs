﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Internal.Extensions
{
    public static class IUserExtensions
    {
        /// <summary>
        /// Returns true if the user is a Bot Owner
        /// </summary>
        /// <param name="user">The user to scan</param>
        /// <returns>True if user is an Owner</returns>
        public static bool IsOwner(this IUser user)
        {
            if (user.Id == 267098766232911882)//Config.Owners.IsOwner(user)
                return true;
            else
                return false;
        }
    }
}
