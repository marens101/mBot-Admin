﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Internal.Extensions
{
    public static class SQLiteConnectionExtension
    {
        public static DataTable QueryToDataTable(this SQLiteConnection conn, string Query)
        {
            DataTable dt = new DataTable();
            SQLitePCL.sqlite3_stmt stQuery = SQLite3.Prepare2(conn.Handle, Query);
            var colLength = SQLite3.ColumnCount(stQuery);
            for (int i = 0; i < colLength; i++)
            {
                dt.Columns.Add(SQLite3.ColumnName(stQuery, i));
            }
            while (SQLite3.Step(stQuery) == SQLite3.Result.Row)
            {
                DataRow row = dt.NewRow();
                for (int i = 0; i < colLength; i++)
                {
                    string colName = SQLite3.ColumnName(stQuery, i);
                    var colType = SQLite3.ColumnType(stQuery, i);
                    switch (colType)
                    {
                        case SQLite3.ColType.Blob:
                            row[colName] = SQLite3.ColumnBlob(stQuery, i);
                            break;
                        case SQLite3.ColType.Float:
                            row[colName] = SQLite3.ColumnDouble(stQuery, i);
                            break;
                        case SQLite3.ColType.Integer:
                            row[colName] = SQLite3.ColumnInt(stQuery, i);
                            break;
                        case SQLite3.ColType.Null:
                            row[colName] = null;
                            break;
                        case SQLite3.ColType.Text:
                            row[colName] = SQLite3.ColumnString(stQuery, i);
                            break;
                    }
                }
                dt.Rows.Add(row);
            }
            SQLite3.Finalize(stQuery);
            return dt;
        }
    }
}
