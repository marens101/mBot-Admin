﻿using App.Metrics.Gauge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mBot.Internal.Extensions
{
    public static class MetricsExtentions
    {
        public static double Value(this GaugeOptions go)
        {
            return Metrics.MetricsRoot.Snapshot.GetGaugeValue(go.Context, go.Name);
        }
        public static double New(this GaugeOptions go)
        {
            return Metrics.MetricsRoot.Snapshot.GetGaugeValue(go.Context, go.Name);
        }
    }
}
