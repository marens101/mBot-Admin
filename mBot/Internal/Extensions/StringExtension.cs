﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Internal.Extensions
{
    public static class StringExtension
    {
        public static string ToAlphanumeric(this string input)
        {
            string str = "";
            string allowedChars = ""
                + "abcdefghijklmnopqrstuvwxyz" 
                + "ABCDEFGHIJKLMNOPQRSTUVWXYZ" 
                + "1234567890"
                ;
            foreach (char c in input)
            {
                if (allowedChars.Contains(c))
                {
                    str = str + c;
                }
            }
            return str;
        }

        /// <summary>
        /// Indicates whether a specified string is null, empty, or consists only of white-space characters.
        /// </summary>
        /// <param name="Str"></param>
        /// <returns></returns>
        public static bool IsNullOrWhitespace(this string Str)
        {
            return String.IsNullOrWhiteSpace(Str);
        }

        public static ulong ToUlong(this string Str)
        {
            return Convert.ToUInt64(Str);
        }
    }
}
