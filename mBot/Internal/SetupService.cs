﻿using System.IO;

namespace mBot.Internal
{
    public partial class SetupService
    {
        public void SetupFolders()
        {
            SetupRoot();
            SetupData();
            SetupRes();
            SetupLogs();
            SetupIntergrations();
        }
        public void SetupRoot()
        {
            Directory.CreateDirectory(@"BotSystem");
            Directory.CreateDirectory(@"BotSystem/Sys");
            Directory.CreateDirectory(@"BotSystem/Sys/Rollbar");
            Directory.CreateDirectory(@"BotSystem/Data");
            Directory.CreateDirectory(@"BotSystem/Res");
            Directory.CreateDirectory(@"data/Logs");
            Directory.CreateDirectory(@"data/Intergrations");
            Directory.CreateDirectory(@"BotSystem/API");
        }

        public void SetupData()
        {
            Directory.CreateDirectory($@"data/Diagnostics");
            Directory.CreateDirectory($@"data/Diagnostics/Dumps");
        }
        public void SetupRes()
        {
            Directory.CreateDirectory(@"data/Res/Decide");
        }
        public void SetupLogs()
        {
            Directory.CreateDirectory(@"data/Logs/Events");
            Directory.CreateDirectory(@"data/Logs/Error");
        }
        public void SetupIntergrations()
        {                                                 
            Directory.CreateDirectory(@"data/Intergrations/OwO");
            Directory.CreateDirectory(@"data/Intergrations/OwO/Users");
            Directory.CreateDirectory(@"data/Intergrations/OwO/Logs");
            Directory.CreateDirectory(@"data/Intergrations/OwO/Temp");
            Directory.CreateDirectory(@"data/Intergrations/RLME");
            Directory.CreateDirectory(@"data/Intergrations/RLME/Users");
            Directory.CreateDirectory(@"data/Intergrations/RLME/Logs");
            Directory.CreateDirectory(@"data/Intergrations/RLME/Temp");
            Directory.CreateDirectory(@"data/Intergrations/NoSelfPromo");
        }
    }
}
