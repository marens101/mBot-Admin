﻿using mBot.ConfigService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot.Internal
{
    public class SysLog
    {
        public static string Path
            = @"data\Logs\SysLog.log";

        public static async Task Write(string msg)
        {
            File.AppendAllText(Path, msg);
        }
        public static async Task WriteLine(string msg)
        {
            File.AppendAllText(Path, msg + Environment.NewLine);
        }
    }

    public class ErrLog
    {
        public static string Path
            = @"data\Logs\ErrLog.log";

        public static async Task Write(string msg)
        {
            File.AppendAllText(Path, msg);
        }
        public static async Task WriteLine(string msg)
        {
            File.AppendAllText(Path, msg + Environment.NewLine);
        }
    }
}
