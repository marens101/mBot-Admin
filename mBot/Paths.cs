﻿using mBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot
{
    public class Paths
    {
        public static void Init()
        {
            System = new SystemPaths();
            RLME = new RlmePaths();
            OwO = new OwOPaths();
            NSP = new NSPPaths();
        }
        public class SystemPaths
        {
            public string ConfigJson { get; }
                 = @"data/config.json";
            public string Database { get; }
                 = @"data/mBot.db";
            public string DiagnosticDump(string Dump_Code)
            {
                var res = Config.Database.Query<DiagnosticResultMap>("SELECT DiagnosticCode FROM DiagnosticResults WHERE DiagnosticCode = ?", Dump_Code);
                if (res.Count == 0)
                    throw new System.Exception("Invalid Diagnostic Code");
                DiagnosticResultMap mapping = res[0];
                return $@"BotSystem/Data/Diagnostics/Dumps/{mapping.FileName}";
            }
        }
        public static SystemPaths System;

        public class RlmePaths
        {
            public string ConfigJson(ulong userId)
            {
                return $@"data/Intergrations/RLME/Logs/{userId}.log";
            }
            public string RlmeConfig { get; }
                = @"data/Intergrations/RLME/RLConf.json";
        }
        public static RlmePaths RLME;

        public class OwOPaths
        {
            public string Config(ulong userId)
            {
                return $@"data/Intergrations/OwO/Users/{userId}.OwOConfig";
            }
            public string Logs(ulong userId)
            {
                return $@"data/Intergrations/OwO/Logs/{userId}.log";
            }
        }
        public static OwOPaths OwO;

        public class NSPPaths
        {
            public string ConfigJson { get; }
                 = @"data/Intergrations/NoSelfPromo/config.json";
            public string Database { get; }
                 = @"data/Intergrations/NoSelfPromo/NoSelfPromo.db";
        }
        public static NSPPaths NSP;
    }
}
