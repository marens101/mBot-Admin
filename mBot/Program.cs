﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using mBot.Internal;
using mBot.Models;
using mBot.Services;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace mBot
{
    public class Program
    {

        //private DependencyMap map;
        //private MusicService musicService;
        private IServiceProvider _services;

        public static void Main(string[] args)
        {
            new Program().Run(args).GetAwaiter().GetResult();
        }

        public async Task Run(string[] args)
        {
            Paths.Init();
            if (!File.Exists(Paths.System.ConfigJson))
            {
                throw new System.Exception($"data/config.json Not Found"); ;
            }
            new SetupService().SetupFolders();
            Config.Init();
            Console.Title = $"mBot v{Config.Version}";
            ErrorHandling.mRollbar.Init();
            //ErrorHandling.mSentry.Init();

            Config.Client = new DiscordSocketClient(new DiscordSocketConfig()
            {
                LogLevel = LogSeverity.Verbose,
                //AudioMode = AudioMode.Outgoing,
            });
            Config.Client.Log += Client_LogAsync;
            await Config.Authenticate();
            await InstallCommands();
            Config.Client.Ready += Client_ReadyAsync;
            Config.Client.UserJoined += Client_UserJoinedAsync;
            //Config.Client.MessageReceived += Client_MessageReceived;
            //Config.Client.MessageUpdated += Client_MessageUpdated;
            Config.Client.JoinedGuild += Client_JoinedGuildAsync;
            Config.Client.Disconnected += Client_DisconnectedAsync;

            Metrics.Init();
            await Metrics.Refresh();
            await Metrics.GetForPrometheus();

            StartMvc(args);

            await Task.Delay(-1);
        }

        public static async Task StartMvc(string[] args)
        {
            // Owin:
            //string baseAddress = $"http://*:{Config.BotConfig.ApiPort}";
            //WebApp.Start<API.OwinStartup>(baseAddress);

            // AspSelfHost
            //var WebApiConfig = new HttpSelfHostConfiguration($"http://0.0.0.0:{Config.BotConfig.ApiPort}");
            //WebApiConfig.Routes.MapHttpRoute(
            //    "API Default", "api/{controller}/{action}/",
            //    new { id = RouteParameter.Optional });
            //HttpSelfHostServer WebApiServer = new HttpSelfHostServer(WebApiConfig);
            //WebApiServer.OpenAsync().Wait();

            // Asp Core
            Config.WebHost = BuildWebHost(args);
            await Config.WebHost.RunAsync();

            //await Task.Delay(-1);
        }
        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel()
                .UseUrls($"http://0.0.0.0:{Config.BotConfig.ApiPort}/")
                .Build();
        }

        public static async Task Client_LogAsync(LogMessage message)
        {
            {
                //await SesLog.WriteLine("	" + DateTime.Now + " | " + message.ToString());
                string sevMsg = message.Severity.ToString().Substring(0, 4).PadRight(6);
                if (message.Severity == LogSeverity.Warning || message.Severity == LogSeverity.Error || message.Severity == LogSeverity.Critical)
                {
                    await ErrLog.WriteLine("	" + DateTime.Now + " | " + sevMsg.Trim() + " | " + message.ToString());
                }

                if (message.Severity == LogSeverity.Warning)
                {
                    Console.Error.WriteLine(sevMsg + message.ToString());
                }
                else if (message.Severity == LogSeverity.Error)
                {
                    Console.Error.WriteLine(sevMsg + message.ToString());
                }
                else if (message.Severity == LogSeverity.Critical)
                {
                    Console.Error.WriteLine(sevMsg + message.ToString());
                }
                else
                {
                    Console.WriteLine(sevMsg + message.ToString());
                }
                //return Task.CompletedTask;
            }
        }

        private async Task Client_ReadyAsync()
        {
            await SysLog.WriteLine("  " + $"----> Connected at {DateTime.Now} <----");
            await Config.Client.SetGameAsync(Config.DefaultStatus);
            //await new NotificationService().OnReady(client);
            //await new DiscordBotsDotOrgService().PostStats(client);
        }
        private async Task Client_UserJoinedAsync(SocketGuildUser arg)
        {
            try
            {
                var guildp = await Config.GetOrCreateGuildAsync(arg.Guild);
                if (guildp == null)
                {
                    return;
                }

                if (!(guildp.AutoRoleId == null))
                {
                    var role = arg.Guild.GetRole(Convert.ToUInt64(guildp.AutoRoleId));
                    await arg.AddRoleAsync(role);
                }
            }
            catch
            {

            }

            return;
        }
        //private Task Client_MessageUpdated(Cacheable<IMessage, ulong> arg1, SocketMessage arg2, ISocketMessageChannel arg3)
        //{
        /* Params:
         * arg1 = old message, might be null, id always there
         * arg2 = new message
         * srg3 = channel
         */
        //throw new NotImplementedException();
        //}
        /*private Task Client_MessageReceived(SocketMessage arg)
        {
            //throw new NotImplementedException();
        }*/
        private async Task Client_JoinedGuildAsync(SocketGuild arg)
        {
            await NotificationService.OnJoinGuild(Config.Client, arg);
        }
        private async Task Client_DisconnectedAsync(Exception arg)
        {
            Console.Error.WriteLine("Disconnected: " + arg.Message);
            try
            {
                (new ErrorHandling.ErrorEvent(arg)).Report();
            }
            catch
            {

            }
            await Task.Delay(10000); //10s, might need adjusting later
            if (Config.Client.ConnectionState == ConnectionState.Connected)
            {
                // Close the current process
                Quit(ExitCode.Disconnected);
            }
        }

        public async Task InstallCommands()
        {
            // Hook the MessageReceived Event into our Command Handler
            Config.Client.MessageReceived += HandleCommand;
            await Config.Commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);
        }
        public async Task HandleCommand(SocketMessage messageParam)
        {
            var message = messageParam as SocketUserMessage;
            if (message == null)
            {
                return;
            }

            if (message.Author.IsBot)
            {
                return;
            }

            int argPos = 0;
            var cont = false;
            if (message.HasStringPrefix(Config.BotConfig.OverridePrefix, ref argPos))
            {
                cont = true;
            }
            else if (message.HasMentionPrefix(Config.Client.CurrentUser, ref argPos))
            {
                cont = true;
            }

            var context = new CommandContext(Config.Client, message);
            try
            {
                var guild = await Config.GetOrCreateGuildAsync(context.Guild);
                if (message.HasStringPrefix(guild.Prefix, ref argPos))
                {
                    cont = true;
                }
            }
            catch
            {
                if (message.HasStringPrefix(Config.BotConfig.DefaultPrefix, ref argPos))
                {
                    cont = true;
                }
            }

            var tuser = await Config.GetOrCreateUserAsync(context.User);
            if (!cont)
            {
                return;
            }

            var result = await Config.Commands.ExecuteAsync(context, argPos, _services);
            if (!result.IsSuccess)
            {
                if (result.Error.Value != CommandError.UnknownCommand)
                {
                    var e = new ErrorHandling.ErrorEvent();
                    await context.Channel.SendMessageAsync($"{result.ErrorReason}");
                }

            }
        }
        public async Task InstallAsync(DiscordSocketClient client)
        {
            // Here, we will inject the ServiceProvider with 
            // all of the services our client will use.
            _services = new ServiceCollection()
                .AddSingleton(client)
                //.AddPaginator(client)
                .AddSingleton(Config.Commands)
                // You can pass in an instance of the desired type
                //.AddSingleton(new RavenClient(Config.BotConfig.SentryDSN))
                // ...or by using the generic method.
                .BuildServiceProvider();
            // ...
            await Config.Commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);
        }

        public static void Quit(ExitCode exitCode)
        {
            Environment.Exit((int)exitCode);
        }
        public enum ExitCode
        {
            /// <summary>
            /// 0 = UnknownOk [Ok]
            /// </summary>
            Unassigned = 0,

            /// <summary>
            /// 1 = Disconnection Shutdown [Error]
            /// </summary>
            Disconnected = 1
        }
    }
}
